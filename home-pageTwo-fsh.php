<?php
/* Template Name: home-pageTwo */
get_header();?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css" integrity="sha256-gVCm5mRCmW9kVgsSjQ7/5TLtXqvfCoxhdsjE6O1QLm8=" crossorigin="anonymous" />

<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

<audio id="loop-limited" autoplay >
	<source src="/wp-content/audio/foot.mp3" type="audio/mpeg">
</audio>


<div id="main-section-seb">


<div class="container-fsh col-md-12">
	<div class="left- col-md-6 col-sm-12"><img src="/wp-content/uploads/2018/08/Screen-Shot-2018-08-24-at-11.12.37.png"></div><div class="right- col-md-6 col-sm-12"><div class="ticket-form-inner">
		<div class="form-image" id="image-shake"><img src="/wp-content/uploads/2018/08/logo-001.png"></div>
<?php echo do_shortcode('[contact-form-7 id="26" title="tickets"]');?>
</div></div>











</div>


</div>
<audio id="roar">
    <source src="/wp-content/audio/roar2.wav" type="audio/wav">

</audio>
<script type="text/javascript">
	// Audio Loop Limit
var loopLimit = 5;
var loopCounter = 0;
document.getElementById('loop-limited').addEventListener('ended', function(){
    if (loopCounter < loopLimit){
        this.currentTime = 0;
        this.play();
        loopCounter++;
    } else  {

    	$('#roar')[0].play();

    }
}, false);

</script>
<?php
get_footer();