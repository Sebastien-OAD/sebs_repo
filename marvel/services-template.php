<?php
/* Template Name: services-page */
get_header();?>
<link rel="stylesheet" href="http://localhost:8888/wordy/wp-content/themes/marvel/css/style.css" type="text/css" media="screen"/>
<style>
        a{
            color:#fff;
            text-decoration:none;
        }
        a:hover{
            text-decoration:underline;
        }
        span.reference{
            position:fixed;
            left:10px;
            bottom:10px;
            font-size:13px;
            font-weight:bold;
        }
        span.reference a{
            color:#fff;
            text-shadow:1px 1px 1px #000;
            padding-right:20px;
        }
        span.reference a:hover{
            color:#ddd;
            text-decoration:none;
        }

    </style>
    <div id="left"></div>
<div id="right"></div>
<div id="top"></div>
<div id="bottom"></div>






    <body id="content" >
    <div class="services-page">

        <div class="section black" id="section1">
            <img src="<?php the_field('section_1_image');?>" />
        </div>

        <div class="section white" id="section2">
<?php the_field('section_2_text');?>
        </div>

        <div class="section black" id="section3">
            <img src="<?php the_field('section_3_image');?>" />
        </div>

        <div class="section white" id="section4">
<?php the_field('section_4_text');?>
        </div>

        <div class="section black" id="section5">
            <img src="<?php the_field('section_5_image');?>" />
        </div>

        <div class="section white" id="section6">
<?php the_field('section_6_text');?>
        </div>

        <div class="section black" id="section7">
            <img src="<?php the_field('section_7_image');?>" />
        </div>

        <div class="section white" id="section8">
<?php the_field('section_8_text');?>
</div>
    </div>
        <!-- The JavaScript -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script type="text/javascript" src="http://localhost:8888/wordy/wp-content/themes/marvel/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript">
            $(function() {
                $('ul.nav a').bind('click',function(event){
                    var $anchor = $(this);
                    /*
                    if you want to use one of the easing effects:
                    $('html, body').stop().animate({
                        scrollLeft: $($anchor.attr('href')).offset().left
                    }, 1500,'easeInOutExpo');
                     */
                    $('html, body').stop().animate({
                        scrollLeft: $($anchor.attr('href')).offset().left
                    }, 3000);
                    event.preventDefault();
                });
            });
        </script>
    </body>







<?php get_footer();?>