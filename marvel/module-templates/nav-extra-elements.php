<?php
/**
 * Navigation Extra Elements.
 *
 * Search, Cart, Side Navigation...
 *
 * @package vslmd
 */

function nav_extra_elements() {

    $options = get_option('vslmd_options');
    $search_header = (empty($options['search_header'])) ? '0' : $options['search_header'];
    $woocart = (empty($options['woocart'])) ? '0' : $options['woocart'];
    $side_navigation = (empty($options['side_navigation'])) ? '' : $options['side_navigation'];

                            // default value of 'items_wrap' is <ul id="%1$s" class="%2$s">%3$s</ul>'

                            // open the <ul>, set 'menu_class' and 'menu_id' values
    $wrap  = '<ul id="%1$s" class="%2$s">';

    if( $side_navigation != '' && is_active_sidebar( 'side-navigation' ) ) {

                            // Side Navigation
        $wrap .= '

        <ul class="nav navbar-nav navbar-right side-navigation-link">
            <li>
                <a id="open-side-navigation" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
            </li>
        </ul>';

    }

    if($search_header == '1') {

                            // Search Middle Screen
        $wrap .= '

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
                <ul role="menu" class="dropdown-menu extra-md-menu">
                    <li>
                        <form method="get" id="searchform" action="'. esc_url( home_url( '/' ) ) .'" role="search">
                            <div class="input-group">
                                <input type="text" class="field form-control" name="s" id="s" placeholder="'. __( 'Search &hellip;', 'vslmd' ).'" />
                                <span class="input-group-btn">
                                    <input type="submit" class="submit btn btn-primary" name="submit" id="searchsubmit" value="'. __( 'Search', 'vslmd' ) .'" />
                                </span>
                            </div>
                        </form>
                    </li>
                </ul>
            </li>
        </ul>';

    }
    global $woocommerce; 
    if($woocommerce && $woocart == '1') {

                            // Cart Menu
        $wrap .= '

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-shopping-cart" aria-hidden="true"><div class="cart-content-count"><span>'. $woocommerce->cart->cart_contents_count .'</span></div></i></a>
                <ul role="menu" class="dropdown-menu extra-md-menu">
                    <li class="cart-menu">
                        <div class="widget_shopping_cart_content"></div>    
                    </li>
                </ul>
            </li>
        </ul>';

    }


                            // get nav items as configured in /wp-admin/
    $wrap .= '%3$s';

                            // close the <ul>
    $wrap .= '</ul>';

                            // return the result
    return $wrap;
} 