<?php
/* Template Name: home-page */
get_header();?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<div class="wrapper" id="page-wrapper">
<div id="left"></div>
<div id="right"></div>
<div id="top"></div>
<div id="bottom"></div>

<div class="row">
       	<div class="yellow-band col-md-6 col-sm-6">
       		<div class="outter-column">
       			<div class="shoosh"></div>
	       	    <div class="inner-column-b">

<ul id="main-menu-hp" class="home-page-menu">
<nav class="site-navigation">

        <div class="navbar <?php echo $header_color_scheme.' '.$header_height;?>">

            <div class="navbar-header">

                <!-- Your site title as branding in the menu -->



         </div>

         <!-- The WordPress Menu goes here -->
<?php wp_nav_menu(
	array(
		'menu'            => 'primary',
		'theme_location'  => $change_menu,
		'depth'           => 4,
		'container_class' => 'collapse navbar-collapse navbar-responsive-collapse',
		'menu_class'      => $link_color_style.' '.'nav navbar-nav vertical-header-menu',
		'fallback_cb'     => '',
		'menu_id'         => 'main-menu',
		'walker'          => new wp_bootstrap_navwalker()
	)
);?>
</div><!-- .navbar -->

        </nav><!-- .site-navigation -->
<!-- <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37 "><a title="Services" href="http://localhost:8888/wordy/services/">
							<div class="title-content">Services</div></a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36 "><a title="Works" href="http://localhost:8888/wordy/works/">
							<div class="title-content">Works</div></a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35 "><a title="What we do" href="http://localhost:8888/wordy/what-we-do/">
							<div class="title-content">What we do</div></a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34 "><a title="News" href="http://localhost:8888/wordy/news/">
							<div class="title-content">News</div></a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33 "><a title="Publications" href="http://localhost:8888/wordy/publications/">
							<div class="title-content">Publications</div></a></li>-->
		            </ul>
	            </div>
	        </div>
        </div>
       	  <div class="image-band col-md-6 col-sm-6"><img class="brand-default" src="http://localhost:8888/wordy/wp-content/uploads/2017/10/logo-1.png">
       	  	<img src="https://www.saraceninteriors.com/assets/media/140937-Sirius-Scarborough-133-LOW-RES.jpg"></div>
</div>



    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">


            <main id="main" class="site-main" role="main">


<?php while (have_posts()):the_post();?>

<?php get_template_part('loop-templates/content', 'page');?>

<?php
// If comments are open or we have at least one comment, load up the comment template
if (comments_open() || get_comments_number()):

comments_template();

endif;
?>

<?php endwhile;// end of the loop. ?>
</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();?>