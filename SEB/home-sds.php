<?php /*Template Name: home-sds */?>
<?php
/**
* The Template for page.
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
get_header();
?>
<div id="thmlvContent">
	
	<div class="col-md-3 col-sm-6">
		<img src="<?php the_field('grid_image');?>" />
		<h2><?php the_field('icon_title');?></h2>
		<p><?php the_field('icon_body');?></p>
	</div>
</div>
<?php
get_footer();
?>

