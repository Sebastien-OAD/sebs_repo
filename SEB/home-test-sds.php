<?php /*Template Name: home-test-sds */ ?>
<?php
/**
* The Template for page.
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
get_header();
?>
<div id="thmlvContent">
	<?php
	echo north_switch_header($post->ID);
	
	?>
	<?php putRevSlider( 'hp-slider' ); ?>
	</div>
</div>
<div id="thmlvContent-sds">
	
	<?php echo do_shortcode( '[query slug="icon-grid"]' ); ?>
	</div>
</div>
<?php
get_footer();
?>