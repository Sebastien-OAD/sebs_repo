<?php
/*
 * Template Name: News Article
 * Template Post Type: news_article
 */
$format_in    = 'Ymd';// the format your value is saved in (set in the field options)
$format_out_1 = 'd';// the format you want to end up with
$format_out_2 = 'F';// the format you want to end up with
$format_out_3 = 'Y';// the format you want to end up with

$date = DateTime::createFromFormat($format_in, get_field('news_date'));

get_header();?>
<div id="left"></div>
<div id="right"></div>
<div id="top"></div>
<div id="bottom"></div>
<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">


					<div class="col-md-12 news-article">
						<div class="row">
						<div class="news-left col-md-2">
							<div class="news-day"><?php echo $date->format($format_out_1);?> </div>
							<div class="news-month"><?php echo $date->format($format_out_2);?></div>
							<div class="news-year"><?php echo $date->format($format_out_3);?> </div>
						</div>
						<div class="news-right col-md-10"> <?php if (get_field('news_hero_image')) {?>
																											<img src="<?php the_field('news_hero_image');?>" />
	<?php }?></div></div>
<div class="news-body col-md-12">
							<h1><?php the_field('news_headline');?></h1>

								<p><?php the_field('news_body');?></p>
									<!--<?php the_field('news_summary');?>
									<?php the_field('news_date');?>-->
					</div>
				</div>









</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();?>