<?php
/*
 * Template Name: Case Study
 * Template Post Type: case_study
 */

get_header();?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>
  <link rel="stylesheet" href="http://localhost:8888/saracen-copy/wp-content/themes/marvel/css/flexslider.css">
<div id="left"></div>
<div id="right"></div>
<div id="top"><div class="saracen-logo-main"> <img src="http://localhost:8888/saracen-copy/wp-content/themes/marvel-child/images/sar-dark.png"> </div> <div class="saracen-services"><img src="http://localhost:8888/saracen-copy/wp-content/themes/marvel-child/images/facilities.png"><img src="http://localhost:8888/saracen-copy/wp-content/themes/marvel-child/images/small-works.png"><img src="http://localhost:8888/saracen-copy/wp-content/themes/marvel-child/images/move-management.png"></div></div>
<div id="bottom"></div>
<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">

<?php if (get_field('hero_image')) {?>
	<div class="case-study-hero-slider col-md-12">
	<?php

	$images = get_field('hero_image');

	if ($images):?>
	<div id="slider" class="flexslider case-study-hero">
																														        <ul class="slides">
	<?php foreach ($images as $image):?>
																														                <li>
																														                    <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" />

																														                </li>
	<?php endforeach;?>
	</ul>
																														    </div>

	<?php endif;?>
	</div>
	<?php }?>


<div class="col-container swoosh-background">
  <div class="col col-md-6">
    <img src="http://localhost:8888/saracen-copy/wp-content/uploads/2017/11/swish-WIP.png" />

  </div>

  <div class="col col-md-6">

  </div>


</div>


<div class="col-container case-study-intro">
  <div class="col  col-intro">
    <h1><?php the_title();?></h1>
    <div class="parameters">
        <div class="parameter">
<?php the_field('location');?></div>
         <div class="parameter">
<?php the_field('project');?></div>
		<div class="parameter">
<?php the_field('project_size');?></div>
		<div class="parameter">
<?php the_field('timeline');?></div>
	</div>
    <p><?php the_field('case_study_body');?></p>

  </div>

  <div class="col col-quicklinks quick-links">
        <li><a href="#gallery">Gallery</a></li>
	    <li><a href="#videos">Videos</a></li>
	    <li><a href="#timelapse">Timelapse</a></li>


  </div>


</div>













<?php the_field('summary_description');?>

<?php the_field('quote');?>
<?php the_field('quote_by');?>
<div class="main" id="videos">
<?php the_field('video');?>
</div>
<div class="main" id="timelapse">
<?php the_field('timelapse');?>
</div>


<div class="my-row">



<?php
wp_reset_query();// necessary to reset query
while (have_posts()):the_post();
the_content();
endwhile;// End of the loop.
?>
</div>


























</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();?>
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>
<script src="http://localhost:8888/saracen-copy/wp-content/themes/marvel/js/jquery.flexslider.js"></script>

  <script type="text/javascript">

  	$(document).ready(function(){

   $(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide"
  });
});
    });
  </script>

