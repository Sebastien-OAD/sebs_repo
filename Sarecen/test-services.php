<?php
/* Template Name: services-test */
get_header();?>
<link rel="stylesheet" type="text/css" href="http://localhost:8888/wordy/wp-content/themes/marvel/css/jquery.fullPage.css" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://localhost:8888/wordy/wp-content/themes/marvel/js/jquery.fullPage.js"></script>



<div id="left"></div>
<div id="right"></div>
<div id="top"></div>
<div id="bottom"></div>
<div class="wrapper" id="page-wrapper">

    <div  id="content-ss" class="container-ss">

       <div id="primary" class=" content-area">

            <main id="main" class="site-main" role="main">




            	<div id="fullpage">
            		<div class="section">
						<div class="slide"><div class="slider-container"><img src="<?php the_field('section_1_image');?>" /></div></div>
						<div class="slide"><?php the_field('section_2_text');?></div>
						<div class="slide"><div class="slider-container"><img src="<?php the_field('section_3_image');?>" /></div></div>
						<div class="slide"> <?php the_field('section_4_text');?> </div>
						<div class="slide"><div class="slider-container"><img src="<?php the_field('section_5_image');?>"/></div></div>
						<div class="slide"> <?php the_field('section_6_text');?> </div>
						<div class="slide"><div class="slider-container"><img src="<?php the_field('section_7_image');?>"/></div></div>
						<div class="slide"> <?php the_field('section_8_text');?></div>

					</div>
				</div>












			</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->
<script type="text/javascript">
    $(document).ready(function() {
	$('#fullpage').fullpage();
});
</script>

<?php get_footer();?>