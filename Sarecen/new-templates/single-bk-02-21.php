<?php
/*
 * Template Name: Case Study
 * Template Post Type: case_study
 */

get_header();?>
<style>
  .embed-container {
    position: relative;
    padding-bottom: 56.25%;
    height: 0;
    overflow: hidden;
    max-width: 100%;
    height: auto;
  }

  .embed-container iframe,
  .embed-container object,
  .embed-container embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
</style>
<!--scripts for full page scroll-->
<link rel="stylesheet" type="text/css" href="/wp-content/themes/marvel-child/jquery.fullPage.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript" src="/wp-content/themes/marvel-child/js/scrolloverflow.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/marvel-child/js/jquery.fullPage.js"></script>
<script type="text/javascript" src="/wp-content/themes/marvel-child/js/masonry.pkgd.min.js"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" integrity="sha256-HAaDW5o2+LelybUhfuk0Zh2Vdk8Y2W2UeKmbaXhalfA=" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js" integrity="sha256-jGAkJO3hvqIDc4nIY1sfh/FPbV+UK+1N+xJJg6zzr7A=" crossorigin="anonymous"></script>






<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>-->
  <link rel="stylesheet" href="/wp-content/themes/marvel-child/flexslider.css">
  <div class="get-in-touch">Get in touch</div>
<div class="saracen-services"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"><!--<img src="/wp-content/uploads/bits/logo-saracen-interiors.png">--></div>

<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">
              <div id="fullpage">

<div class="section fp-auto-height-responsive" id="sectionA">



<div class="row cs">
 <div class="col-lg-3 col-md-6 col-sm-12 left-column-cs">
  <div class="left-inner-cs ">

<h1><?php the_title();?></h1>
<div class="parameters">
                  <div class="parameter">
<?php the_field('location');?></div>
                   <div class="parameter">
<?php the_field('project');?></div>
            <div class="parameter">
<?php the_field('project_size');?></div>
            <div class="parameter">
<?php the_field('timeline');?></div>
           </div>
<?php the_field('summary_/_description');?>
<div class="col-quicklinks quick-links">
                  <li><a href="#section5">Gallery</a></li>
               <li><a href="#section4">Videos</a></li>
               <li><a href="#section6">Timelapse</a></li>


            </div>
            <div class="embed-container">
<?php
$video = get_field('video');
if ($video) {
	preg_match('/src="(.+?)"/', $video, $matches_url);
	$src = $matches_url[1];

	preg_match('/embed(.*?)?feature/', $src, $matches_id);
	$id = $matches_id[1];
	$id = str_replace(str_split('?/'), '', $id);

	?>
		<div class="utube-image">
		<a href="https://www.youtube.com/watch?v=<?php echo $id;?>" data-toggle="lightbox"><img src="http://img.youtube.com/vi/<?php echo $id;?>/mqdefault.jpg"></a>



		</div>  <?php }?>
</div>
</div></div>


<div class="slider-holder col-lg-9 col-md-6 col-sm-12 right-column-cs">

<?php

$images = get_field('hero_image');

if ($images):?>
<div id="slider" class="flexslider">

        <ul class="slides">
<?php foreach ($images as $image):?>
<li>
    <div class="image-holder" style="background-image: url('<?php echo $image['url'];?>')"></div>
                    <!--<img class="respin" src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" />-->

</li>
<?php endforeach;?>
</ul>
    </div>

<?php endif;?></div></div></div>


<?php $body_background = get_field('body_background');?>
<!--<div class="section">

<?php the_field('summary_description');?></div>-->
<div class="section fp-auto-height-responsive"  id="sectionB" style="background-image: url('<?php echo $body_background['url'];?>')" >
 <div class="main-body-text-cs col-md-12">
 <div class="col-md-12">
<h2><?php the_title();?></h2>
</div>
<div class="parameters col-md-3">
                  <div class="parameter">
<?php the_field('location');?></div>
                   <div class="parameter">
<?php the_field('project');?></div>
            <div class="parameter">
<?php the_field('project_size');?></div>
            <div class="parameter">
<?php the_field('timeline');?></div>
           </div>

 <div class="cs-body col-md-9"><?php the_field('case_study_body');?></div>
</div>
</div>

<div class="section fp-auto-height-responsive" id="sectionC">

<div class="quote-area col-md-12">
  <div class="quote-inner">
<div class="quote-body-text"><?php the_field('quote');?></div>
<div class="quote-by-text"><?php the_field('quote_by');?></div></div></div>
</div>
<div class="section fp-auto-height-responsive" id="sectionD">
<div class="main" id="videos">
<div class="video-container">
  <div class="col-md-12 video-holder-cs">
<div class="embed-container">
<?php the_field('video');?>
</div>
</div>
</div>

</div>
</div>




<div class="section fp-auto-height-responsive" id="sectionE">
<div class="gallery-container">
<div class="grid">


<?php

$images = get_field('hero_image');

if ($images):?>
 <?php $toggle_class = 'even';?>
<?php foreach ($images as $image):?>
  <?php $toggle_class = ($toggle_class == 'odd'?'even':'odd');?>
          <div class="grid-item <?php echo $toggle_class;?> ">
                <a href="<?php echo $image['url'];?>">
                     <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" />
                </a>
                </div>

<?php endforeach;?>

<?php endif;?>
</div>
</div>
</div>


<div class="section fp-auto-height-responsive" id="sectionF">
<div class="main" id="timelapse">
<div class="video-container">
<div class="video-holder-cs col-md-12">
<div class="embed-container">
<?php the_field('timelapse');?>
</div>
</div></div>
</div></div>

<div class="section fp-auto-height-responsive" id="sectionG">
<div class="my-row">



<?php
wp_reset_query();// necessary to reset query
while (have_posts()):the_post();
the_content();
endwhile;// End of the loop.
?>
<div class="col-lg-3 col-md-6 contact-cs-box left-address-box">
  <div class="left-inner-address">
  <div class="bold-title">Aldershot</div>
  <div class="address-text">Hampshire GU12 4RH<br>
United Kingdom<br>
Mon-Fri 9am - 5pm<br>
Phone: 0125 233 9438</div>

  <div class="bold-title">Wakefield</div>
  <div class="address-text">Yorkshire WF2 8EF<br>
United Kingdom<br>
Mon-Fri 9am - 5pm<br>
Phone: 0870 743 0925</div>

  <div class="bold-title">London</div>
  <div class="address-text">London W1H 1DP<br>
United Kingdom<br>
Mon-Fri 9am - 5pm<br>
Phone: 0208 226 2161</div>

  <div class="bold-title">Manchester</div>
  <div class="address-text">Manchester M50 2ST<br>
United Kingdom<br>
Mon-Fri 9am - 5pm<br>
Phone:0161 401 0833</div>
<div class="address-bottom-box">
<div class="address-fancy">Mon - Fri 9am - 5pm</div>
 <div class="address-fancy">0870 743 0925</div>
 <div class="address-fancy">enquiries@saraceninteriors.com</div>
</div>
</div></div>

<div class="inline-form-cs col-lg-9 col-md-6">
  <div class="form-holder-cs">
<?php echo do_shortcode('[contact-form-7 id="431" title="Get in Touch - Case Studies"]');?>
</div></div>


<div class="cs-footer col-lg-9 col-md-6">
<?php get_footer();?>
</div></div>



</div>


</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->





  <script type="text/javascript">



  	$(document).ready(function(){




 $(document).ready(function() {
  $('#fullpage').fullpage({
    responsiveWidth:1300,
    anchors: ['section1', 'section2', 'section3', 'section4' ,'section5', 'section6', 'section7'],
    sectionsColor: ['#ffffff', 'rgb(97, 97, 97)', '#fbce20', '#ffffff', '#ffffff','#ffffff', '#ffffff']

  });
});


// init Masonry
var $grid = $('.grid').imagesLoaded( function() {
  // init Masonry after all images have loaded
  $grid.masonry({

  itemSelector: '.grid-item',
  columnWidth: 50,
  gutter: 10

  });
});
    });
  </script>
  <script src="/wp-content/themes/marvel-child/js/jquery.flexslider.js"></script>

<script type="text/javascript">

     $(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        slideshowSpeed: 9000,
        controlNav: false,
        directionNav: false,
        animationSpeed: 6000,
        pauseOnHover: true,
        reverse: true,
animationLoop: true,
    });
  });

  </script>
 <script type="text/javascript">$(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });</script>

