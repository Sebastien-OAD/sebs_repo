<?php
/*
 * Template Name: Case Study - change
 * Template Post Type: case_study
 */

get_header();?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


<script type="text/javascript" src="/wp-content/themes/marvel-child/js/masonry.pkgd.min.js"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" integrity="sha256-HAaDW5o2+LelybUhfuk0Zh2Vdk8Y2W2UeKmbaXhalfA=" crossorigin="anonymous" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js" integrity="sha256-jGAkJO3hvqIDc4nIY1sfh/FPbV+UK+1N+xJJg6zzr7A=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="/wp-content/themes/marvel-child/flexslider.css">
<style>
  .embed-container {
    position: relative;
    padding-bottom: 56.25%;
    height: 0;
    overflow: hidden;
    max-width: 100%;
    height: auto;
  }

  .embed-container iframe,
  .embed-container object,
  .embed-container embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
</style>
<!--scripts for full page scroll-->


<div class="get-in-touch"><a href="/contact">Get in touch</a></div>
<div class="saracen-services"><a href="/services/facilities/"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"></a><a href="/services/small-works/"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"></a><a href="/services/move-management/"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"></a></div>

<div class="wrapper" id="page-wrapper">
    <div class="slider-holder-new col-lg-12 col-md-12 col-sm-12">

<?php

$images = get_field('hero_image');

if ($images):?>
<div id="slider" class="flexslider">

        <ul class="slides">
<?php foreach ($images as $image):?>
<li>
    <div class="image-holder"> <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" /> </div>


</li>
<?php endforeach;?>
</ul>
    </div>

<?php endif;?></div>

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">

              <div id="fullpage">

<div class="section" id="sectionA">
   <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Review",
  "datePublished":"<?php the_field('date_of_quote');?>",
  "reviewBody":"<?php echo sanitize_text_field(get_field('quote'));?>",
  "itemReviewed": {
    "@type": "Thing",
    "name": "<?php the_field('project');?>",
    "description":"<?php echo sanitize_text_field(get_field('quote'));?>"

  },
  "author": {
    "@type": "Person",
    "name": "<?php the_field('quote_by');?>"
  },
  "reviewRating": {
    "@type": "Rating",
    "ratingValue": "<?php the_field('rating');?>",
    "bestRating": "100"

  },
  "publisher": {
    "@type": "Organization",
    "name": "Saracen Interiors"
  }
}
</script>





<div class="row cs">
 <div class="col-lg-4 col-md-12 col-sm-12 left-column-cs-new">
  <div class="left-inner-cs ">

<h1><?php the_title();?></h1>
<div class="parameters">
                  <div class="parameter">
<?php the_field('location');?></div>
                   <div class="parameter">
<?php the_field('project');?></div>
            <div class="parameter">
<?php the_field('project_size');?></div>
            <div class="parameter">
<?php the_field('timeline');?></div>
           </div>

<div class="col-quicklinks quick-links">
                  <li><a href="#sectionE">Gallery</a></li>
<?php $video3 = get_field('video');?>
                  <?php if ($video3) {?>
																																																	<li><a href="#sectionC">Videos</a></li> <?php }?>
<?php $timelapse2 = get_field('timelapse');?>
<?php if ($timelapse2) {?>
																																														<li><a href="#sectionD">Timelapse</a></li> <?php }?>
</div>
            <div class="embed-container">

<?php
$video = get_field('video');
if ($video) {
	preg_match('/src="(.+?)"/', $video, $matches_url);
	$src = $matches_url[1];

	preg_match('/embed(.*?)?feature/', $src, $matches_id);
	$id = $matches_id[1];
	$id = str_replace(str_split('?/'), '', $id);

	?>
																									<div class="utube-image">
																									<a href="https://www.youtube.com/watch?v=<?php echo $id;?>" data-toggle="lightbox"><img src="http://img.youtube.com/vi/<?php echo $id;?>/mqdefault.jpg"></a>
																									</div>  <?php }?>
</div>
</div>

</div>


<div class="cs-body col-lg-8 col-md-12 col-sm-12"><?php the_field('case_study_body');?></div>
<div class="quote-inner-new col-lg-8 col-md-12 col-sm-12">
<div class="quote-body-text-new"><?php the_field('quote');?></div>
<div class="quote-by-text-new"><?php the_field('quote_by');?></div>

<!--from here-->
</div></div></div>
<div class="mobile-padding">
<div class="section" id="sectionE">
  <div class="underline-title"><h2>Gallery</h2></div>
<div class="gallery-container">
<div class="grid">


<?php

$images = get_field('gallery');

if ($images):?>
 <?php $toggle_class = 'even';?>
<?php foreach ($images as $image):?>
  <?php $toggle_class = ($toggle_class == 'odd'?'even':'odd');?>
          <div class="grid-item <?php echo $toggle_class;?> ">
                <a href="<?php echo $image['url'];?>" data-toggle="lightbox">
                     <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" />
                </a>
                </div>

<?php endforeach;?>

<?php endif;?>
</div>
</div>
</div>






<?php $video2 = get_field('video');?>
<?php if ($video2) {?>
	<div class="section" id="sectionD">
											    <div class="underline-title"><h3>Video</h3></div>
																																							<div class="main" id="videos">
																																							<div class="video-container">
																																							<div class="col-md-12 video-holder-cs">
																																						<div class="embed-container">
	<?php the_field('video');?>
																																						</div>
																																						</div>
																																						</div>
																																						</div>
																																						</div><?php }?>


<?php $timelapse = get_field('timelapse');?>
<?php if ($timelapse) {?><div class="section" id="sectionF">

																										<div class="main" id="timelapse">
									                                     <div class="underline-title"><h4>Timelapse</h4></div>
																										<div class="video-container">
																										<div class="video-holder-cs col-md-12">
																										<div class="embed-container">
	<?php the_field('timelapse');?>
																										</div>
																										</div></div>
																										</div></div> <?php }?>
</div>
<div class="section" id="sectionG">
<div class="my-row">



<?php
wp_reset_query();// necessary to reset query
while (have_posts()):the_post();
the_content();
endwhile;// End of the loop.
?>
<div class="case-bottom col-md-12">
<div class="form-holder-cs col-md-12">

<?php echo do_shortcode('[contact-form-7 id="431" title="Get in Touch - Case Studies"]');?>
</div>
<div class="col-lg-9 col-md-6">
<?php echo do_shortcode("[osd_social_media_sharing]");?></div>
<div class="col-lg-2 col-md-3">
<div class="btn btn-primary"><a href="/case-studies/">Back to Case Studies</a></div>
  </div>
</div>


<div class="cs-footer col-lg-12">
<?php get_footer();?>
</div></div>



</div>


</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->





  <script type="text/javascript">




// init Masonry
var $grid = jQuery('.grid').imagesLoaded( function() {
  // init Masonry after all images have loaded
  $grid.masonry({

  itemSelector: '.grid-item',
  columnWidth: 50,
  gutter: 10

  });
});

  </script>


  <script src="/wp-content/themes/marvel-child/js/jquery.flexslider.js"></script>

<script type="text/javascript">





     jQuery(window).load(function() {
    jQuery('.flexslider').flexslider({
        animation: "slide",
        slideshowSpeed: 9000,
        controlNav: false,
        directionNav: false,
        animationSpeed: 6000,
        pauseOnHover: true,
        reverse: true,
animationLoop: true,
    });
  });

  </script>
 <script type="text/javascript">jQuery(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                jQuery(this).ekkoLightbox();
            });</script>

