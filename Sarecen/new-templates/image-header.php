<?php
/*
 * Template Name: Saracen - image header(what we do)
 *
 */

get_header();?>
<div class="get-in-touch"><span class="popmake-get-in-touch-global">Get in touch</span></div>
<div class="saracen-services"><a href="/services/facilities/"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"></a><a href="/services/small-works/"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"></a><a href="/services/move-management/"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"></a></div>


<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">
                <div class="row">




<div class="news-hero col-md-12 col-sm-12">
<?php $image_header = get_field('image_header');?>
<?php if ($image_header) {?>
											<img src="<?php echo $image_header['url'];?>" alt="<?php echo $image_header['alt'];?>" />
	<?php }?>
</div>

<div class="col-md-12 col-sm-12 content-column-news">

<?php $service_icon = get_field('service_icon');?>
<?php if ($service_icon) {?>


					<script type='application/ld+json'>
					{
					  "@context": "http://schema.org/",
					  "@type": "Service",
					    "provider": {
					    "@type": "LocalBusiness",
					    "name": "Saracen Interiors",
					    "telephone":"08707430925",
					    "address":{"@type":"PostalAddress","streetAddress":"78 York Street, Marylebone","addressLocality":"London","postalCode":"W1H 1DP","addressCountry":"GB"},
					    "image":"http://www.officerefurbishment.site/wp-content/uploads/2018/02/logo-1.png"
					  },
					  "areaServed": "United Kingdom",
					  "serviceType": "<?php the_field('service_title');?>",
					  "alternateName": "<?php the_field('service_title');?>",
					  "description": "<?php echo sanitize_text_field(get_field('service_description'));?>",
					  "image": {
					    "@type": "ImageObject",
					    "contentUrl": "<?php echo $service_icon['url'];?>",
					    "embedUrl": "<?php echo $service_icon['url'];?>"
					  },
					  "mainEntityOfPage": "<?php echo get_permalink()?>",
					  "name": "<?php the_field('service_title');?>"
					}
					</script>
	<?php }?>


<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?>
</div>



</main><!-- #main -->

</div><!-- #primary -->

</div><!-- Container end -->
</div><!-- Wrapper end -->
<?php get_footer();?>

</script>