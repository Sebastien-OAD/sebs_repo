<?php
/*
 * Template Name: News Feed
 *
 */

get_header();

?>
<div class="get-in-touch"><span class="popmake-get-in-touch-global">Get in touch</span></div>
<div class="saracen-services"><a href="/services/facilities/"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"></a><a href="/services/small-works/"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"></a><a href="/services/move-management/"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"></a></div>

<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">
            	<div class="news-page-content"><?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?></div>
            	<div class="saracen-news-feed col-md-12">

<div class="ui-group saracen-news-filters">
    <div class="ui-group__title s-n-f">Filter Blogs by Category</div>
    <div class="filter-button-group js-radio-button-group news-buttons-left">
      <button class="btn btn-lg btn-secondary is-checked" data-filter="*">show all</button>

<?php
$terms = get_terms("news_category");
$count = count($terms);
if ($count > 0) {
	foreach ($terms as $term) {

		?> <button class="btn btn-lg btn-secondary" data-filter=".<?php echo $term->slug?>">  <?php echo $term->name?> </button><?php
	}
}
?>
</div>
  </div>

<!-- swapping classes for grid -->
<?php $class_name = array('big', 'small', 'wide', 'long');
$arrKey           = 0;?>

<?php

// args

$loop = new WP_Query(array('post_type' => 'news_article', 'posts_per_page' => -1, 'orderby' => 'news_date', 'order' => 'DESC'));

// query

if ($loop->have_posts()):?>
<div class="grid">
  <div class="grid-sizer"></div>

<?php while ($loop->have_posts()):$loop->the_post();?>
  <?php $format_in = 'd/m/Y';// the format your value is saved in (set in the field options)
$format_out_2      = 'd F Y';// the format you want to end up with

$termsArray  = get_the_terms($post->ID, "news_category");
$termsString = "";//initialize the string that will contain the terms
foreach ($termsArray as $term) {// for each term
	$termsString .= $term->slug.' ';//create a string that has all the slugs
};

$date2 = DateTime::createFromFormat($format_in, get_field('article_date'));?>

<div class="saracen-news-article <?php echo $termsString;?> grid-item grid-item-<?php echo $class_name[$arrKey]?>">
<?php if (get_field('news_hero_image')) {?>
																																		<a href="<?php the_permalink();?>"><div class="saracen-news-image">
																																				<img src="<?php the_field('news_hero_image');?>" />
																																			</div></a>
	<?php }?>
<div class="saracen-news-inner">
<div class="saracen-news-title"> <a href="<?php the_permalink();?>"><?php the_field('news_headline');
?></a></div>

<div class="saracen-news-date"><?php echo $date2->format($format_out_2);?></div>

</div>
<div class="overlay-news">
<div class="saracen-news-summary"><?php the_field('news_summary');?>
<div class="saracen-news-more"><div class="btn btn-lg btn-outline-info"><a href="<?php the_permalink();?>">Read more</a></div></div>
</div></div>
<script type="application/ld+json">{
"@context": "http://schema.org",
"@type": "NewsArticle",
"mainEntityOfPage": {
         "@type": "WebPage",
         "@id": "<?php echo the_permalink()?>"
      },
"headline": " <?php the_title();?>",
"author": "<?php $author = get_field('author');?>",
"articleBody": "<?php the_field('news_summary');?>",
"datePublished": "<?php echo get_the_date('Y-m-d');?>",
"dateModified":"<?php the_modified_date('Y-m-d');?>",
"image": {
"@type": "imageObject",
"url": "<?php the_field('news_hero_image');?>"
},
"publisher": {
"@type": "Organization",
"name": "Saracen Interiors",
"logo": {
"@type": "imageObject",
"url": "http://www.officerefurbishment.site/wp-content/uploads/2018/02/logo-1.png"
}
}
}</script>

</div>
<?php $arrKey++;?>
<?php if ($arrKey >= '4') {

	$arrKey = 0;
}?>

<?php endwhile;?>
</div></div>
<?php endif;?>

<?php wp_reset_query();// Restore global post data stomped by the_post(). ?>
</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();?>
<!--<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>-->
<script src="/wp-content/themes/marvel-child/js/isotope.pkgd.min.js"></script>
<script src="/wp-content/themes/marvel-child/js/packery-mode.pkgd.min.js"></script>

<script type="text/javascript">
jQuery(document).ready(function(){
jQuery('.grid').isotope({
  // set itemSelector so .grid-sizer is not used in layout
  itemSelector: '.grid-item',
  percentPosition: true,
  layoutMode: 'packery',
  masonry: {
    // use element for option
    columnWidth: '.grid-sizer',

  }
})
// init Isotope
var $grid = jQuery('.grid').isotope({
  // options
});
// filter items on button click
jQuery('.filter-button-group').on( 'click', 'button', function() {
  var filterValue = jQuery(this).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});

});
</script>