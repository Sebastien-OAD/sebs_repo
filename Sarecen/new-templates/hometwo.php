<?php
/* Template Name: home-page2 */
get_header();?>
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->

<div class="sar-top-logo"><img src="/wp-content/uploads/bits/sar-dark.png"></div>
<div class="get-in-touch">Get in touch</div>
<div class="wrapper" id="page-wrapper">
<div id="left"></div>
<div id="right"></div>
<div id="top">

<div class="saracen-services"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"><!--<img src="/wp-content/uploads/bits/logo-saracen-interiors.png">--></div></div>


<div id="bottom"></div>

<div class="row">
       	<div class="yellow-band col-md-3 col-sm-6 col-xs-12">
       		<div class="outter-column">
       			<div class="shoosh"></div>
	       	    <div class="inner-column-b">
	       	    	<h1> We create exceptional workplaces for business, from concept through to completion</h1>


	            </div>
	        </div>
        </div>
       	  <div class="image-band col-md-9 col-sm-6 col-xs-12">
<?php putRevSlider("home-hero")?>
<!--<img src="https://www.saraceninteriors.com/assets/media/140937-Sirius-Scarborough-133-LOW-RES.jpg">-->

       	  	</div>
</div>



    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">


            <main id="main" class="site-main" role="main">


<?php while (have_posts()):the_post();?>

<?php get_template_part('loop-templates/content', 'page');?>

<?php
// If comments are open or we have at least one comment, load up the comment template
if (comments_open() || get_comments_number()):

comments_template();

endif;
?>

<?php endwhile;// end of the loop. ?>
</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();?>