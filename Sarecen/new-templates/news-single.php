<?php
/*
 * Template Name: News - single
 * Template Post Type: news_article
 */

get_header();?>
<div class="get-in-touch"><a href="/contact">Get in touch</a></div>
<div class="saracen-services"><a href="/services/facilities/"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"></a><a href="/services/small-works/"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"></a><a href="/services/move-management/"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"></a></div>


<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">
                <div class="row">
<?php $image_column = get_field('image_column');?>
<div class="col-md-3 col-sm-12 col-holder-news">

<div class="full-height-image-column-news">
  <div class="news-feed-inner">
<div class="feed-title">Latest news</div>
<?php

// args
$currentID = get_the_ID();//removes current viewed post from the feed, using post no in var array
$loop      = new WP_Query(array('post_type' => 'news_article', 'posts_per_page' => 10, 'orderby' => 'news_date', 'order' => 'DESC', 'post__not_in' => array($currentID)));

// query

if ($loop->have_posts()):?>

<?php while ($loop->have_posts()):$loop->the_post();?>
  <?php $format_in = 'd/m/Y';// the format your value is saved in (set in the field options)
$format_out_2      = 'd F Y';// the format you want to end up with

$date2 = DateTime::createFromFormat($format_in, get_field('article_date'));?>
<div class="latest-news-title"> <a href="<?php the_permalink();?>"><?php the_field('news_headline');
?></a></div>

<div class="latest-news-date"><?php echo $date2->format($format_out_2);?></div>

<?php endwhile;?>
<?php endif;?>
<?php wp_reset_query();// Restore global post data stomped by the_post(). ?>
</div>
</div></div>
<div class="news-hero col-md-9 col-sm-12">
<?php if (get_field('news_hero_image')) {?>
																																						    <img src="<?php the_field('news_hero_image');?>" />
	<?php }?></div>
<div class="col-md-9 col-sm-12 content-column-news">
<?php $format_in = 'd/m/Y';// the format your value is saved in (set in the field options)
$format_out_1    = 'd F Y';// the format you want to end up with

$date = DateTime::createFromFormat($format_in, get_field('article_date'));?>

  <div class="col-md-12 news-primary-row">
    <div class="col-md-9 news-headline"><h1><?php the_field('news_headline');?></h1></div>
    <div class="col-md-3 news-date-top"><?php echo $date->format($format_out_1);?>
</div>


  </div>
<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<script type="application/ld+json">{
"@context": "http://schema.org",
"@type": "NewsArticle",
"mainEntityOfPage": {
         "@type": "WebPage",
         "@id": "<?php echo the_permalink()?>"
      },
"headline": " <?php the_title();?>",
"author": "<?php $author = get_field('author');?>",
"articleBody": "<?php echo sanitize_text_field(get_field('news_summary'));?>",
"datePublished": "<?php echo get_the_date('Y-m-d');?>",
"dateModified":"<?php the_modified_date('Y-m-d');?>",
"image": {
"@type": "imageObject",
"url": "<?php the_field('news_hero_image');?>"
},
"publisher": {
"@type": "Organization",
"name": "Saracen Interiors",
"logo": {
"@type": "imageObject",
"url": "http://www.officerefurbishment.site/wp-content/uploads/2018/02/logo-1.png"
}
}
}</script>
<?php endwhile;
endif;
?>
<div class="next-blog-controls"><?php previous_post_link();
?> |  <?php next_post_link();
?></div>
<div class="col-lg-12 col-md-12 news-button-area">
<div class="git-button"><a href="/contact/">Get in touch</a></div>
  </div>

</div>
</div>


            </main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->
</div><!-- Wrapper end -->
<?php get_footer();?>
<!--<script
  src="http://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>-->

<script type="text/javascript" src="/wp-content/themes/marvel-child/js/jquery.matchHeight.js"></script>


<script type="text/javascript">


jQuery( document ).ready(function() {
  var isMobile = window.matchMedia("only screen and (max-width: 990px)");

  if (isMobile.matches) {

    }
    else {
jQuery(function() {
    jQuery('.col-holder-news').matchHeight({
        target: jQuery('.site-main')
    });
});

    }
 });




</script>