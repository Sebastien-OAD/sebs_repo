<?php

/* Template Name: News Article Loop */

get_header();?>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="http://localhost:8888/saracen/wp-content/themes/marvel/js/theta-carousel.min.js"></script>


<div id="left"></div>
<div id="right"></div>
<div id="top"></div>
<div id="bottom"></div>
<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">

<div class="summary-holder col-lg-3 col-md-12 col-sm-12 col-xs-12"><h1><?php the_title();
?></h1><p><?php the_content();
?></p></div>
            	<div class="feed-inner col-lg-9 col-md-12 col-sm-12 col-xs-12">
            	<div class="news-feed">


<?php

// args
$args = array(
	'numberposts' => -1,
	'post_type'   => 'news_article',
	'meta_key'    => 'news_date',
	'order_by'    => 'news_date',
	'order'       => 'ASC',

);

// query
$the_query = new WP_Query($args);

?>
<?php if ($the_query->have_posts()):?>

<?php while ($the_query->have_posts()):$the_query->the_post();?>

<?php

$format_in    = 'd/m/Y';// the format your value is saved in (set in the field options)
$format_out_1 = 'd';// the format you want to end up with
$format_out_2 = 'M';// the format you want to end up with
$format_out_3 = 'Y';// the format you want to end up with

$date = DateTime::createFromFormat($format_in, get_field('news_date'));

?>
<div class="news-post-article item col-xs-12">
<?php if (get_field('news_hero_image')) {?>
																																											<a href="<?php the_permalink();?>"><img src="<?php the_field('news_hero_image');?>" /></a>
	<?php }?>

								<div class="news-bottom">
									<div class="news-date-area-l">
		            		        	<div class="news-day-l"><?php echo $date->format($format_out_1);
?> <?php echo $date->format($format_out_2);
?> </div>
										<div class="news-year-l"><?php echo $date->format($format_out_3);?> </div>
									</div>

								<div class="news-title"> <h3><a href="<?php the_permalink();?>"><?php the_field('news_headline');
?></a></h3></div></div>
<div class="news-summary-side"><?php the_field('news_summary');?></div>


									</div>


<?php endwhile;?>

<?php endif;?>

<?php wp_reset_query();// Restore global post data stomped by the_post(). ?>
</div>
<div class="news-feed-2">

<?php

// args
$args = array(
	'numberposts' => -1,
	'post_type'   => 'news_article',
	'meta_key'    => 'news_date',
	'order_by'    => 'news_date',
	'order'       => 'ASC',
);

// query
$the_query_mobile = new WP_Query($args);

?>
<?php if ($the_query_mobile->have_posts()):?>

<?php while ($the_query_mobile->have_posts()):$the_query_mobile->the_post();?>

<?php

$format_in    = 'd/m/Y';// the format your value is saved in (set in the field options)
$format_out_1 = 'd';// the format you want to end up with
$format_out_2 = 'M';// the format you want to end up with
$format_out_3 = 'Y';// the format you want to end up with

$date = DateTime::createFromFormat($format_in, get_field('news_date'));

?>
<div class="news-post-article mobile col-xs-12">
<?php if (get_field('news_hero_image')) {?>
																				<a href="<?php the_permalink();?>"><img src="<?php the_field('news_hero_image');?>" /></a>
	<?php }?>

								<div class="news-bottom">
									<div class="news-date-area-l">
		            		        	<div class="news-day-l"><?php echo $date->format($format_out_1);
?> <?php echo $date->format($format_out_2);
?> </div>
										<div class="news-year-l"><?php echo $date->format($format_out_3);?> </div>
									</div>

								<div class="news-title"> <h3><a href="<?php the_permalink();?>"><?php the_field('news_headline');
?></a></h3></div></div>
<div class="news-summary-side"><?php the_field('news_summary');?></div>


									</div>


<?php endwhile;?>

<?php endif;?>

<?php wp_reset_query();// Restore global post data stomped by the_post(). ?>
</div><!---end of feed-2-->
</div>
</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->


<script type="text/javascript">

$('.news-feed').theta_carousel( {
        selectedIndex: 0,
        distance: 103,
        numberOfElementsToDisplayRight: 5,
        numberOfElementsToDisplayLeft: 6,
        designedForWidth: 800,
        designedForHeight: 900,
        distanceInFallbackMode: 650,
        scaleX: 1.05,
        scaleY: 0.62,
        scaleZ: 0,
        path: {
                settings: {
                        shiftZ: 0,
                        rotationAngleXY: 6,
                        rotationAngleZX: 3,
                        wideness: 178,
                        endless: true
                },
                type: 'cubic'
        },
        perspective: 10000,
        mode3D: 'scale',
        inertiaFriction: 7,
        inertiaHighFriction: 28,
        fadeAway: true,
        fadeAwayBezierPoints: {
                p1: {
                        x: 0,
                        y: 100
                },
                p2: {
                        x: 32,
                        y: 35
                },
                p3: {
                        x: 50,
                        y: 50
                },
                p4: {
                        x: 100,
                        y: 0
                }
        },
        sizeAdjustment: true,
        sizeAdjustmentNumberOfConfigurableElements: 7,
        sizeAdjustmentBezierPoints: {
                p1: {
                        x: 0,
                        y: 100
                },
                p2: {
                        x: 30,
                        y: 0
                },
                p3: {
                        x: 378,
                        y: 36
                },
                p4: {
                        x: 100,
                        y: 101
                }
        }

});







</script>





<?php get_footer();?>



