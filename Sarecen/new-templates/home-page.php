<?php
/* Template Name: home-page */
get_header();?>
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->


<div class="get-in-touch"><span class="popmake-get-in-touch-global">Get in touch</span></div>
<div class="image-band col-md-12 col-sm-6 col-xs-12">
<?php putRevSlider("hero-2")?>
<!--<img src="https://www.saraceninteriors.com/assets/media/140937-Sirius-Scarborough-133-LOW-RES.jpg">-->

            </div>
<div class="wrapper" id="page-wrapper">

<div class="saracen-services"><a href="/services/facilities/"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"></a><a href="/services/small-works/"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"></a><a href="/services/move-management/"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"></a></div>


<div class="row">
       	<!--<div class="yellow-band col-md-3 col-sm-6 col-xs-12">
       		<div class="outter-column">
       			<div class="shoosh"></div>
	       	    <div class="inner-column-b">
	       	    	<h1> We create exceptional workplaces for business, from concept through to completion</h1>


	            </div>
	        </div>
        </div>-->

</div>



    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">


            <main id="main" class="site-main" role="main">

<div class="col-md-12 home-text-new">
  <h1> We create exceptional workplaces for business, from concept through to completion</h1>

<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?>
</div>

<!--start of news-->
<div class="home-page-news-feed col-md-12">
  <div class="news-title-hp">What's new</div>
<?php $args2 = array(
	'posts_per_page' => 4,
	'post_type'      => 'news_article',
	'orderby'        => 'news_date',
	'order'          => 'DESC',

);
$the_query2 = new WP_Query($args2);?>
<?php if ($the_query2->have_posts()):?>
<div class="news-feed-hp-inner">
<?php while ($the_query2->have_posts()):$the_query2->the_post();?>
<article class="saracen-hp-news col-lg-3 col-md-6 col-sm-6">
<?php if (get_field('news_hero_image')) {?>
															                  <a href="<?php the_permalink();?>"><div class="saracen-news-image">
															                    <img src="<?php the_field('news_hero_image');?>" />
															                   </div></a>
	<?php }?>
  <div class="saracen-news-inner-hp">
<div class="saracen-news-title"> <a href="<?php the_permalink();?>"><?php the_field('news_headline');
?></a></div>
<div class="news-summary-hp"><?php the_field('news_summary');?></div>

<div class="saracen-news-more"><div class="btn btn-lg btn-outline-info"><a href="<?php the_permalink();?>">Read more</a></div></div>

</div>
</article>

<?php endwhile;?>

<?php endif;?>

<?php wp_reset_query();// Restore global post data stomped by the_post(). ?>
</div></div>
<!--end of news-->



<!--testimonails feed -->

<div class="case-feed col-md-12">
  <div class="fancy-title-text">Testimonials</div>


<?php

// args
$args = array(
	'posts_per_page' => -1,
	'post_type'      => 'case_study',
	'orderby'        => 'post_date',
	'order'          => 'ASC',

);

// query
$the_query = new WP_Query($args);?>
<?php if ($the_query->have_posts()):?>
<div class="owl-carousel owl-theme">
<?php while ($the_query->have_posts()):$the_query->the_post();?>
<div class="testimonial-cs-hp">


<div class="hp-quote-body">"<?php the_field('quote');?>"</div>
<div class="hp-quote-by"><?php the_field('quote_by');?></div>
</div>


<?php endwhile;?>
</div>

<?php endif;?>

<?php wp_reset_query();// Restore global post data stomped by the_post(). ?>
</div>
<div class="awards-home col-md-12">
  <div class="col-md-8"></div>
<div class="col-md-2 awards-col"><img src="http://www.officerefurbishment.site/wp-content/uploads/2018/03/mixology-16-FINALIST.jpg"></div>
<div class="col-md-2 awards-col"><img src="http://www.officerefurbishment.site/wp-content/uploads/2018/03/mixologylogofinalistinsta.jpg"></div>
</div>



<!--end of testi-->
</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();?>
<script src="/wp-content/themes/marvel-child/js/owl.carousel.min.js"></script>
<link rel="stylesheet" href="/wp-content/themes/marvel-child/owl.carousel.min.css" />
<script type="text/javascript">
  jQuery(document).ready(function(){
  var owl = jQuery('.owl-carousel');
owl.owlCarousel({
    items:3,
    loop:true,
     nav:false,
    margin:10,
    dots:true,
    dotsEach:true,
     responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
      },
    autoplay:true,
    autoplayTimeout:10000,
    autoplayHoverPause:true
});
jQuery('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[1000])
})
jQuery('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})
});


</script>


