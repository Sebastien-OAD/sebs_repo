<?php
/* Template Name: services-test */
get_header();?>

<?php
if (wp_is_mobile()) {
	/* Display and echo mobile specific stuff here */
} else {
	?>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
																				<script src="/wp-content/themes/marvel-child/js/jquery.fullPage.js"></script>
																	<!--<script type="text/javascript" src="/wp-content/themes/marvel-child/js/fullpage.fadingEffect.limited.min.js"></script>-->

	<?php

}
?>
<link rel="stylesheet" type="text/css" href="/wp-content/themes/marvel-child/jquery.fullPage.css" />

<script type="text/javascript" src="/wp-content/themes/marvel-child/js/scrolloverflow.min.js"></script>



<div class="left-right"><saracen-left >&larr;
</saracen-left> | <saracen-right class="animated infinite pulse">&rarr;
</saracen-right> </div>

<div class="get-in-touch"><a href="/contact">Get in touch</a></div>
<div class="saracen-services"><a href="/services/facilities/"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"></a><a href="/services/small-works/"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"></a><a href="/services/move-management/"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"></a></div>

<div class="wrapper" id="page-wrapper">

    <div  id="content-ss" class="container-ss">

       <div id="primary" class=" content-area">

            <main id="main" class="site-main" role="main">
<?php $service_icon = get_field('service_icon');?>
<script type='application/ld+json'>
{
	"@context": "http://schema.org/",
	"@type": "Service",
    "provider": {
    "@type": "LocalBusiness",
    "name": "Saracen Interiors",
    "telephone":"08707430925",
    "address":{"@type":"PostalAddress","streetAddress":"78 York Street, Marylebone","addressLocality":"London","postalCode":"W1H 1DP","addressCountry":"GB"},
    "image":"http://www.officerefurbishment.site/wp-content/uploads/2018/02/logo-1.png"
  },
	"areaServed": "United Kingdom",
	"serviceType": "<?php the_field('service_title');?>",
	"alternateName": "<?php the_field('service_title');?>",
	"description": "<?php the_field('service_description');?>",
	"image": {
		"@type": "ImageObject",
		"contentUrl": "<?php echo $service_icon['url'];?>",
		"embedUrl": "<?php echo $service_icon['url'];?>"
	},
	"mainEntityOfPage": "<?php echo get_permalink()?>",
	"name": "<?php the_field('service_title');?>"
}
</script>



            	<div id="fullpage">
            		<div class="section">
						<div class="slide"><div class="slider-container"><img src="<?php the_field('section_1_image');?>" /><div class="slider-text-1 "><?php the_field('section_2_text');
?></div></div></div>
						<!--<div class="slide spt"><div class="slider-page-text"><?php the_field('section_2_text');?></div></div>-->
						<div class="slide"><div class="slider-container"><img src="<?php the_field('section_3_image');?>" /><div class="slider-text-2 "><?php the_field('section_4_text');
?></div></div></div>
						<!--<div class="slide spt"> <div class="slider-page-text"><?php the_field('section_4_text');?> </div></div>-->
						<div class="slide"><div class="slider-container"><img src="<?php the_field('section_5_image');?>"/> <div class="slider-text-3 "><?php the_field('section_6_text');
?></div> </div></div>
						<!--<div class="slide spt"><div class="slider-page-text"> <?php the_field('section_6_text');?> </div></div>-->
						<div class="slide"><div class="slider-container"><img src="<?php the_field('section_7_image');?>"/><div class="slider-text-4 "><?php the_field('section_8_text');
?></div></div></div>
<div class="slide"><div class="slider-container">

<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?>
<?php get_footer();?>
</div>
</div>



</div></div></div>
						<!--<div class="slide spt"><div class="slider-page-text"> <?php the_field('section_8_text');?></div></div>-->

					</div>
				</div>



			</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" integrity="sha256-j+P6EZJVrbXgwSR5Mx+eCS6FvP9Wq27MBRC/ogVriY0=" crossorigin="anonymous" />
<script type="text/javascript">
	$( "saracen-left" ).click(function() {
  $.fn.fullpage.moveSlideLeft();
});
$( "saracen-right" ).click(function() {
  $.fn.fullpage.moveSlideRight();
});


	$( document ).ready(function() {
		var isMobile = window.matchMedia("only screen and (max-width: 760px)");

    if (isMobile.matches) {

    }
    else {

$('#fullpage').fullpage({
		responsiveWidth:900,
		scrollOverflow: true,
		autoScrolling: true,
		afterRender: function(){

             setInterval(function() {
	          $.fn.fullpage.moveSlideRight();
              }, 60000);

		}
	});
}

 });











</script>

