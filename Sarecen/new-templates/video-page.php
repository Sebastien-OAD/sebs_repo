<?php
/*
 * Template Name: Saracen - video page
 *
 */

get_header();?>
<div class="get-in-touch"><span class="popmake-get-in-touch-global">Get in touch</span></div>
<div class="saracen-services"><a href="/services/facilities/"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"></a><a href="/services/small-works/"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"></a><a href="/services/move-management/"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"></a></div>


<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">
                <div class="row">




<div class="news-hero col-md-12 col-sm-12">
<?php $image_header = get_field('image_header');?>
<?php if ($image_header) {?>
																						<img src="<?php echo $image_header['url'];?>" alt="<?php echo $image_header['alt'];?>" />
	<?php }?>
</div>

<div class="col-md-12 col-sm-12 content-column-news video-page">
<div class="upper-video col-md-12">
<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?>
</div>
<?php

// args
$args = array(
	'posts_per_page' => -1,
	'post_type'      => 'case_study',
	'orderby'        => 'post_date',
	'order'          => 'DESC',
	'meta_query'     => array(

		'relation' => 'OR',
		array(
			'key'     => 'video',
			'value'   => '',
			'compare' => '!=',
		),
		array(
			'key'     => 'timelapse',
			'value'   => '',
			'compare' => '!=',
		),
	),
);

// query
$the_query = new WP_Query($args);?>
<div class="video-page col-md-12">
<?php if ($the_query->have_posts()):?>

<?php while ($the_query->have_posts()):$the_query->the_post();?>
<div class="video-item col-md-12">

<?php if (get_field('video') || get_field('timelapse')) {
	?>
									<div class="cs-title-feed"><?php the_title();?></div>
									<div class="video-field col-md-6"><?php if (get_field('video')) {?><div class="video-title">Video</div> <?php {the_field('video');}
	}
	?></div>
									<div class="timelapse-field col-md-6"><?php if (get_field('timelapse')) {?><div class="video-title">Timelapse</div> <?php {the_field('timelapse');}
	}
	?></div>
									<div class="video-summary"><?php the_field('summary_/_description');?></div>



	<?php

}?>
</div>


<?php endwhile;?>
</div></div></div>

<?php endif;?>

<?php wp_reset_query();// Restore global post data stomped by the_post(). ?>
</div>



</main><!-- #main -->

</div><!-- #primary -->

</div><!-- Container end -->
</div><!-- Wrapper end -->
<?php get_footer();?>

</script>