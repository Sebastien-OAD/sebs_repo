<?php
/*
 * Template Name: News Article-test
 * Template Post Type: news_article
 */
$format_in    = 'd/m/Y';// the format your value is saved in (set in the field options)
$format_out_1 = 'd';// the format you want to end up with
$format_out_2 = 'F';// the format you want to end up with
$format_out_3 = 'Y';// the format you want to end up with

$date = DateTime::createFromFormat($format_in, get_field('news_date'));

get_header();?>
<div id="left"></div>
<div id="right"></div>
<div id="top"></div>
<div id="bottom"></div>
<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">
            	<div class="news-article">

            		<div class="container table-container">
  <div class="row table-row">



		            	<div class="news-left col-md-4 table-col">
								<div class="news-date-area">
		            		        <div class="news-day"><?php echo $date->format($format_out_1);?> </div>
									<div class="news-month"><?php echo $date->format($format_out_2);?></div>
									<div class="news-year"><?php echo $date->format($format_out_3);?> </div>
								</div>


								<div class="news-summary">
									<h1><?php the_field('news_summary');?>.....</h1>


								</div>

								<div class="social-share me">
									<h2>Share us</h2>
									<i class="fa fa-facebook fa-4x" aria-hidden="true"></i>
									<i class="fa fa-twitter fa-4x" aria-hidden="true"></i>
									<i class="fa fa-linkedin fa-4x" aria-hidden="true"></i>



								</div>

						</div>


							<div class="news-right col-md-8 table-col">
								<div class="right-inner">

<?php if (get_field('news_hero_image')) {?>
																						<img src="<?php the_field('news_hero_image');?>" />
	<?php }?>

								<div class="news-body-inner">
									<h1><?php the_field('news_headline');?></h1>
								  <p><?php the_field('news_body');?></p> </div>





								</div>
							</div>
						</div>
</div>
</div>









</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();?>