<?php

/* Template Name: Case studies feed */

get_header();?>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="/wp-content/themes/marvel-child/js/theta-carousel.min.js"></script>

<div class="get-in-touch"><span class="popmake-get-in-touch-global">Get in touch</span></div>
<div class="saracen-services"><a href="/services/facilities/"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"></a><a href="/services/small-works/"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"></a><a href="/services/move-management/"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"></a></div>


<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">

<div class="summary-holder col-lg-3 col-md-12 col-sm-12 col-xs-12"><h1><?php the_title();
?></h1><p><?php the_content();
?></p></div>
            	<div class="feed-inner col-lg-9 col-md-12 col-sm-12 col-xs-12">
            	<div class="case-feed">


<?php

// args
$args = array(
	'posts_per_page' => -1,
	'post_type'      => 'case_study',
	'orderby'        => 'post_date',
	'order'          => 'DESC',

);

// query
$the_query = new WP_Query($args);?>
<?php if ($the_query->have_posts()):?>

<?php while ($the_query->have_posts()):$the_query->the_post();?>
<div class="case-post-article item ">

<?php $body_background = get_field('body_background');?>
<?php if ($body_background) {?>

																						<div class="cs-image-main">	<a href="<?php the_permalink();?>"><img src="<?php echo $body_background['url'];?>" alt="<?php echo $body_background['alt'];?>" /></a></div>
	<?php }?>
  <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Review",
  "datePublished":"<?php the_field('date_of_quote');?>",
  "reviewBody":"<?php echo sanitize_text_field(get_field('quote'));?>",
  "itemReviewed": {
    "@type": "Thing",
    "name": "<?php the_field('project');?>",
    "description":"<?php echo sanitize_text_field(get_field('quote'));?>"

  },
  "author": {
    "@type": "Person",
    "name": "<?php the_field('quote_by');?>"
  },
  "reviewRating": {
    "@type": "Rating",
    "ratingValue": "<?php the_field('rating');?>",
    "bestRating": "100"

  },
  "publisher": {
    "@type": "Organization",
    "name": "Saracen Interiors"
  }
}
</script>

<div class="cs-title-feed"><?php the_title();?></div>
<div class="cs-summary"><?php the_field('summary_/_description');?></div>
</div>


<?php endwhile;?>
</div></div>

<?php endif;?>

<?php wp_reset_query();// Restore global post data stomped by the_post(). ?>
</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->
<?php get_footer();?>
<script type="text/javascript">
 $( document ).ready(function() {
    var isMobile = window.matchMedia("only screen and (max-width: 760px)");

    if (isMobile.matches) {

    }
    else {
$('.case-feed').theta_carousel( {



        distance: 180,
        numberOfElementsToDisplayRight: 5,
        numberOfElementsToDisplayLeft: 7,
        designedForWidth: 1130,
        designedForHeight: 1000,
        distanceInFallbackMode: 350,
        scaleX: 1.70,
        scaleY: 0.90,
        scaleZ: 0.93,
        path: {
                settings: {
                        shiftZ: -222,
                        wideness: 160,
                        endless: true,
                        rotationAngleXY: 7,
                         shiftY: -40
                },
                type: 'cubic'
        },
        perspective: 1084,
        shadow: true,
        shadowBlurRadius: 80,
        shadowSpreadRadius: -11,
        sizeAdjustment: true,
        sizeAdjustmentBezierPoints: {
                p1: {
                        x: 0,
                        y: 67
                },
                p2: {
                        x: 49,
                        y: 57
                },
                p3: {
                        x: 74,
                        y: 60
                },
                p4: {
                        x: 100,
                        y: 88
                }
        },
        popoutSelected: true,
        popoutSelectedShiftZ: 605
});

    }
 });











</script>









