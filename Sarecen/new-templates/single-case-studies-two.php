<?php
/*
 * Template Name: Case Study two
 * Template Post Type: case_study
 */

get_header();?>
<div class="get-in-touch">Get in touch</div>
<div class="saracen-services"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"><!--<img src="/wp-content/uploads/bits/logo-saracen-interiors.png">--></div>

<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">


<div class="slider-holder">

<?php

$images = get_field('hero_image');

if ($images):?>
<div id="slider" class="flexslider">

        <ul class="slides">
<?php foreach ($images as $image):?>
<li>
    <div class="image-holder" style="background-image: url('<?php echo $image['url'];?>')"></div>
                    <!--<img class="respin" src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" />-->

</li>
<?php endforeach;?>
</ul>
    </div>

<?php endif;?>






<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?>
</div>
</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->
</div><!-- Wrapper end -->
<script
  src="http://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
<script src="/wp-content/themes/marvel-child/js/jquery.flexslider.js"></script>

<script type="text/javascript">

     $(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        slideshowSpeed: 9000,
        controlNav: false,
        directionNav: false,
        animationSpeed: 6000,
        pauseOnHover: true,
        reverse: true,
animationLoop: false,
    });
  });

  </script>


