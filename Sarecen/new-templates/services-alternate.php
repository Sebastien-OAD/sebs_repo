<?php
/*
 * Template Name: Services Alternate
 *
 */

get_header();?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<div class="get-in-touch"><span class="popmake-get-in-touch-global">Get in touch</span></div>
<div class="saracen-services"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"><!--<img src="/wp-content/uploads/bits/logo-saracen-interiors.png">--></div>

<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">

            	<div class="slider-holder2 col-lg-12 col-md-12 col-sm-12 services-slider-main">

<?php

$images = get_field('services-gallery');

if ($images):?>
<div id="slider-2" class="flexslider2">

        <ul class="slides">
<?php foreach ($images as $image):?>
<li>
    <div class="services-slide-image"><img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" /></div>
</li>


<?php endforeach;?>
</ul>
    </div>

<?php endif;?>
</div>


<div class="bottom-content-services col-md-12">
<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?>
</div>


</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();?>

 <script src="/wp-content/themes/marvel-child/js/jquery.flexslider.js"></script>

<script type="text/javascript">

     $(window).load(function() {
    $('.flexslider2').flexslider({
        animation: "fade",
        slideshowSpeed: 4000,
        controlNav: false,
        directionNav: false,
        animationSpeed: 2000,
        pauseOnHover: true,
        reverse: true,
animationLoop: true,
    });
  });

  </script>