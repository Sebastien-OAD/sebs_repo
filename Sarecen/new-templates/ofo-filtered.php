<?php
/**
 * Template Name: CS-filtered Office Fit Out
 */

get_header();?>
<div class="get-in-touch"><span class="popmake-get-in-touch-global">Get in touch</span></div>
<div class="saracen-services"><a href="/services/facilities/"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"></a><a href="/services/small-works/"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"></a><a href="/services/move-management/"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"></a></div>


<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">

            	<div class="filtered-cs-feeds col-md-12">
            		<div class="filtered-header">
            		<h1><?php the_title();
?></h1></div>

<?php $categories = get_the_terms($post->ID, 'case_studies_categories');

foreach ($categories as $category) {
	$pageCat = $category->term_id;
	//echo $pageCat;
}
?>

<?php

// args
$args = array(
	'posts_per_page' => -1,
	'post_type'      => 'case_study',
	'orderby'        => 'post_date',
	'order'          => 'DESC',
	'tax_query'      => array(
		array(
			'taxonomy' => 'case_studies_categories', //or tag or custom taxonomy
			'field'    => 'id',
			'terms'    => array($pageCat)
		)
	)

);

// query
$the_query = new WP_Query($args);?>
<?php if ($the_query->have_posts()):?>

<?php while ($the_query->have_posts()):$the_query->the_post();?>
<div class="case-post-article-filtered col-md-4">
	<div class="filtered-inner">
		  <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Review",
  "datePublished":"<?php the_field('date_of_quote');?>",
  "reviewBody":"<?php echo sanitize_text_field(get_field('quote'));?>",
  "itemReviewed": {
    "@type": "Thing",
    "name": "<?php the_field('project');?>",
    "description":"<?php echo sanitize_text_field(get_field('quote'));?>"

  },
  "author": {
    "@type": "Person",
    "name": "<?php the_field('quote_by');?>"
  },
  "reviewRating": {
    "@type": "Rating",
    "ratingValue": "<?php the_field('rating');?>",
    "bestRating": "100"

  },
  "publisher": {
    "@type": "Organization",
    "name": "Saracen Interiors"
  }
}
</script>

<?php $body_background = get_field('feed_image');?>
<?php if ($body_background) {?>
										<div class="filtered-image-holder">
										<div class="cs-image-main-filtered">
										<a href="<?php the_permalink();?>"><img src="<?php echo $body_background['url'];?>" alt="<?php echo $body_background['alt'];?>" /></a>   </div></div>
	<?php }?>


<div class="cs-title-feed"><a href="<?php the_permalink();?>"><?php the_title();
?></a></div>
<div class="parameters">
                  <div class="parameter">
<?php the_field('location');?></div>
                   <div class="parameter">
<?php the_field('project');?></div>
            <div class="parameter">
<?php the_field('project_size');?></div>
            <div class="parameter">
<?php the_field('timeline');?></div>
           </div>
<div class="cs-summary"><?php the_field('summary_/_description');?></div>
<div class="btn btn-primary filtered-readmore"><a href="<?php the_permalink();?>">Read More</a></div>
</div></div>
<?php endwhile;?>


<?php endif;?>

<?php wp_reset_query();// Restore global post data stomped by the_post(). ?>
</div>
</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();?>