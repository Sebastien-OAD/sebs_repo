<?php
/*
 * Template Name: Case Study
 * Template Post Type: case_study
 */

get_header();?>
<div id="left"></div>
<div id="right"></div>
<div id="top"></div>
<div id="bottom"></div>
<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">


					<div class="col-md-12 case-study">
						<div class="row">

						<div class="case-study-main">
<div class="row">
							<div class="case-study-body col-md-4">

								<div class="case-study-body-inner">
										<h1><?php the_title();?></h1>
										<p><?php the_field('case_study_body');?></p>

							</div></div>
							<div class="swooshy"><img src="http://localhost:8888/wordy/wp-content/uploads/2017/10/swoosh-trans-3.png"></div>
							<div class="case-study-hero col-md-8 col-sm-12 col-xs-12">
<?php if (get_field('hero_image')) {?>
															<div class="hero-inner"  style="background-image: url('<?php the_field('hero_image');?>');">

	<?php }?>
</div></div>
</div>


<?php the_field('summary_description');?>
<?php the_field('location');?>
<?php the_field('project');?>
<?php the_field('project_size');?>
<?php the_field('timeline');?>
<?php the_field('quote');?>
<?php the_field('quote_by');?>

<?php the_field('video');?>
<?php the_field('timelapse');?>
<div class="my-row">



<?php
wp_reset_query();// necessary to reset query
while (have_posts()):the_post();
the_content();
endwhile;// End of the loop.
?>
</div>


</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();?>