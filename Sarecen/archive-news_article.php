<?php

/* Template Name: News Article Loop */

get_header();?>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="http://localhost:8888/wordy/wp-content/themes/marvel/js/theta-carousel.min.js"></script>


<div id="left"></div>
<div id="right"></div>
<div id="top"></div>
<div id="bottom"></div>
<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">
            	<div class="news-feed">


<?php

// args
$args = array(
	'numberposts' => -1,
	'post_type'   => 'news_article',
);

// query
$the_query = new WP_Query($args);

?>
<?php if ($the_query->have_posts()):?>

<?php while ($the_query->have_posts()):$the_query->the_post();?>
<div class="col-md-4 news-post-article item">
<?php if (get_field('news_hero_image')) {?>
																								<img src="<?php the_field('news_hero_image');?>" />
	<?php }?>
			<?php the_field('news_date');
?> <a href="<?php the_permalink();?>">
<?php the_title();?>
</a>
			</div>

<?php endwhile;?>

<?php endif;?>

<?php wp_reset_query();// Restore global post data stomped by the_post(). ?>
</div>
</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->

<script type="text/javascript">

$('.news-feed').theta_carousel( {
        selectedIndex: 3,
        distance: 69,
        numberOfElementsToDisplayRight: 12,
        numberOfElementsToDisplayLeft: 13,
        designedForWidth: 1366,
        designedForHeight: 734,
        distanceInFallbackMode: 731,
        scaleX: 1.12,
        scaleY: 0.63,
        scaleZ: 0.41,
        path: {
                settings: {
                        shiftZ: -222,
                        rotationAngleXY: 13,
                        rotationAngleZX: -2,
                        wideness: 87,
                        endless: true
                },
                type: 'cubic'
        },
        perspective: 1290,
        mode3D: 'scale',
        inertiaFriction: 7,
        inertiaHighFriction: 28,
        popoutSelected: true,
        popoutSelectedShiftX: -72,
        popoutSelectedShiftY: 5,
        popoutSelectedShiftZ: 492
});





</script>

<?php get_footer();?>



