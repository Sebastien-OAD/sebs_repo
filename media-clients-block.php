<?php
/*
 * Template Name: Clients Posts feed media
 *
 */

get_header();
get_template_part('media-switch');
get_template_part('media-logo');
?>

<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?>
<div class="clients-blocks ">
	<div class="our-clients-feed-title">we are proud to work with</div>








<?php $loop = new WP_Query(array('post_type' => 'clients', 'posts_per_page' => -1));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class="col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 client-blocks">
			<div class="client-block">

<?php the_post_thumbnail('featured');?>
			</div>
			<div class="team-details">
				<a href="<?php the_permalink()?>">
					<div class="overlay-cb">
			    <div class="text-bs">
			              <div class="one-agency-member-name"><?php echo get_the_title($ID);?></div>

								<div class="one-agency-member-role">
<?php the_excerpt();?></div>
								</div>
			    </div>
			       </div></a>
			</div>







</article>


<?php endwhile;
wp_reset_query();
?>
</div>
<div class="digital-main-cta col-md-12">
	<div class="digital-cta-large col-md-6 col-md-offset-3">

<?php echo do_shortcode('[contact-form-7 id="5548" title="Digital - CTA Large"]');?>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>

<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>


<?php
get_footer('');
?>