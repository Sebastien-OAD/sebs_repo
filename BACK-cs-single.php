<?php
/*
 * Template Name: Case Study
 * Template Post Type: case_study
 */

get_header();?>
<style>
  .embed-container {
    position: relative;
    padding-bottom: 56.25%;
    height: 0;
    overflow: hidden;
    max-width: 100%;
    height: auto;
  }

  .embed-container iframe,
  .embed-container object,
  .embed-container embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
</style>
<!--scripts for full page scroll-->
<link rel="stylesheet" type="text/css" href="/wp-content/themes/marvel-child/jquery.fullPage.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/marvel-child/js/scrolloverflow.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/marvel-child/js/jquery.fullPage.js"></script>
<script type="text/javascript" src="/wp-content/themes/marvel-child/js/masonry.pkgd.min.js"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>





<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>-->
  <link rel="stylesheet" href="/wp-content/themes/marvel-child/flexslider.css">
  <div class="get-in-touch">Get in touch</div>
<div class="saracen-services"><img src="/wp-content/uploads/bits/black-saracen-facilities.png"><img src="/wp-content/uploads/bits/black-saracen-small-works.png"><img src="/wp-content/uploads/bits/black-saracen-move-mgmt.png"><!--<img src="/wp-content/uploads/bits/logo-saracen-interiors.png">--></div>

<div class="wrapper" id="page-wrapper">

    <div  id="content" class="container">

       <div id="primary" class="col-md-12 content-area">

            <main id="main" class="site-main" role="main">
              <div id="fullpage">

<div class="section fp-auto-height-responsive">
<div class="slider-holder">

<?php

$images = get_field('hero_image');

if ($images):?>
<div id="slider" class="flexslider">

        <ul class="slides">
<?php foreach ($images as $image):?>
<li>
    <div class="image-holder" style="background-image: url('<?php echo $image['url'];?>')"></div>
                    <!--<img class="respin" src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" />-->

</li>
<?php endforeach;?>
</ul>
    </div></div>

<?php endif;?>


          <div class="col-container swoosh-background">
            <div class="col col-md-6">
              <img src="http://localhost:8888/saracen-copy/wp-content/uploads/2017/11/swish-WIP.png" />

            </div>

            <div class="col col-md-6">

            </div>


          </div>


          <div class="col-container case-study-intro">
            <div class="col  col-intro">
              <h1><?php the_title();?></h1>
              <div class="parameters">
                  <div class="parameter">
<?php the_field('location');?></div>
                   <div class="parameter">
<?php the_field('project');?></div>
          		<div class="parameter">
<?php the_field('project_size');?></div>
          		<div class="parameter">
<?php the_field('timeline');?></div>
          	</div>
              <p><?php the_field('case_study_body');?></p>

            </div>

            <div class="col col-quicklinks quick-links">
                  <li><a href="#section4">Gallery</a></li>
          	    <li><a href="#section3">Videos</a></li>
          	    <li><a href="#section5">Timelapse</a></li>


            </div>


          </div>

</div>









<!--<div class="section">

<?php the_field('summary_description');?></div>-->

<div class="section fp-auto-height-responsive" >
<div class="quote-area">
<div class="quote"><h1><?php the_field('quote');?></h1></div>
<div class="quote-by"><h3><?php the_field('quote_by');?></h3></div></div></div>

<div class="section fp-auto-height-responsive" >
<div class="main" id="videos">
<div class="video-container">
<div class="embed-container">
<?php the_field('video');?>
</div>
</div>

</div>
</div>




<div class="section fp-auto-height-responsive" >
<div class="gallery-container">
<div class="grid">


<?php

$images = get_field('hero_image');

if ($images):?>
 <?php $toggle_class = 'even';?>
<?php foreach ($images as $image):?>
  <?php $toggle_class = ($toggle_class == 'odd'?'even':'odd');?>
          <div class="grid-item <?php echo $toggle_class;?> ">
                <a href="<?php echo $image['url'];?>">
                     <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>" />
                </a>
                </div>

<?php endforeach;?>

<?php endif;?>
</div>
</div>
</div>


<div class="section fp-auto-height-responsive" >
<div class="main" id="timelapse">
<div class="video-container">
<div class="embed-container">
<?php the_field('timelapse');?>
</div></div>
</div></div>

<div class="section fp-auto-height-responsive" >
<div class="my-row">



<?php
wp_reset_query();// necessary to reset query
while (have_posts()):the_post();
the_content();
endwhile;// End of the loop.
?>
</div>
<div class="case-study-footer">
<?php get_footer();?>
</div></div>



</div>


</main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->





  <script type="text/javascript">



  	$(document).ready(function(){




 $(document).ready(function() {
  $('#fullpage').fullpage({
    responsiveWidth:1400,
    anchors: ['section1', 'section2', 'section3', 'section4' ,'section5', 'section6'],
    sectionsColor: ['#ffffff', '#fbce20', '#ffffff', '#ffffff','#ffffff', '#fbce20']

  });
});


// init Masonry
var $grid = $('.grid').imagesLoaded( function() {
  // init Masonry after all images have loaded
  $grid.masonry({

  itemSelector: '.grid-item',
  columnWidth: 50,
  gutter: 10

  });
});
    });
  </script>
  <script src="/wp-content/themes/marvel-child/js/jquery.flexslider.js"></script>

<script type="text/javascript">

     $(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        slideshowSpeed: 9000,
        controlNav: false,
        directionNav: false,
        animationSpeed: 6000,
        pauseOnHover: true,
        reverse: true,
animationLoop: false,
    });
  });

  </script>

