<?php
/**
 * Template Name: Team-new-digital
 */
get_header('digital');
get_template_part('digital-switch');
get_template_part('digital-logo');
?>
<div class="one-team col-md-12">






<!--<?php $loop = new WP_Query(array('post_type' => 'digitalteam', 'posts_per_page' => -1, 'meta_key' => 'orderByNum', 'orderby' => 'meta_value_num', 'order' => 'ASC', 'meta_query' => array(
			'relation'                                    => 'AND',
			array(
				'key'     => 'team',
				'value'   => 'digital',
				'compare' => '=',

			))));?>-->


<?php $loop = new WP_Query(array('post_type' => 'digitalteam', 'posts_per_page' => -1, 'meta_key' => 'orderByNum', 'orderby' => 'meta_value_num', 'order' => 'ASC'));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class="col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 one-team-member">
<div class="one-team-member-image">

<?php
if (has_post_thumbnail()) {
	$large_image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
	echo the_post_thumbnail('full');
}
?></div>
<div class="team-details">


<a href="<?php the_permalink()?>"><div class="overlay-bs">
    <div class="text-bs-t">
<div class="one-agency-member-name"><?php echo get_the_title($ID);?></div>

<div class="one-agency-member-role">
<?php the_field('role_title');?></div>
</div>
  </div></a>

</div>


</article>


<?php endwhile;
wp_reset_query();
?>
</div>


<?php if (have_posts()):while (have_posts()):the_post();?>
<div class="container">
<div class="digital-team col-md-12">
<?php the_content();?>
</div></div>
<?php endwhile;
endif;
?>
<div class="quick-chat col-md-12">
<?php echo do_shortcode('[contact-form-7 id="5427" title="Digital CTA - Small"]');?></div>








<?php get_footer('digital');?>