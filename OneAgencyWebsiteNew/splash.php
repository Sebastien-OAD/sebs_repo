<?php
/**
 * Template Name: Splash
 *
 * @package WordPress
 * @subpackage North
 * @since North 1.0
 */
get_header();
?>
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<div class="white-logo-left"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/07/logo_white.png"></div>



<div class="home-page">

			<div class="col-md-6 media-visit">
					<div class="vertical-center">
					<div class="splah-intro col-md-10 col-md-offset-1 col-xs-12 ">
						<div class="splash-title col-md-12"><h1>Visit Media</h1></div>
			<div class="col-md-12"><a href="http://www.oneagencymedia.co.uk/"><button type="button" class="btn btn-outline-secondary btn-lg">Click Here</button></a></div>
					</div></div></div>

<div class="col-md-6 digital-visit ">
				<div class="vertical-center">
		            <div class="splah-intro col-md-10 col-md-offset-1 col-xs-12">
					<div class="splash-title col-md-12"><h1>Visit Digital</h1></div>
					<div class="col-md-12"><a href="/digital/"><button type="button" class="btn btn-outline-secondary btn-lg">Click Here</button></a></div>
		    </div></div></div>


<div class="col-md-6 digital-image "><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/digital-block-2.jpg"></div>
<div class="col-md-6 media-image "><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/10/new-header-1.jpg"></div>
<div class="splash-floating-box">A full service advertising agency based in Manchester </div>

<div class="splash-intro col-md-12">A full service media agency specialising in Outdoor Advertising and a Digital Media department for all your online needs</div>



</div>

<script>
$(function() {
    $('.media-visit').hover(function() {
        $('.media-image').fadeIn('slow');
      },
      function(){
        $('.media-image').fadeOut('slow');
      }
   );
});

$(function() {
    $('.digital-visit').hover(function() {
        $('.digital-image').fadeIn('slow');
      },
      function(){
        $('.digital-image').fadeOut('slow');
      }
   );
});
</script>









