<?php
/**
 * Template Name: digital case study feed
 */
get_header('digital');
get_template_part('digital-switch');
get_template_part('digital-logo');
?>
<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?>
<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>

<div class="digital-case-studies-feed col-md-12">
<div class="ui-group">
    <div class="ui-group__title d-cs-f">Filter case studies by service</div>
    <div class="filter-button-group js-radio-button-group buttons-left">
      <button class="button od-cd-but is-checked" data-filter="*">show all</button>

<?php
$terms = get_terms("digital_services");
$count = count($terms);
if ($count > 0) {
	foreach ($terms as $term) {

		?> <button class="button od-cd-but" data-filter=".<?php echo $term->slug?>">  <?php echo $term->name?> </button><?php
	}
}
?>
</div>
  </div>
<!-- swapping classes for grid -->
<?php $class_name = array('big', 'small', 'wide', 'long');
$arrKey           = 0;?>





<?php $loop = new WP_Query(array('post_type' => 'digital_case_study', 'posts_per_page' => -1));?>
<?php if ($loop->have_posts()):?>
<div class="grid">
  <div class="grid-sizer"></div>

<?php while ($loop->have_posts()):$loop->the_post();
$termsArray  = get_the_terms($post->ID, "digital_services");
$termsString = "";//initialize the string that will contain the terms
foreach ($termsArray as $term) {// for each term
	$termsString .= $term->slug.' ';//create a string that has all the slugs
};

?>

             <div class="<?php echo $termsString;?> grid-item grid-item-<?php echo $class_name[$arrKey]?>">

<a href="<?php the_permalink();?>"><div class="container-bbbb">
<?php $arrKey++;?>
<?php if ($arrKey >= '4') {

	$arrKey = 0;
}?>
<div class="container-digital-cs">
<?php the_post_thumbnail('featured');?>
  <div class="overlay-digital-cs">
    <div class="text-digital-cs"><?php the_excerpt()?></div>
  </div>
</div>

</div></div></a>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Article",
  "headline": "<?php the_title();?>",
  "image": {
    "@type": "ImageObject",
    "url": "<?php echo get_the_post_thumbnail_url($post_id, 'thumbnail');?>",
    "width": 696,
    "height": 400

  },
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "<?php the_permalink();?>"
  },
  "description": "<?php the_excerpt();?>",
  "datePublished": "<?php the_time('Y/m/d ')?>",
  "dateModified": "<?php the_modified_date('Y/m/d');?>",
  "author": {
    "@type": "Organization",
    "name": "One Agency"
  },
  "publisher": {
    "@type": "Organization",
    "name": "OneAgency",
    "logo": {
      "@type": "ImageObject",
      "url": "http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/logo.png",
      "width": 100,
      "height": 60
    }
  }
}
</script>
<?php endwhile;?>
</div>


<?php endif;?>


<?php
wp_reset_query();
?>
</div>



<!--<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>-->

<link rel="stylesheet" href="/wp-content/themes/north/styles/owl.carousel.min.css" />
<script src="/wp-content/themes/north/include/isotope.pkgd.min.js"></script>
<script src="/wp-content/themes/north/include/packery-mode.pkgd.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
$('.grid').isotope({
  // set itemSelector so .grid-sizer is not used in layout
  itemSelector: '.grid-item',
  percentPosition: true,
  layoutMode: 'packery',
  masonry: {
    // use element for option
    columnWidth: '.grid-sizer',

  }
})
// init Isotope
var $grid = $('.grid').isotope({
  // options
});
// filter items on button click
$('.filter-button-group').on( 'click', 'button', function() {
  var filterValue = $(this).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});

});
</script>

<script type="text/javascript">

$(document).ready(function(){


  $(".breadcrumbs").detach().appendTo('.breadcrumb-holder')

});



</script>



<?php get_footer('digital');?>