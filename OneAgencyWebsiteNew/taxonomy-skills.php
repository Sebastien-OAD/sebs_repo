<?php
/**
* The Template for taxonomy
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
$term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
get_header();
?>
<div id="thmlvContent">
	<div id="thmlvIsotope">
		<?php	
			echo north_switch_header();
			$args = array(
				'nopaging' => true,
				'post_type' => 'portfolio',
				'skills' => $term->slug,
				'orderby' => array('menu_order' => 'ASC', 'ID' => 'ASC')
			);
			$wp_query = new WP_Query($args);
			while ($wp_query->have_posts()) : $wp_query->the_post(); 
				get_template_part('loop-portfolio', get_post_format());
			endwhile;
			wp_reset_postdata();
		?>
	</div>
</div>
<?php get_footer(); ?>