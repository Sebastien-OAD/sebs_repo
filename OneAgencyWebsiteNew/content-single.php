<?php
/**
 * The Template for standard post format.
 *
 * @package WordPress
 * @subpackage North
 * @since North 1.0
 */
?>
<div id="thmlv-main" class="container row">
<?php the_content();?>
<div class="thmlvClear"></div>
<?php wp_link_pages();?>