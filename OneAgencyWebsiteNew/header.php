<?php
/**
 * The Template for displaying site head.
 *
 * @package WordPress
 * @subpackage North
 * @since North 1.0
 */
?>
<!DOCTYPE HTML>
<html <?php language_attributes();?>>
<head>
	<!--[if ie]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
	<meta charset="<?php bloginfo('charset');?>" />
	<meta name="author" content="Themelovin" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="profile" href="http://gmpg.org/xfn/11" />

<?php
$thmlvFavicon = get_option('north_favicon_image');
if ($thmlvFavicon != '') {?>
				<link rel="shortcut icon" href="<?php echo $thmlvFavicon;?>" />
	<?php
}
?>
	<?php
$thmlvTouchIcon = get_option('north_touchicon_image');
if ($thmlvTouchIcon != '') {
	?>
				<link rel="apple-touch-icon-precomposed" href="<?php echo get_option('north_touchicon_image');?>" />
	<?php
}
?>

	<!-- RSS & Pingbacks -->
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name');?> RSS Feed" href="<?php bloginfo('rss2_url');?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url');?>" />
<?php wp_head();?>
<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "WebSite",
  "name": "One Agency Media",
  "alternateName": "One Agency",
  "url": "http://www.oneagencymedia.co.uk/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "outdoor-advertising{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "ProfessionalService",
  "name": "One Agency Media",
  "image": "http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/logo.png",
  "@id": "#organization",
  "url": "http://www.oneagencymedia.co.uk/",
  "telephone": "03304004169",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "Unit 14, SchoolHouse, Third Avenue, Trafford Park",
    "addressLocality": "Manchester",
    "postalCode": "M171JE",
    "addressCountry": "GB"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 53.464801,
    "longitude": -2.309677
  },
  "openingHoursSpecification": {
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday"
    ],
    "opens": "08:30",
    "closes": "17:00"
  },
  "sameAs": [
    "https://en-gb.facebook.com/oneagencymedia/",
    "https://twitter.com/oneagencymedia?lang=en",
    "https://uk.linkedin.com/company/one-agency-ltd"
  ]
}
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s);
 js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-54272509-22"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-54272509-22');
</script>

</head>
<body  <?php body_class();?>>

<div id="thmlvScrollMenuWrap">
	<div class="row">
		<div class="col span_2">
<?php
$output = '<div id="thmlvLogoVV"><a class="fancybox" href="#contact_form_pop"><img src="'.esc_url(get_option('north_logo_dark')).'" alt="'.get_bloginfo('name').'" id="thmlvLogoDark" class="thmlvLogoSwitch" /></a></div>';
echo $output;
?>
</div>

		<div class="col span_4">
<?php
$outputs = '<div id="thmlvLogoCC"><a href="'.home_url().'"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/logo.png" alt="'.get_bloginfo('name').'" id="thmlvLogoDarkC" class="thmlvLogoSwitchC" /></a></div>';
echo $outputs;
?>
</div>

		<div class="col span_6">
<?php wp_nav_menu(array('theme_location' => 'top-menu', 'menu_class' => 'thmlvTopMenu', 'sort_column' => 'menu_order', 'container' => 'nav', 'fallback_cb' => false, 'depth' => 1));?>
			<a href="#" title="<?php _e('Open main menu', 'themelovin')?>" id="thmlvStickyOpenMenu" class="thmlvOpenMenu"><i class="fa fa-bars"></i></a>
		</div>
	</div>
</div>