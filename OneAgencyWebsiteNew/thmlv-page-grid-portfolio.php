<?php
/**
* Template Name: Portfolio Grid
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
get_header();
?>
<div id="thmlvContent">
	<?php
	echo north_switch_header($post->ID);
    
	include_once(ABSPATH.'wp-admin/includes/plugin.php'); 
	if(is_plugin_active('themelovin-portfolio/thmlv-portfolios.php')) {
	$args = array(
		'nopaging' => true,
		'post_type' => 'post',
		'orderby' => array('menu_order' => 'ASC', 'ID' => 'ASC')
	);
	$wp_query = new WP_Query($args);
	while ($wp_query->have_posts()) : $wp_query->the_post(); 
		get_template_part('loop-portfolio-grid', get_post_format());
	endwhile;
	wp_reset_postdata();
	}
	?>
</div>
<?php
get_footer();
?>