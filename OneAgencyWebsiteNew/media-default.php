
<?php
/**
 * Template Name: default-media
 *
 * @package WordPress
 * @subpackage North
 * @since North 1.0
 */
get_header('');

?>
<div id="digital-content-ss">

<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;?>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>

<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>

<?php get_footer('');?>