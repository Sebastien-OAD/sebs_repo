<?php
/*
 * Template Name: digital case studies
 * Template Post Type: digital_case_study
 */

get_header('digital');
get_template_part('digital-switch');
get_template_part('digital-logo');
?>
<!--<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>-->


<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?>
<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>
<?php if (get_field('case_study_client_logo')) {?>
													<div class="digital-cs-quote col-md-12">
													<div class="col-md-4 digital-cs-quote-image">
																		<img src="<?php the_field('case_study_client_logo');?>" />
														  </div>
													<div class="col-md-8 digital-cs-quote-text"> <div class="quote-body"><?php the_field('quote_body');
	?></div>
													<div class="quote-by">
	<?php the_field('quote_by');
	?></div></div><?php }?>
</div>

<?php $related_case_studies = get_field('related_case_studies');?>
<?php if ($related_case_studies):?>
<div class="related-casestudies col-md-12">
	<div class="related-feed-title">Related Case Studies</div>
<?php foreach ($related_case_studies as $post):?>
		<?php setup_postdata($post);?>



		<div class="digital-case-study-feed-item col-md-4">
			<a href="<?php the_permalink();?>">


<div class="related-cs-image col-md-12"><?php the_post_thumbnail('featured');?></div>
<div class="related-cs-title col-md-12"><?php the_title();?></div>
<div class="related-cs-summary col-md-12"><?php the_excerpt()?></div></a>
</div>
<?php endforeach;?>
<?php wp_reset_postdata();?>
<?php endif;?>
</div>

<!--seo -->
<div class="seo-sections col-md-12">

<div class="proud-clients">

<div class="clients-blocks ">
	<div class="our-clients-feed-title">we are proud to work with</div>








<?php $loop = new WP_Query(array('post_type' => 'clients', 'posts_per_page' => -1));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2 client-blocks">
			<div class="client-block">

<?php the_post_thumbnail('featured');?>
			</div>
			<div class="team-details">
				<a href="<?php the_permalink()?>">
					<div class="overlay-cb">
			    <div class="text-bs">
			              <div class="one-agency-member-name"><?php echo get_the_title($ID);?></div>

								<div class="one-agency-member-role">
<?php the_excerpt();?></div>
								</div>
			    </div>
			       </div></a>








</article>


<?php endwhile;
wp_reset_query();
?>
</div>
</div>
<!-- carousel-->


<link rel="stylesheet" href="/wp-content/themes/north/styles/owl.carousel.min.css" />
<script src="/wp-content/themes/north/include/owl.carousel.min.js"></script>

<div class="digital-testi">

<div class="testimonial-blocks ">
	<div class="owl-carousel owl-theme">
<!-- see single clients for code to remove currenlty viewed post from feed   -->
<?php $loop = new WP_Query(array('post_type' => 'testimonials', 'posts_per_page' => -1));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class=" testimonial-feed">

				<!--<a href="<?php the_permalink()?>"><div class="testimonial-title"><?php echo get_the_title($ID);?></div></a>-->


                   <div class="testimonial-title"> '<?php echo get_the_title($post_id);?>'</div>

                           <div class="testimonial-body"><?php the_field('body_quote');?></div>

    <div class="testimonial-person"><?php the_field('person_name');?></div>

       <div class="testimonial-position"><?php the_field('position');?></div>


       <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Review",
  "datePublished":"<?php the_field('date');?>",
  "reviewBody":"<?php the_field('body_quote');?>",
  "itemReviewed": {
    "@type": "Thing",
    "name": "<?php the_field('service_product_review');?>",
    "description":"<?php the_field('body_quote');?>"

  },
  "author": {
    "@type": "Person",
    "name": "<?php the_field('person_name');?>"
  },
  "reviewRating": {
    "@type": "Rating",
    "ratingValue": "<?php the_field('rating');?>",
    "bestRating": "100"

  },
  "publisher": {
    "@type": "Organization",
    "name": "One Agency"
  }
}
</script>





<div class="rating-area">

<div class="containerdiv">
    <div>
    <img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_blank-one.png" alt="img">
    </div>
    <div class="cornerimage" style="width:<?php the_field('rating');?>%;">
    <img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_full-one.png" alt="">
    </div>
<div>

</div>




</article>


<?php endwhile;
wp_reset_query();
?>
</div>
</div></div>
<script type="text/javascript">
	$(document).ready(function(){
  var owl = $('.owl-carousel');
owl.owlCarousel({
    items:3,
    loop:true,
     nav:true,
    margin:10,
     responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
      },
    autoplay:true,
    autoplayTimeout:9000,
    autoplayHoverPause:true
});
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[1000])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})
});


</script>


</div>
<!--end of seo-->
<div class="digital-main-cta col-md-12">
  <div class="digital-cta-large col-md-6 col-md-offset-3">

<?php echo do_shortcode('[contact-form-7 id="5548" title="Digital - CTA Large"]');?>
</div>
</div>


<?php get_footer('digital');?>


<script type="text/javascript">

$(document).ready(function(){


	$(".breadcrumbs").detach().appendTo('.breadcrumb-holder')

});



</script>