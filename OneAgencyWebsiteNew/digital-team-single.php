<?php
/*
 * Template Name: digital team single
 * Template Post Type: digitalteam
 */
get_header('digital');
get_template_part('digital-switch');
get_template_part('digital-logo');
?>


<?php if (get_field('header_iamge')) {?>

																				<div class="header-holder-digi">

																					<header id="digital-team-header" style="background-image: url('<?php the_field('header_iamge');?>')">

																																		<!--	<div class="team-image col-md-12" style="background-image: url('<?php the_field('header_iamge');?>')">
																																						<!--<img src="<?php the_field('header_iamge');?>" />--></div>
	<?php }?>



</header></div>
<div class="team-details-info"><div class="team-name"><?php echo get_the_title();?></div>

	<div class="team-role"><?php the_field('role_title');?></div>
</div>
<div class="breadcrumb-holder"></div>
<div class="container">
<div class="one-team col-md-12">




<?php if (have_posts()):while (have_posts()):the_post();?>
<div class="digital-team col-md-12">
<?php the_content();?>
</div>
<?php endwhile;
endif;
?>
</div>

<div class="container">

<?php

// check if the repeater field has rows of data
if (have_rows('skills')):?>
<div class="my-skills">
<div class="col-md-6 all-skills-title">
	Skills </div>


	<div class="col-md-6">

<?php while (have_rows('skills')):the_row();?>


<div class="team-skill-title"><?php the_sub_field('skill_name');?></div>


<!--<div class="team-skill-value"> <?php the_sub_field('skill_value');?></div>-->

<div class="thmlv-short-progress-bar">
<strong style="opacity: 1;"><i><?php the_sub_field('skill_value');
?></i>%</strong><div class="bar-wrap"><span style="background-color: #03464e; width: 45%;" data-width="<?php the_sub_field('skill_value');?>"></span></div></div>




<?php

endwhile;

 else :

// no rows found

endif;

?>
</div></div></div></div>
<!--related news area-->
<div class="container">
<?php $related_content = get_field('related_content');
$related_content       = array_slice($related_content, 0, 2)
?>
<?php if ($related_content):?>
<div class="related-content-posts">

<div class="relates-news-title col-md-12">News Stories</div>

<?php foreach ($related_content as $post):?>
<div class="related-content-posts-data col-md-6">

<?php setup_postdata($post);?>
<div class="news-feature-image">
<?php the_post_thumbnail('featured');?>
	</div>
	<div class="related-content-title">
			<a href="<?php the_permalink();?>"><?php the_title();
?></a></div>
<div class="digital-news-excerpt">

<?php the_excerpt();?>
</div>
</div>
<?php endforeach;?>
<?php wp_reset_postdata();?>
<?php endif;?>
</div>





</div>
<div class="container">
<div class="people-control">
<div class="people-prev"><?php previous_post_link();
?> |  <?php next_post_link();
?></div>
</div>
</div>
<div class="quick-chat col-md-12">
<?php echo do_shortcode('[contact-form-7 id="5427" title="Digital CTA - Small"]');?></div>
<script type="text/javascript">
jQuery(document).ready(function(){
  jQuery(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>

<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>

<?php get_footer('digital');?>
