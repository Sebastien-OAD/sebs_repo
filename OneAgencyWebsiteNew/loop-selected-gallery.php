<?php
/**
* The Template for loop gallery portfolio.
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
if(get_post_meta($post->ID, "_north_customizeColors", true) != '') {
	$captionBg = north_hex2rgb(esc_url(get_post_meta($post->ID, "_north_portfolioCaptionBg", true)));
} else {
	$captionBg = '0, 0, 0';
}
?>
<div id="post-<?php the_ID(); ?>"  <?php post_class('thmlvFullSelected'); ?> data-captionbg="<?php echo $captionBg ?>">
	<div class="thmlvGalleryWrap">
		<div class="thmlvGalleryCell">
			<div>
			</div>
		</div>
	</div>
	<?php echo north_loop_port_gallery($post->ID, 'full'); ?>
</div>