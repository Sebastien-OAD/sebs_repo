
<?php
/**
 * Template Name: digital about
 *
 */

get_header('digital');
get_template_part('digital-switch');
get_template_part('digital-logo');

?>
<div id="digital-content-ss">

<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;?>
</div>

<div class="about-case col-md-12">
<div class="recent-case-studies-digital col-md-12">Recent Case Studies</div>
<!--case studies-->
<?php $loop = new WP_Query(array('post_type' => 'digital_case_study', 'posts_per_page' => 3));?>
<?php while ($loop->have_posts()):$loop->the_post();?>

	<div class="digital-case-study-feed-item col-md-4">
			<a href="<?php the_permalink();?>">


<div class="related-cs-image col-md-12"><?php the_post_thumbnail('featured');?></div>
<div class="related-cs-title col-md-12"><?php the_title();?></div>
<div class="related-cs-summary col-md-12"><?php the_excerpt()?></div></a>
</div>





<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Article",
  "headline": "<?php the_title();?>",
  "image": {
    "@type": "ImageObject",
    "url": "<?php echo get_the_post_thumbnail_url($post_id, 'thumbnail');?>",
    "width": 696,
    "height": 400

  },
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "<?php the_permalink();?>"
  },
  "description": "<?php the_excerpt();?>",
  "datePublished": "<?php the_time('Y/m/d ')?>",
  "dateModified": "<?php the_modified_date('Y/m/d');?>",
  "author": {
    "@type": "Organization",
    "name": "One Agency"
  },
  "publisher": {
    "@type": "Organization",
    "name": "OneAgency",
    "logo": {
      "@type": "ImageObject",
      "url": "http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/logo.png",
      "width": 100,
      "height": 60
    }
  }
}
</script>
<?php endwhile;
wp_reset_query();
?>
</div></div>



<!--end of case studies -->


<!--testimonials -->
<link rel="stylesheet" href="/wp-content/themes/north/styles/owl.carousel.min.css" />
<script src="/wp-content/themes/north/include/owl.carousel.min.js"></script>

<div class="digital-testi">

<div class="testimonial-blocks ">
	<div class="owl-carousel owl-theme">
<!-- see single clients for code to remove currenlty viewed post from feed   -->
<?php $loop = new WP_Query(array('post_type' => 'testimonials', 'posts_per_page' => -1));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class=" testimonial-feed">

				<!--<a href="<?php the_permalink()?>"><div class="testimonial-title"><?php echo get_the_title($ID);?></div></a>-->


                   <div class="testimonial-title"> '<?php echo get_the_title($post_id);?>'</div>

                           <div class="testimonial-body"><?php the_field('body_quote');?></div>

    <div class="testimonial-person"><?php the_field('person_name');?></div>

       <div class="testimonial-position"><?php the_field('position');?></div>


       <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Review",
  "datePublished":"<?php the_field('date');?>",
  "reviewBody":"<?php the_field('body_quote');?>",
  "itemReviewed": {
    "@type": "Thing",
    "name": "<?php the_field('service_product_review');?>",
    "description":"<?php the_field('body_quote');?>"

  },
  "author": {
    "@type": "Person",
    "name": "<?php the_field('person_name');?>"
  },
  "reviewRating": {
    "@type": "Rating",
    "ratingValue": "<?php the_field('rating');?>",
    "bestRating": "100"

  },
  "publisher": {
    "@type": "Organization",
    "name": "One Agency"
  }
}
</script>





<div class="rating-area">

<div class="containerdiv">
    <div>
    <img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_blank-one.png" alt="img">
    </div>
    <div class="cornerimage" style="width:<?php the_field('rating');?>%;">
    <img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_full-one.png" alt="">
    </div>
<div>

</div>




</article>


<?php endwhile;
wp_reset_query();
?>
</div>
</div></div>
<script type="text/javascript">
	$(document).ready(function(){
  var owl = $('.owl-carousel');
owl.owlCarousel({
    items:3,
    loop:true,
     nav:true,
    margin:10,
     responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
      },
    autoplay:true,
    autoplayTimeout:9000,
    autoplayHoverPause:true
});
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[1000])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})
});


</script>





<!--end of testimonials-->
<script type="text/javascript">
$(document).ready(function(){
  $(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>

<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>
<div class="digital-main-cta col-md-12">
	<div class="digital-cta-large col-md-6 col-md-offset-3">

<?php echo do_shortcode('[contact-form-7 id="5548" title="Digital - CTA Large"]');?>
</div>
</div>

<?php get_footer('digital');?>