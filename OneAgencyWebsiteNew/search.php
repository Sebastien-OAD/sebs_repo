<?php
/**
 * The Template for displaying search results.
 *
 * @package WordPress
 * @subpackage North
 * @since North 1.0
 */
get_header();
?>
<meta name="robots" content="noindex" />
<div id="thmlvContent">
<?php
echo north_switch_header();
if (have_posts()) {
	while (have_posts()):the_post();
	get_template_part('loop', 'search');
	endwhile;
} else {
	?>
		<div class="container row">
			<h2><?php _e('Sorry, but nothing matched your search criteria. Please try again.', 'themelovin');?></h2>
		</div>
	<?php
}
?>
</div>
<?php
get_footer();
?>