<?php
/**
* The Template for loop search.
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
?>
<article id="post-<?php the_ID(); ?>"  <?php post_class('typeSearch'); ?>>
	<div class="container row">
		<h1><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(array('before' => 'Permalink to: ', 'after' => '')); ?>"><?php the_title(); ?></a></h1>
		<?php the_excerpt(); ?>
	</div>
</article>