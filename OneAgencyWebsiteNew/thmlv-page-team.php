<?php
/**
 * Template Name: Team
 *
 * @package WordPress
 * @subpackage North
 * @since North 1.0
 */
get_header();
?>
<div id="thmlvContent">
<?php
echo north_switch_header($post->ID);
echo "<br><br>";
include_once (ABSPATH.'wp-admin/includes/plugin.php');
if (is_plugin_active('themelovin-team/thmlv-team.php')) {
	$args = array(
		'nopaging'  => true,
		'post_type' => 'team',
		'orderby'   => array('menu_order'   => 'ASC', 'ID'   => 'ASC')
	);
	$wp_query = new WP_Query($args);
	while ($wp_query->have_posts()):$wp_query->the_post();
	get_template_part('loop-team', get_post_format());
	endwhile;
	wp_reset_postdata();
}
?>
</div>

<?php
get_footer();
?>

