<?php
/**
 * The Template for single team.
 *
 * @package WordPress
 * @subpackage North
 * @since North 1.0
 */
get_header();
?>
<div id="thmlvContent">
<?php
echo north_switch_header($post->ID, 'tasks');
while (have_posts()) {
	the_post();
	get_template_part('content-team', get_post_format());
}
?>
</div>
<!--<div class="container">

<?php

// check if the repeater field has rows of data
if (have_rows('skills')):?>
<div class="my-skills">
<div class="col-md-6 all-skills-title">
	Skills </div>


	<div class="col-md-6">

<?php while (have_rows('skills')):the_row();?>


<div class="team-skill-title"><?php the_sub_field('skill_name');?></div>


<!--<div class="team-skill-value"> <?php the_sub_field('skill_value');?></div>-->

<!--<div class="thmlv-short-progress-bar">
<strong style="opacity: 1;"><i><?php the_sub_field('skill_value');
?></i>%</strong><div class="bar-wrap"><span style="background-color: #DD1A1E; width: 45%;" data-width="<?php the_sub_field('skill_value');?>"></span></div></div>




<?php

endwhile;

 else :

// no rows found

endif;

?>
</div></div></div></div>-->



<?php
get_footer();
?>

<?php
$imgsrc = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');

?>


<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Person",
  "name": "<?php the_title();?>",
  "alternateName": "<?php the_title();?>",
  "url": "<?php echo the_permalink()?>",
  "image": "<?php echo $imgsrc[0];?>",
  "jobTitle": "<?php the_field('role_title');?>",
  "worksFor": {
    "@type": "Organization",
    "name": "One Agency Digital"
  }
}
</script>