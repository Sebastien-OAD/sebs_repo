<?php
/**
 * Template Name: digital services single
 */

get_header('digital');
get_template_part('digital-switch');
get_template_part('digital-logo');
?>
<!--<div id="thmlvContent">
<?php

if (have_posts()) {
	while (have_posts()) {
		the_post();
		$format = get_post_format();
		get_template_part('loop-single', get_post_format());
	}
}

?>
</div>-->

<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?>
<!--<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>-->
<script type="text/javascript">
$(document).ready(function(){
  $(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>

<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>
<!--questions area-->

<div class="col-md-12 digital-services-questions">
<?php if (have_rows('questions')):?>

<?php while (have_rows('questions')):the_row();?>

<div class="col-md-10 col-md-offset-1 digital-service-question-block">

<div class="digital question-question col-md-12"><?php the_sub_field('question_title');?></div>

<div class="digital-question-answer col-md-12"><?php the_sub_field('answer');?></div>

<!--<div class="question-date col-md-12"><?php the_sub_field('question_date');?></div>-->


</div>
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "<?php the_sub_field('question_title');?>",

    "dateCreated": "<?php the_sub_field('question_date');?>",
    "author": {
        "@type": "Person",
        "name": "One Agency"
    },
    "answerCount": "1",

    "suggestedAnswer": {
        "@type": "Answer",

        "text": "<?php the_sub_field('answer');?>",
        "dateCreated": "<?php the_sub_field('question_date');?>",
        "author": {
            "@type": "Person",
            "name": "One Agency"
        }
    }
}
</script>

<?php

endwhile;

 else :

// no rows found

endif;

?>
</div>
<script>
$(document).ready(function(){
  var colors = ["#03cea4","#e40066","#345995","#eac435","#f5464b", "#03464e"];
  var rand = Math.floor(Math.random()*colors.length);
  $('.digital-question-answer').css("border-color", colors[rand]);

});
</script>



<?php get_footer('digital');?>




<div class="schema-ss">
<script type='application/ld+json'>
{
	"@context": "http://schema.org/",
	"@type": "Service",
    "provider": {
    "@type": "LocalBusiness",
    "name": "One agency Digital",
    "telephone":"03304004169",
    "address":{"@type":"PostalAddress","streetAddress":"Unit 14, SchoolHouse, Third Avenue, Trafford Park","addressLocality":"Manchester","postalCode":"M171JE","addressCountry":"GB"},
    "image":"http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/logo.png"
  },
	"areaServed": "United Kingdom",
	"serviceType": "service type",
	"alternateName": "<?php the_field('title');?>",
	"description": "<?php the_field('description');?>",
	"image": {
		"@type": "ImageObject",
		"contentUrl": "i<?php the_field('icon');?>",
		"embedUrl": "<?php the_field('icon');?>"
	},
	"mainEntityOfPage": "",
	"name": ""
}
</script>

</div>