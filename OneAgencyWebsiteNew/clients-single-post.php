<?php
/*
 * Template Name: Clients Posts
 * Template Post Type: clients
 */

get_header('digital');
get_template_part('digital-switch');
get_template_part('digital-logo');
?>
<div class="digital-clients-header">
<img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/clients-header.png" class="" alt="">
<div class="breadcrumb-holder"></div>
</div>
<div class="container">
	<div class="our-clients">
<div class="client-title col-md-6">
<?php echo get_the_title($post_id);?>
</div>
<div class="client-logo-img col-md-6">
<?php the_post_thumbnail('featured');?>
</div>
<div class="client-about col-md-12">

</div>
</div>
</div>

<div id="clients-post-feed">
<?php if (have_posts()):while (have_posts()):the_post();?>
<div class="container">
<div class="client-body col-md-12">
<?php the_content();?>
</div>
<?php endwhile;

endif;?>
</div></div>


</div>
</div>
<div class="clients-blocks ">
	<div class="our-clients-feed-title">We are also proud to work with</div>







<?php $currentID = get_the_ID();//removes current viewed post from the feed, using post no in var array
$loop            = new WP_Query(array('post_type' => 'clients', 'posts_per_page' => -1, 'post__not_in' => array($currentID)));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class="col-xs-12 col-sm-6 col-md-4 col-lg-2 col-xl-2 client-blocks">
			<div class="client-block">

<?php the_post_thumbnail('featured');?>
			</div>
			<div class="team-details">
				<a href="<?php the_permalink()?>">
					<div class="overlay-cb">
			    <div class="text-bs">
			              <div class="one-agency-member-name"><?php echo get_the_title($ID);?></div>

								<div class="one-agency-member-role">
<?php the_excerpt();?></div>
								</div>
			    </div>
			       </div></a>
			</div>







</article>


<?php endwhile;
wp_reset_query();
?>
</div>
<div class="digital-main-cta col-md-12">
	<div class="digital-cta-large col-md-6 col-md-offset-3">

<?php echo do_shortcode('[contact-form-7 id="5548" title="Digital - CTA Large"]');?>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>

<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>



<?php
get_footer('digital');
?>