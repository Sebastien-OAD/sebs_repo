<?php
/**
 * Template Name: media services - homepage
 *
 *
 */
get_header();

?>


<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;?>
</div>


<div class="media-services-feed">
<?php if (have_rows('service_group')):
$count = 0;
$tog   = 0;

?>

<?php while (have_rows('service_group')):the_row();

?>
<?php $icon = get_sub_field('service_image');?>
<div class="media-service-block col-md-12 <?php echo (++$count%2?"odd":"even")?>" >
<div class="col-md-6 service-left">
<div class="media-service-image"><img src="<?php echo $icon['url'];?>" alt="<?php echo $icon['alt'];?>" /></div></div>
<div class="col-md-6 service-right">
<div class="media-fancy-text-2"><?php the_sub_field('service_title');?></div>

<div class="media-service-summary"><?php the_sub_field('service_body');?></div></div>
<div class="services-control-area">
<div class="button-<?php echo ($tog)?>">  <a class="btn-animated-lg circle invert">
        <span class="one"></span>
        <span class="two"></span>
    </a> </div></div>
<script>
jQuery(document).ready(function(){

    jQuery(".button-<?php echo ($tog)?>").click(function(){
        jQuery(".tog-<?php echo ($tog)?>").toggleClass("hidden show");
    });
});
</script>



<?php if (have_rows('sub_service')):?>
<div class="media-service-block col-md-12 hidden tog-<?php echo ($tog)?>">



<?php while (have_rows('sub_service')):the_row();

?>
<?php $icon2 = get_sub_field('service_icon');?>
<a href="<?php the_sub_field('service_page_link')?>">
<div class="col-md-3 sub-service">
<div class="media-service-image-sub"><img src="<?php echo $icon2['url'];?>" alt="<?php echo $icon2['alt'];?>" /></div></div></a>

<?php

endwhile;?>
</div> <?php

 else :

// no rows found

endif;

?>
<?php $tog++?>



</div>
<script type='application/ld+json'>
{
	"@context": "http://schema.org/",
	"@type": "Service",
    "provider": {
    "@type": "LocalBusiness",
    "name": "One agency Digital",
    "telephone":"03304004169",
    "address":{"@type":"PostalAddress","streetAddress":"Unit 14, SchoolHouse, Third Avenue, Trafford Park","addressLocality":"Manchester","postalCode":"M171JE","addressCountry":"GB"},
    "image":"http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/logo.png"
  },
	"areaServed": "United Kingdom",
	"serviceType": "service type",
	"alternateName": "<?php the_sub_field('title');?>",
	"description": "<?php the_sub_field('summary');?>",
	"image": {
		"@type": "ImageObject",
		"contentUrl": "<?php echo $icon['url'];?>",
		"embedUrl": "<?php echo $icon['url'];?>"
	},
	"mainEntityOfPage": "<?php the_sub_field('link');?>",
	"name": "<?php the_sub_field('title');?>"
}
</script>

<?php

endwhile;

 else :

// no rows found

endif;

?>
</div>


<script type="text/javascript">

jQuery( " .btn-animated-lg" ).on( "click", function() {
  jQuery(this).toggleClass( "closed" );
});



</script>

<script type="text/javascript">
$(document).ready(function(){
  $(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>

<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>

<?php get_footer('');?>