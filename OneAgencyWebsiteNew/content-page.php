<?php
/**
 * The Template for pages.
 *
 * @package WordPress
 * @subpackage North
 * @since North 1.0
 */
?>
<div id="thmlv-main" class="container row">
<?php
the_content();
?>
<div class="thmlvClear"></div>
<?php
wp_link_pages();
?>
</div>
<?php
if (comments_open() || get_comments_number() != 0) {
	?>
	<div id="thmlvCommentsWrap">
	<?php comments_template();?>
	</div>
	<?php
}
?>