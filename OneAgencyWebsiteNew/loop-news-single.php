<?php
/**
 * The Template for loop blog.
 *
 */
?>
<article id="post-<?php the_ID();?>"  <?php post_class('digital-news-articles');
?>>
<?php if (has_post_thumbnail()) {?>
	<div class="news-feature-image">
	<?php the_post_thumbnail('featured');?>
	</div>
	<?php }?>
<div class="digital-news-body">
<?php

global $more;
$more = 0;
the_content();
?>
</div>
</article>

<?php
$imgsrc  = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
$author  = get_field('author');
$content = get_the_content();

?>

<!--News articles json schema mark up-->
<script type="application/ld+json">{
"@context": "http://schema.org",
"@type": "NewsArticle",
"mainEntityOfPage": {
         "@type": "WebPage",
         "@id": "<?php echo the_permalink()?>"
      },
"headline": " <?php the_title();?>",
"author": "<?php $author = get_field('author');?>
<?php if ($author):?>
	<?php foreach ($author as $post):?>
		<?php setup_postdata($post);?>
			<?php the_title();
?>
<?php endforeach;?>
<?php wp_reset_postdata();?>
<?php endif;?>",
"articleBody": "<?php echo wp_filter_nohtml_kses($content);?>",
"datePublished": "<?php echo get_the_date('Y-m-d');?>",
"dateModified":"<?php the_modified_date('Y-m-d');?>",
"image": {
"@type": "imageObject",
"url": "<?php echo $imgsrc[0];?>"
},
"publisher": {
"@type": "Organization",
"name": "One Agency",
"logo": {
"@type": "imageObject",
"url": "http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/logo.png"
}
}
}</script>




