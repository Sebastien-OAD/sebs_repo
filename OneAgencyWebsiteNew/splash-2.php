<?php
/**
 * Template Name: Splash-2
 */
get_header();
?>
<meta http-equiv="Cache-Control" content="no-store" />
<link rel="stylesheet" type="text/css" href="http://www.oneagencymedia.co.uk/wp-content/themes/north/styles/jquery.multiscroll.css" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
<script type="text/javascript" src="http://www.oneagencymedia.co.uk/wp-content/themes/north/include/jquery.multiscroll.min.js"></script>


<div id="multiscroll">
	<div class="white-logo-left splash-2"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/off-white-logo.png">

	</div>
	<div class="ms-left">
		<div class="ms-section red-back"><div class="splash-title2"><div class="big-text">Visit Media</div><p>A full service media agency, specialising in outdoor advertising</p></div>
			<div class="splash2-button"><a href="http://www.oneagencymedia.co.uk/"><button type="button" class="btn btn-outline-secondary btn-lg">Click Here</button></a></div></div>
		<div class="ms-section red-logo"><div class="business-title-1">One Agency Media</div>
		<div class="red-logo-left"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/logo_red.png"></div>
			<div class="splash-title"><h1>A full service advertising agency delivering exceptional results</h1> </div><div class="splash2-button red"><a href="http://www.oneagencymedia.co.uk/"><button type="button" class="btn btn-outline-secondary btn-lg">Click Here</button></a></div></div>
		<div class="ms-section green-back"><div class="splash-text-body"><p>Providing dynamic digital campaigns across the entire spectrum of online marketing.<br> Our team of creative and passionate people work hard to design and develop innovative digital campaigns that get results. Whether it’s <a href="http://www.oneagencymedia.co.uk/digital/services/seo/">SEO</a> and <a href="http://www.oneagencymedia.co.uk/digital/services/ppc-management/">pay-per-click</a>, or social media and virtual reality – we will deliver. We pride ourselves on keeping on trend in the digital world whilst casting a watchful eye over its exciting future. Start your digital journey with us today.</p></div></div>
		<div class="ms-section red-back"><div class="splash-title3">Media Services</div><div class="splash2-button"><a href="http://www.oneagencymedia.co.uk/"><button type="button" class="btn btn-outline-secondary btn-lg">Click Here</button></a></div></div>
	</div>
	<div class="ms-right">
		<div class="ms-section green-back"><div class="splash-title2"><div class="big-text">Visit Digital</div><p>The digital media department for all your online marketing needs</p></div>
					<div class="splash2-button"><a href="/digital/"><button type="button" class="btn btn-outline-secondary btn-lg">Click Here</button></a></div></div>
		<div class="ms-section red-back"><div class="splash-text-body"><p>Offering a friendly and collaborative environment for out of home (OOH) advertising, including <a href="http://www.oneagencymedia.co.uk/media/services/outdoor/">outdoor</a> and <a href="http://www.oneagencymedia.co.uk/media/services/media/">other media formats</a>.<br>
Based in Manchester, our trusted team will provide creative and effective solutions in providing all kinds of OOH advertising campaigns. From ad vans and billboards, to bus, rail, radio and beyond – we are your one stop shop for out of home. If the endless potential of OOH excites you as much as us, then get in touch with our team.</p></div></div>
		<div class="ms-section"><div class="business-title-2">One Agency Digital</div>
			<div class="splash-title-2"><h2>Digital serivces for today and tomorrow</h2> </div><div class="splash2-button green"><a href="/digital/"><button type="button" class="btn btn-outline-secondary btn-lg">Click Here</button></a></div></div>
		<div class="ms-section green-back"><div class="splash-title3">Digital Services</div><div class="splash2-button"><a href="/digital/"><button type="button" class="btn btn-outline-secondary btn-lg">Click Here</button></a></div></div>
	</div>

</div>


<script type="text/javascript">

$(document).ready(function() {
	$('#multiscroll').multiscroll({
		loopBottom: true,
		navigation: true,


		});
});




</script>