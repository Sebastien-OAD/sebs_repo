<?php
/**
* The Template for single portfolio.
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
get_header();
?>
<div id="thmlvContent">
	<?php
	echo north_switch_header($post->ID, 'skills');
	while (have_posts()) {
		the_post();
		get_template_part('content-portfolio', get_post_format());
	}
	?>
	</div>
</div>
<?php
get_footer();
?>