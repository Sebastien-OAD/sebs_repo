<?php
/**
* The Template for loop video portfolio.
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
if(get_post_meta($post->ID, "_north_customizeColors", true) != '') {
	$captionBg = north_hex2rgb(esc_url(get_post_meta($post->ID, "_north_portfolioCaptionBg", true)));
} else {
	$captionBg = '0, 0, 0';
}
?>
<div id="post-<?php the_ID(); ?>"  <?php post_class('thmlvFullSelected'); ?> data-captionbg="<?php echo $captionBg ?>">
	<?php echo north_video_featured($post->ID, TRUE); ?>
</div>