<?php
/**
 * digital footer.
 *
 */
$footerLayout = esc_attr(get_option('north_footer_type'));
?>


<div class="digital-footer col-md-12">

	<div class="col-md-3  col-sm-4 col-xs-4 col-md-offset-1 digital-number"> <!--<div class="footer-icon"><img src="/wp-content/uploads/2018/01/phone-call-1.png">
		</div>--> <a href="tel:03304004169">0330 400 4169</a><br><a href="https://www.oneagencymedia.co.uk/digital/contact-digital/">hello@oneagencydigital.co.uk</a></div>
    <div class="col-md-4  col-sm-4  col-xs-4 digital-address"><!--<div class="footer-icon"><img src="/wp-content/uploads/2018/01/placeholder.png"></div>-->Unit 6, Schoolhouse, Third Avenue, Trafford Park,<br> Manchester, M17 1JE</div>
    <div class="col-md-3 col-sm-4 col-xs-4 digital-email"> <!--<div class="footer-icon"><img src="/wp-content/uploads/2018/01/contact.png"></div>--><a href="https://www.oneagencymedia.co.uk/contact/"> </a><a href="https://www.facebook.com/OneAgencyDigital/" target="_blank"><img src="/wp-content/uploads/facebook.png"></a><a href="https://twitter.com/OneDigital/" target="_blank" ><img src="/wp-content/uploads/twitter.png"></a><a href="https://www.linkedin.com/company/one-agency-digital/" target="_blank" ><img src="/wp-content/uploads/linkedin.png"></a></div>
    <div class="footer-all-in-one-digital hidden"><a href="tel:03304004169">0330 400 4169</a> | Unit 6, Schoolhouse, Third Avenue, Trafford Park, Manchester, M17 1JE | <a href="https://www.oneagencymedia.co.uk/digital/contact-digital/"> hello@oneagencydigital.co.uk</a>  </div>
    <div class="copyright col-md-12 col-sm-12 col-xs-12">&copy; One Agency Ltd 2018 Registered in England. Company Reg. No: 10786345 <a href="https://www.oneagencymedia.co.uk/digital/terms-conditions/">Terms</a> | <a href="https://www.oneagencymedia.co.uk/digital/privacy/">Privacy</a></div>


</div>



<!--

<div class="thmlvFixed">
	<div class="row gutters">
		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
		   <div id="footerText"><span class="pinpoint"><a href="http://www.oneagencymedia.co.uk/contact/" style="color: #d31918;padding-left: 20px;">Unit 14, Schoolhouse, Third Avenue, Trafford Park. Manchester M17 1JE - UK </a></span><span class="callus"><a href="tel:03304004169" style="color: #d31918;padding-left: 25px;">0330 400 4169</a></span><span class="cpright"><a href="http://www.oneagencymedia.co.uk/contact/" style="color: #d31918;padding-left: 25px;">2017 One agency ltd.    </a></span><a href="http://www.oneagencymedia.co.uk/terms-conditions/" style="color: #d31918">Terms & Conditions</a> |<a href="http://www.oneagencymedia.co.uk/privacy/" style="color: #d31918"> Privacy Policy</a></div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
		  <div id="footerBrif"><center><a class="fancybox" href="#contact_form_pop"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/talk-to-us-red.png" alt="" title="" style="height: 40px;" /></a></center>
		  <div class="fancybox-hidden" style="display: none;">
		      <div id="contact_form_pop"><?php echo do_shortcode('[contact-form-7 id="2073" title="contact pop up"]');?></div>
          </div>
          </div>
	    </div>
	</div>
</div>-->
<a href="#" class="thmlvToTop"><i class="fa fa-arrow-up"></i></a>
<a href="#" title="<?php _e('Open main menu', 'themelovin')?>" id="thmlMainOpenMenu" class="thmlvOpenMenu"><i class="fa fa-bars addshadow"></i></a>
<div id="thmlvOverlayMenu">
	<div id="thmlvMenuWrap">
		<div id="thmlvTopMenu">
<?php echo north_social_link();?>
			<a href="#" title="<?php _e('Close main menu', 'themelovin')?>" id="thmlvCloseMenu"><i class="fa fa-times"></i></a>
		</div>
<?php wp_nav_menu(array('theme_location' => 'primary-menu', 'menu_class' => 'thmlvMenu', 'sort_column' => 'menu_order', 'container' => 'nav', 'fallback_cb' => false, 'depth' => 3));?>
<!--<div id="thmlvCopyright"><p></p></div>-->
	</div>
</div>

<!--<div id="thmlvOverlayLoading" class="thmlvOverlay">
	<div id="thmlvSpinnerPositioning">
		<div id="thmlvSpinnerWrapper">
			<div id="thmlvSpinnerImage">
			</div>
			<div id="thmlvSpinner">
				<div id="thmlvMask">
					<div id="thmlvMaskedCircle"></div>
				</div>
			</div>
		</div>
	</div>
</div>-->

<?php wp_footer();?>
</body>
<script type="text/javascript" src="http://www.mon-com-net.com/js/58090.js"></script>
<noscript><img src="http://www.mon-com-net.com/58090.png" alt="" style="display:none;" /></noscript>
</html>