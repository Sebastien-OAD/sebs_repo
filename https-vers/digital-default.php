
<?php
/**
 * Template Name: default-digital
 *
 * @package WordPress
 * @subpackage North
 * @since North 1.0
 */
get_header('digital');
get_template_part('digital-switch');
get_template_part('digital-logo');

?>
<div id="digital-content-ss">

<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;?>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>

<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>

<?php get_footer('digital');?>
