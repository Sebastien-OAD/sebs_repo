<?php
/**
 * Template Name: media-news
 */
get_header();
get_template_part('media-switch');
get_template_part('media-logo');

?>
<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?>
<div class="digital-news-blocks col-md-12 ">









<?php $loop = new WP_Query(array('post_type' => 'media_news', 'orderby' => 'meta_value', 'meta_type' => 'DATE', 'meta_key' => 'news_article_date', 'posts_per_page' => -1));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
	<?php $format_in = 'd/m/Y';// the format your value is saved in (set in the field options)
$format_out       = 'jS \of F Y';// the format you want to end up with

$date = DateTime::createFromFormat($format_in, get_field('news_article_date'));
?>
<article class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 digital-news <?php echo ($loop->current_post%2 == 0?'odd':'even');?>" style="background-image: url(<?php the_field('background_image');?>);">

			<div class="news-details">
				<div class="digital-news-inner">


			<div class="digital-news-title-grid"><a href="<?php the_permalink()?>"><?php echo get_the_title($ID);?></a></div>
<div class="news-date"><?php echo $date->format($format_out);?></div>
			<div class="digital-news-excerpt">

<?php the_excerpt();?>
</div>
</div>

			</div>





</article>


<?php endwhile;
wp_reset_query();
?>
</div>



<script type="text/javascript">
$(document).ready(function(){
  $(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>

<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>









<?php get_footer();?>