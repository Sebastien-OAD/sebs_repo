<?php
/**
* The Template for loop blog.
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
?>
<article id="post-<?php the_ID(); ?>"  <?php post_class('thmlvGridPost'); ?>>
	<div class="thmlvGridCaption">
		<?php
		echo north_switch_loop_title($post->ID, 1);
		echo north_entry_meta($post->ID);
		//the_excerpt();
		?>
	</div>
	<div class="thmlvGridOverlay" style="display: block;">
		<div class="thmlvGridCaption">
			<?php
			echo north_switch_loop_title($post->ID, 1);
			echo north_entry_meta($post->ID);
			//the_excerpt();
			?>
		</div>
	</div>
</article>