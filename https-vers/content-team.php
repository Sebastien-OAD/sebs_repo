<?php
/**
 * The Template for standard team format.
 *
 * @package WordPress
 * @subpackage North
 * @since North 1.0
 */
?>
<div id="thmlv-main" class="container row">
<?php the_content();?>
<div class="thmlvClear"></div>
<?php wp_link_pages();?>
<!--skills area-->
<div class="container">

<?php

// check if the repeater field has rows of data
if (have_rows('skills')):?>
<div class="my-skills">
<div class="col-md-6 all-skills-title">
	Skills </div>


	<div class="col-md-6">

<?php while (have_rows('skills')):the_row();?>


<div class="team-skill-title"><?php the_sub_field('skill_name');?></div>


<!--<div class="team-skill-value"> <?php the_sub_field('skill_value');?></div>-->

<div class="thmlv-short-progress-bar">
<strong style="opacity: 1;"><i><?php the_sub_field('skill_value');
?></i>%</strong><div class="bar-wrap"><span style="background-color: #DD1A1E; width: 45%;" data-width="<?php the_sub_field('skill_value');?>"></span></div></div>




<?php

endwhile;

 else :

// no rows found

endif;

?>
</div></div></div></div>
<!--related news area-->
<div class="container">
<?php $related_content = get_field('related_content');
$related_content       = array_slice($related_content, 0, 2)
?>
<?php if ($related_content):?>
<div class="related-content-posts">

<div class="relates-news-title">News Stories</div>

<?php foreach ($related_content as $post):?>
<div class="related-content-posts-data col-md-6">

<?php setup_postdata($post);?>
<div class="news-feature-image">
<?php the_post_thumbnail('featured');?>
	</div>
	<div class="related-content-title">
			<a href="<?php the_permalink();?>"><?php the_title();
?></a></div>
<div class="digital-news-excerpt">

<?php the_excerpt();?>
</div>
</div>
<?php endforeach;?>
<?php wp_reset_postdata();?>
<?php endif;?>
</div>





</div>





