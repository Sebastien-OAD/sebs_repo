<?php
/**
* The Template for loop portfolio.
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
$thmlvLoopClient = get_post_meta($post->ID, '_north_portfolioClient', true);
?><article id="post-<?php the_ID(); ?>"  <?php post_class('thmlvGridPortfolio'); ?>>
	<div class="thmlvGridOverlay">
		<div class="thmlvGridCaption">
			<?php
			if($thmlvLoopClient != '') {
			?>
			<span class="thmlvLoopClient">
				<?php echo $thmlvLoopClient; ?>
			</span>
			<?php
			}
			echo north_switch_loop_title($post->ID, 1);
			echo north_post_categories($post->ID, 'skills');
			?>
		</div>
	</div>
	<?php thmlv_portfolio_image($post->ID); ?>
</article>