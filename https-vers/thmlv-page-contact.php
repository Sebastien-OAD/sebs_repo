<?php
/**
* Template Name: Contact
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
get_header();
?>
<div id="thmlvContent">
	<?php
	echo north_switch_header($post->ID);
	while (have_posts()) {
		the_post();
		get_template_part('content', 'page');
	}
	?>
</div>
<script>
	(function($) {
		'use strict';
		$(document).ready(function($){
			jQuery('.thmlvGmap').mobileGmap();
		});
	} (jQuery))
</script>
<?php get_footer(); ?>