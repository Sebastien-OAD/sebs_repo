<?php
/**
 * Template Name: Blog Grid
 *
 * @package WordPress
 * @subpackage North
 * @since North 1.0
 */
get_header();
?>
<img src="https://www.oneagencymedia.co.uk/wp-content/uploads/2017/07/news.jpg" class="" alt="" style="height:100% !important; width:100% !important;"/>    </div>
</div></div></div></div>

	<div id="fws_5978a11ee5a04" class="thmlv-short-full-width-section standard_section   " style="background-color: rgb(221, 26, 24); color: #ffffff; padding-top: 10px; padding-bottom: 10px; font-size: 1.5em !important; ">
    <div class="container"> Keep an eye on this page for news and blogs regarding our latest campaigns, both delivered and ongoing. We will also keep you up-to-date on other news and stories from within the world of advertising, along with further engaging content. </div></div>

<div id="thmlvContent">
<?php
echo north_switch_header($post->ID);
$temp     = $wp_query;
$wp_query = null;
$wp_query = new WP_Query();
$num      = get_option('posts_per_page');
$wp_query->query('showposts='.$num.'&paged='.$paged);
if (have_posts()) {
	while (have_posts()) {
		the_post();
		get_template_part('loop-grid-single', get_post_format());
	}
}
wp_reset_postdata();
north_numeric_posts_nav();
?>
</div>
<?php get_footer();?>