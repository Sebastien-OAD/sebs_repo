<?php
/**
* Template Name: WorkExample
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
get_header();
?>
<div id="thmlvContent">
	<?php
	echo north_switch_header($post->ID);
	while (have_posts()) {
		the_post();?>
		
		
<div id="thmlv-main" class="container row">
	<?php
	the_content();
	?>
	<div class="thmlvClear"></div>
	<?php
		wp_link_pages();
	?>
</div>
<?php
if(comments_open() || get_comments_number() != 0) {
?>
<div id="thmlvCommentsWrap">
	<?php comments_template(); ?>
</div>
<?php
}
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	?>
	</div>
</div>
<?php
get_footer();
?>