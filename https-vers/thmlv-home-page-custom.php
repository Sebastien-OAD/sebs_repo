<?php
/**
* Template Name: Home Page
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
get_header();
?>

<div id="thmlvContent">
	<?php echo north_switch_logo(); ?>
	<div id="thmlvCaptionWrapper">
	<?php
		include_once(ABSPATH.'wp-admin/includes/plugin.php'); 
		if(is_plugin_active('themelovin-portfolio/thmlv-portfolios.php')) {
			$args = array(
				'nopaging' => true,
				'post_type' => 'portfolio',
				'meta_query' => array(
					array(
						'key' => '_north_portfolioSelected',
						'value' => 'on'
					)
				),
				'orderby' => array('menu_order' => 'ASC', 'ID' => 'ASC')
			);
			$wp_query = new WP_Query($args);
			while ($wp_query->have_posts()) : $wp_query->the_post(); 
				get_template_part('loop-selected-caption');
			endwhile;
			
			wp_reset_postdata();
		}
	?>
	</div>
	<div id="thmlvSelectedWrap">
		<?php
		include_once(ABSPATH.'wp-admin/includes/plugin.php'); 
		if(is_plugin_active('themelovin-portfolio/thmlv-portfolios.php')) {
			$args = array(
				'nopaging' => true,
				'post_type' => 'portfolio',
				'meta_query' => array(
					array(
						'key' => '_north_portfolioSelected',
						'value' => 'on'
					)
				),
				'orderby' => array('menu_order' => 'ASC', 'ID' => 'ASC')
			);
			$wp_query = new WP_Query($args);
			while ($wp_query->have_posts()) : $wp_query->the_post(); 
				get_template_part('loop-selected', get_post_format());
			endwhile;
			
			wp_reset_postdata();
		}
		?>
	</div>
</div>
<?php
get_footer();
?>