<?php
/*
 * Template Name: media blog Posts
 * Template Post Type: media_news
 */
get_header();
get_template_part('media-switch');
get_template_part('media-logo');

$format_in  = 'd/m/Y';// the format your value is saved in (set in the field options)
$format_out = 'jS \of F Y';// the format you want to end up with

$date = DateTime::createFromFormat($format_in, get_field('news_article_date'));
?>
<div class="digital-clients-header">
<img src="/wp-content/uploads/2018/01/blog-header.png" class="" alt=""></div>
<div class="digital-clients-header-small">
<img src="/wp-content/uploads/2018/01/blog-small.png" class="" alt=""></div>
<div class="news-breadcrumb">
<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>
<div class="container digi-news">
<div id="digital-news-article col-md-12">
	<div class="digital-news-top">
<h1><?php echo get_the_title($ID);?></h1>
<div class="news-date"><?php echo $date->format($format_out);?></div>
</div>
<?php

if (have_posts()) {
	while (have_posts()) {
		the_post();
		$format = get_post_format();
		get_template_part('loop-news-single', get_post_format());
	}
}
?>
</div>
<hr>
<div class="news-controls">
<?php previous_post_link();
?>  |  <?php next_post_link();
?>
</div>
</div>
</div>
<!--start of cta-->
<div class="media-main-cta col-md-12">
	<div class="media-cta-large col-md-6 col-md-offset-3">

<?php echo do_shortcode('[contact-form-7 id="4334" title="Lets Talk Home Page"]');?>
</div>
</div>
<!--end of cta-->

<?php get_footer();?>