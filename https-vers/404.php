<?php
/**
* The Template for page 404.
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
get_header();
?>
<div id="thmlvContent">
	<?php
	echo north_switch_header();
	?>
	<div id="thmlv-main" class="container row gutters">
		<div class="col span_4">
			<h5 class="thmlvArchiveTitle"><?php _e('Categories', 'themelovin'); ?></h5>
			<ul class="thmlvArchiveList">
			<?php $args = array(
				'show_option_all'    => '',
				'orderby'            => 'name',
				'order'              => 'ASC',
				'style'              => 'list',
				'show_count'         => 1,
				'hide_empty'         => 1,
				'use_desc_for_title' => 0,
				'child_of'           => 0,
				'feed'               => '',
				'feed_type'          => '',
				'feed_image'         => '',
				'exclude'            => '',
				'exclude_tree'       => '',
				'include'            => '',
				'hierarchical'       => true,
				'title_li'           => '',
				'show_option_none'   => __('No categories', 'themelovin'),
				'number'             => NULL,
				'echo'               => 1,
				'depth'              => 0,
				'current_category'   => 0,
				'pad_counts'         => 0,
				'taxonomy'           => 'category',
				'walker'             => 'Walker_Category' );
				 wp_list_categories( $args );
			?>
			</ul>
			
			<h5 class="thmlvArchiveTitle"><?php _e('Feeds', 'themelovin'); ?></h5>
			<ul class="thmlvArchiveList">
				<li><a title="<?php _e( 'Full content', 'themelovin'); ?>" href="feed:<?php bloginfo('rss2_url'); ?>"><?php _e( 'Main RSS', 'themelovin'); ?></a></li>
				<li><a title="<?php _e( 'Comment Feed', 'themelovin'); ?>" href="feed:<?php bloginfo('comments_rss2_url'); ?>"><?php _e( 'Comment Feed', 'themelovin'); ?></a></li>
			</ul>
			
			<h5 class="thmlvArchiveTitle"><?php _e('Archives', 'themelovin'); ?></h5>
			<ul class="thmlvArchiveList">
				<?php wp_get_archives('type=monthly&show_post_count=true'); ?>
			</ul>
		</div><div class="col span_4">
			<h5 class="thmlvArchiveTitle"><?php _e( 'Pages', 'themelovin'); ?></h5>
			<ul class="thmlvArchiveList">
				<?php wp_list_pages("title_li=" ); ?>
			</ul>
		</div><div class="col span_4">
			<h5 class="thmlvArchiveTitle"><?php _e('All Blog Posts', 'themelovin'); ?></h5>
			<ul class="thmlvArchiveList">
				<?php $archive_query = new WP_Query('showposts=1000&cat=-8');
				while ($archive_query->have_posts()) : $archive_query->the_post(); ?>
				<li>
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e( 'Permanent Link to', 'themelovin'); ?><?php the_title(); ?>"><?php the_title(); ?></a>
				(<?php comments_number('0', '1', '%'); ?>)
				</li>
				<?php endwhile; ?>
			</ul>
		</div>
	</div>
</div>
<?php
get_footer();
?>