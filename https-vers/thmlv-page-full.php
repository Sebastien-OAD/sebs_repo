<?php
/**
* Template Name: Full Width
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
get_header();
?>
<div id="thmlvContent">
	<?php
	echo north_switch_header($post->ID);
	while (have_posts()) {
		the_post();
		the_content();
	}
	?>
	<div class="thmlvClear"></div>
	<?php
		wp_link_pages();
	?>
</div>
<?php get_footer(); ?>