<?php
/**
* The Template for loop team.
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
?><article id="post-<?php the_ID(); ?>"  <?php post_class('thmlvGridTeam'); ?>>
	<div class="thmlvGridOverlay" style="display: block;">
		<div class="thmlvGridCaption">
			<?php
			//echo north_switch_loop_title($post->ID, 1);
			//echo north_post_categories($post->ID, 'tasks');
			?>
		</div>
		<center>
			<?php
			echo north_switch_loop_title($post->ID, 1);
			echo north_post_categories($post->ID, 'tasks');
			?>
		</center>
	</div>
	<?php //echo north_switch_loop_title($post->ID, 1); ?>
    <?php //echo north_post_categories($post->ID, 'tasks'); ?>
	<?php thmlv_portfolio_image($post->ID); ?>
</article>