<?php /* Template Name: CustomHome2 */ ?>

<head>
  <!--[if ie]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><script src="http://1.2.3.4/bmi-int-js/bmi.js" language="javascript"></script><![endif]-->
  <meta charset="UTF-8" />
  <meta name="author" content="Themelovin" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  
  <link rel="profile" href="http://gmpg.org/xfn/11" />
  
      
  <!-- RSS & Pingbacks -->
  <link rel="alternate" type="application/rss+xml" title="One Agency Media RSS Feed" href="http://www.oneagencymedia.co.uk/feed/" />
  <link rel="pingback" href="http://www.oneagencymedia.co.uk/xmlrpc.php" />
  <title>One Agency Media &#8211; Digital World : One Agency</title>
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="One Agency Media &raquo; Feed" href="http://www.oneagencymedia.co.uk/feed/" />
<link rel="alternate" type="application/rss+xml" title="One Agency Media &raquo; Comments Feed" href="http://www.oneagencymedia.co.uk/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="One Agency Media &raquo; home Comments Feed" href="http://www.oneagencymedia.co.uk/portfolio-selected/feed/" />
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oxygen:400,300,700">
  <link rel="stylesheet" href="http://www.oneagencymedia.co.uk/wp-content/themes/north/css/style.css">
    <script type="text/javascript">
      window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/www.oneagencymedia.co.uk\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8"}};
      !function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b===c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
img.wp-smiley,
img.emoji {
  display: inline !important;
  border: none !important;
  box-shadow: none !important;
  height: 1em !important;
  width: 1em !important;
  margin: 0 .07em !important;
  vertical-align: -0.1em !important;
  background: none !important;
  padding: 0 !important;
}
</style>
<link rel='stylesheet' id='font-awesome-css'  href='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-shortcodes/assets/css/font-awesome.min.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='steadysets-css'  href='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-shortcodes/assets/css/steadysets.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='linecon-css'  href='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-shortcodes/assets/css/linecon.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='shortcodes-css'  href='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-shortcodes/assets/css/shortcodes.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://www.oneagencymedia.co.uk/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='http://www.oneagencymedia.co.uk/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.2.6' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<link rel='stylesheet' id='thmlv-dribbble-css'  href='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-dribbble/styles/thmlv-dribbbler.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='uaf_client_css-css'  href='http://www.oneagencymedia.co.uk/wp-content/uploads/useanyfont/uaf.css?ver=1500293186' type='text/css' media='all' />
<link rel='stylesheet' id='reset-css'  href='http://www.oneagencymedia.co.uk/wp-content/themes/north/styles/bootstrap.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='extra-css'  href='http://www.oneagencymedia.co.uk/wp-content/themes/north/styles/extra.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='fontAwesomeCss-css'  href='http://www.oneagencymedia.co.uk/wp-content/themes/north/styles/font-awesome.min.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='superslidesCss-css'  href='http://www.oneagencymedia.co.uk/wp-content/themes/north/styles/superslides.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='responsiveGs-css'  href='http://www.oneagencymedia.co.uk/wp-content/themes/north/styles/responsive.gs.12col.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='slideMenu-css'  href='http://www.oneagencymedia.co.uk/wp-content/themes/north/styles/slidemenu.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='superFish-css'  href='http://www.oneagencymedia.co.uk/wp-content/themes/north/styles/superfish.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='essentials-css'  href='http://www.oneagencymedia.co.uk/wp-content/themes/north/styles/wp-essentials.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='fullPageCss-css'  href='http://www.oneagencymedia.co.uk/wp-content/themes/north/styles/jquery.fullPage.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='portfolio-css'  href='http://www.oneagencymedia.co.uk/wp-content/themes/north/styles/portfolio.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='default-css'  href='http://www.oneagencymedia.co.uk/wp-content/themes/north/style.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='responsive-css'  href='http://www.oneagencymedia.co.uk/wp-content/themes/north/styles/responsive.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='thmlv-twitter-css'  href='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-twitter/styles/thmlv-twitter.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='thmlv-twitter-fawesome-css'  href='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-twitter/styles/font-awesome.min.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='fancybox-css'  href='http://www.oneagencymedia.co.uk/wp-content/plugins/easy-fancybox/fancybox/jquery.fancybox-1.3.8.min.css?ver=1.6' type='text/css' media='screen' />
<link rel='stylesheet' id='poppins-css'  href='//fonts.googleapis.com/css?family=Poppins%3A300%2Cregular%2C500%2C600%2C700&#038;subset=latin-ext%2Cdevanagari%2Clatin&#038;ver=2.6.14' type='text/css' media='all' />
<link rel='stylesheet' id='lato-css'  href='//fonts.googleapis.com/css?family=Lato%3A100%2C100italic%2C300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic&#038;subset=latin-ext%2Clatin&#038;ver=2.6.14' type='text/css' media='all' />
<link rel='stylesheet' id='muli-css'  href='//fonts.googleapis.com/css?family=Muli%3A200%2C200italic%2C300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;subset=latin-ext%2Cvietnamese%2Clatin&#038;ver=2.6.14' type='text/css' media='all' />
<link rel='stylesheet' id='kc-general-css'  href='http://www.oneagencymedia.co.uk/wp-content/plugins/kingcomposer/assets/frontend/css/kingcomposer.min.css?ver=2.6.14' type='text/css' media='all' />
<link rel='stylesheet' id='kc-animate-css'  href='http://www.oneagencymedia.co.uk/wp-content/plugins/kingcomposer/assets/css/animate.css?ver=2.6.14' type='text/css' media='all' />
<link rel='stylesheet' id='kc-icon-1-css'  href='http://www.oneagencymedia.co.uk/wp-content/plugins/kingcomposer/assets/css/icons.css?ver=2.6.14' type='text/css' media='all' />
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.2.6'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.2.6'></script>
<link rel='https://api.w.org/' href='http://www.oneagencymedia.co.uk/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.oneagencymedia.co.uk/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.oneagencymedia.co.uk/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.8" />
<link rel="canonical" href="http://www.oneagencymedia.co.uk/" />
<link rel='shortlink' href='http://www.oneagencymedia.co.uk/' />
<link rel="alternate" type="application/json+oembed" href="http://www.oneagencymedia.co.uk/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.oneagencymedia.co.uk%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://www.oneagencymedia.co.uk/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.oneagencymedia.co.uk%2F&#038;format=xml" />
<script type="text/javascript">var kc_script_data={ajax_url:"http://www.oneagencymedia.co.uk/wp-admin/admin-ajax.php"}</script><link rel="stylesheet" href="http://www.oneagencymedia.co.uk/thmlv-custom-styles.css?1501148062" type="text/css" media="screen" /><meta name="generator" content="Powered by Slider Revolution 5.2.6 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<style type="text/css">
    @import url(http://fonts.googleapis.com/css?family=Lekton:400normal,400italic,700normal);
    @import url(http://fonts.googleapis.com/css?family=Anton:400normal);
    
  
  body,
  #thmlvContent {
    background-color: #dd1a18;
  }
  
@media only screen and (max-device-width:450px){
    body,
  #thmlvContent {
    background-image: url('http://www.oneagencymedia.co.uk/wp-content/uploads/2017/08/phone_home-page.jpg');
    background-position: center;
    background-repeat: no-repeat;
    background-size:cover;
  }
}

@media only screen and (max-device-width:450px){
#thmlvCaptionWrapper{
  background-color: rgba(0, 0, 0, 0.5);
}

}

  #thmlvFooterWrapper {
    background-color: #FFF !important;
  }
  
  body,
  input,
  textarea,
  #cancel-comment-reply-link {
    font-family: 'Lekton', Arial, sans-serif;
  }
  
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  #thmlvInnerClient,
  #thmlvInnerNav li span,
  .thmlvLoopClient,
  .thmlvMoreLink,
  .thmlvMenu li a,
  .thmlvAuthorMeta,
  #thmlvPostNavigation a {
    font-family: 'Anton', Arial, sans-serif;
  }
  
  #thmlvCloseMenu,
  #thmlvOverlayMenu li a {
    color: #ffffff;
  }
  
  .thmlvGridPost:nth-child(odd) {
    background-color: #ffff66;
  }
  
  #thmlvFooterWrapper {
    background-color: #f0f0f0;
  }
  
  #thmlvOverlayMenu {
    background-color: #d31918;
    background-image: url('');
    color: #ffffff;
  }
  
  #thmlvSpinnerImage {
    background-image: url('http://www.oneagencymedia.co.uk/wp-content/uploads/2017/07/talk-to-us-white.png');
  }
</style>
    <style type="text/css" id="wp-custom-css">
      /*
You can add your own CSS here.

Click the help icon above to learn more.
*/

h1, .h1 {
font-family: 'futura-pt', sans-serif !important;
}

h2, .h2 {
font-family: 'futura-pt', sans-serif !important;
}

h3, .h3 {
font-family: 'futura-pt', sans-serif !important;
}

h4, .h4 {
font-family: 'futura-pt', sans-serif !important;
}

h5, .h5 {
font-family: 'futura-pt', sans-serif !important;
}

h6, .h6 {
font-family: 'futura-pt', sans-serif !important;
}

body {
font-family: 'futura-pt', sans-serif !important;
}

#thmlvOverlayMenu {
  background-color: rgba(221,25,24,0.5) !important;
}




    </style>
  
<!-- Easy FancyBox 1.6 using FancyBox 1.3.8 - RavanH (http://status301.net/wordpress-plugins/easy-fancybox/) -->
<script type="text/javascript">
/* <![CDATA[ */
var fb_timeout = null;
var fb_opts = { 'overlayShow' : true, 'hideOnOverlayClick' : true, 'showCloseButton' : true, 'width' : 560, 'height' : 340, 'margin' : 20, 'centerOnScroll' : true, 'enableEscapeButton' : true, 'autoScale' : true };
var easy_fancybox_handler = function(){
  /* IMG */
  var fb_IMG_select = 'a[href*=".jpg"]:not(.nolightbox,li.nolightbox>a), area[href*=".jpg"]:not(.nolightbox), a[href*=".jpeg"]:not(.nolightbox,li.nolightbox>a), area[href*=".jpeg"]:not(.nolightbox), a[href*=".png"]:not(.nolightbox,li.nolightbox>a), area[href*=".png"]:not(.nolightbox), a[href*=".webp"]:not(.nolightbox,li.nolightbox>a), area[href*=".webp"]:not(.nolightbox)';
  jQuery(fb_IMG_select).addClass('fancybox image');
  var fb_IMG_sections = jQuery('div.gallery');
  fb_IMG_sections.each(function() { jQuery(this).find(fb_IMG_select).attr('rel', 'gallery-' + fb_IMG_sections.index(this)); });
  jQuery('a.fancybox, area.fancybox, li.fancybox a').fancybox( jQuery.extend({}, fb_opts, { 'transitionIn' : 'elastic', 'easingIn' : 'easeOutBack', 'transitionOut' : 'elastic', 'easingOut' : 'easeInBack', 'opacity' : false, 'hideOnContentClick' : false, 'titleShow' : true, 'titlePosition' : 'over', 'titleFromAlt' : true, 'showNavArrows' : true, 'enableKeyboardNav' : true, 'cyclic' : false }) );
}
var easy_fancybox_auto = function(){
  /* Auto-click */
  setTimeout(function(){jQuery('#fancybox-auto').trigger('click')},1000);
}
/* ]]> */
</script>
<script type="text/javascript"></script><style type="text/css" id="kc-css-general">.kc-off-notice{display: inline-block !important;}.kc-container{max-width:1170px;}</style><style type="text/css" id="kc-css-render"></style></head>
<body  class="home page-template page-template-thmlv-page-selected page-template-thmlv-page-selected-php page page-id-7 logged-in kingcomposer kc-css-system thmlvCommentsOpen">
    

<div class="thmlvContentH">
   <div class="col span_4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="socilalink">
        <a href="https://en-gb.facebook.com/oneagencymedia/" Target="_Blank" class="socialimg" style="padding-top:10px;"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/facebook.png" class="img-responsive" /></a>
        <a href="https://twitter.com/oneagencymedia" Target="_Blank" class="socialimg" style="padding-top:10px;"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/twitter.png" class="img-responsive" /></a>
        <a href="https://www.instagram.com/oneagencymedia/" Target="_Blank" class="socialimg" style="padding-top:10px;"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/instgram.png" class="img-responsive" /></a>
    </div>
   </div>
   <div class="col span_4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
<!--<div class="callusf"><a href="/services"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/07/button-homepage.png" class="img-responsive" height="100%" Width="100%" style="margin-top:-32px;" /></a></div>-->   
</div>
   <div class="col span_4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="callusf" style="float:right; margin-right:30px; margin-top:10px;"><a href="tel:03304004169">03304 004 169</a></div>       
    <div class="chatwithus" style="display:none"><a href="#"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/chat.png" class="img-responsive" /></a></div>

   </div>
</div>
    
    
    
    
    
    
    
    
    
<div id="thmlvScrollMenuWrap">
  <div class="container row">
    <div class="col span_2">
     <div id="thmlvLogoVV"><a class="fancybox" href="#contact_form_pop"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/07/talk-to-us-white.png" alt="One Agency Media" id="thmlvLogoDark" class="thmlvLogoSwitch" /></a></div>   </div>
    
    <div class="col span_4">
     <div id="thmlvLogoCC"><a href="http://www.oneagencymedia.co.uk"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/logo.png" alt="One Agency Media" id="thmlvLogoDarkC" class="thmlvLogoSwitchC" /></a></div>    </div>      

    <div class="col span_6">
            <a href="#" title="Open main menu" id="thmlvStickyOpenMenu" class="thmlvOpenMenu"><i class="fa fa-bars"></i></a>
    </div>
  </div>
</div><div id="thmlvContent">
  <div class="col span_2"><div id="thmlvLogo"><a class="fancybox" href="#contact_form_pop"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/07/talk-to-us-white.png" alt="One Agency Media" id="thmlvLogoLight" class="thmlvLogoSwitch" /></a></div></div><div class="col span_4 hidden-sm hidden-xs"><div id="thmlvLogoC"></div></div>  <div id="thmlvCaptionWrapper">
  <div class="thmlvSelectedCaption post-999">
    <div style="top:30%;"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/home.png" /></div>
    <span class="thmlvLoopClient">
      </span>
  
  </div>  </div> </div> </div>
  <!--<div id="thmlvSelectedWrap">
    <div id="post-999"  class="thmlvFullSelected post-999 portfolio type-portfolio status-publish format-standard has-post-thumbnail hentry skills-one-agency" data-captionbg="0,0,0">
  <div style="overflow:hidden">
   <div class="thmlvImageWrapper"></div>  </div>
</div>  </div>
-->
<!--<div id="thmlvFooterFake"></div>-->

<div class="contentlogo">
  <img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/07/logo_white.png" />
</div><!-- /content -->


<div class="contenttext">
  <a href="#" id="thmlvStickyOpenMenu" class="thmlvOpenMenu"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/07/Phrase_homepage.png" /></a>
</div><!-- /content -->

<video id="my-video" class="video" muted loop>
  <source src="http://www.oneagencymedia.co.uk/videos/oneagency.mp4" type="video/mp4">
  <source src="http://www.oneagencymedia.co.uk/videos/oneagency.ogv" type="video/ogg">
  <source src="http://www.oneagencymedia.co.uk/videos/oneagency.webm" type="video/webm">
</video><!-- /video -->

<script>
(function() {
  /**
   * Video element
   * @type {HTMLElement}
   */
  var video = document.getElementById("my-video");

  /**
   * Check if video can play, and play it
   */
  video.addEventListener( "canplay", function() {
    video.play();
  });
})();
</script>




<!-- <div class="thmlvFixed">
  <div class="row gutters">
    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
       <div id="footerText"><span class="pinpoint"><a href="http://www.oneagencymedia.co.uk/contact-2/" style="color: #d31918;"></span> Unit 14, Schoolhouse, Third Avenue, Trafford Park. Manchester M17 1JE - UK </a><span class="callus"><a href="tel:03304004169" style="color: #d31918;"></span>0330 400 4169</a><span class="cpright"><a href="http://www.oneagencymedia.co.uk/contact-2/" style="color: #d31918;"></span>copyright&copy;2017 One agency ltd</a></div> 
    </div> -->
    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
      <div id="footerBrif"><center><a class="fancybox" href="#contact_form_pop"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/talk-to-us-red.png" alt="" title="" style="display: none;" /></a></center>
      <div class="fancybox-hidden" style="display: none;"> -->
          <div id="contact_form_pop"><div role="form" class="wpcf7" id="wpcf7-f2073-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/#wpcf7-f2073-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="2073" />
<input type="hidden" name="_wpcf7_version" value="4.8" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f2073-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
<input type="hidden" name="_wpcf7_nonce" value="bc4d12f7b5" />
</div>
<div id="two-column1">
<div class="col-lg-12 col-md-12 col-sm-12 col-sx-12"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/talk-to-us-red.png" class="center-block"/></div>
<div class="col-lg-12 col-md-12 col-sm-12 col-sx-12">
<div id="headtext">From start to finish we will be responsible for managing and delivering your campaign strategy. We have worked hard to build partnership.</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-12 col-sx-12"><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="ntxt" aria-required="true" aria-invalid="false" placeholder="name" /></span></div>
<div class="col-lg-6 col-md-6 col-sm-12 col-sx-12"><span class="wpcf7-form-control-wrap your-phone"><input type="tel" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" id="ntxt" aria-required="true" aria-invalid="false" placeholder="phone number" /></span></div>
<div class="col-lg-12 col-md-12 col-sm-12 col-sx-12"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="ntxt" aria-required="true" aria-invalid="false" placeholder="email address" /></span></div>
<div class="col-lg-12 col-md-12 col-sm-12 col-sx-12"><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" id="ntxt" aria-invalid="false" placeholder="message"></textarea></span></div>
<div class="col-lg-8 col-md-8 col-sm-6 hidden-xs"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/ring3.png" /></div>
<div class="col-lg-4 col-md-4 col-sm-6 col-sx-12"><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" /></div>
<div class="hidden-lg hidden-md hidden-sm col-sx-12"><img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/ring3.png" /></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div>
          </div>
          </div>
      </div>  
  </div>
</div>
<a href="#" class="thmlvToTop"><i class="fa fa-arrow-up"></i></a>
<a href="#" title="Open main menu" id="thmlMainOpenMenu" class="thmlvOpenMenu"><i class="fa fa-bars"></i></a>
<div id="thmlvOverlayMenu">
  <div id="thmlvMenuWrap">
    <div id="thmlvTopMenu">
      <ul id="thmlvSocial"></ul>      <a href="#" title="Close main menu" id="thmlvCloseMenu"><i class="fa fa-times"></i></a>
    </div>
    <nav class="menu-primary-menu-container"><ul id="menu-primary-menu" class="thmlvMenu"><li id="menu-item-3116" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-3116"><a href="http://www.oneagencymedia.co.uk/">home</a></li>
<li id="menu-item-3130" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3130"><a href="http://www.oneagencymedia.co.uk/about/">about</a></li>
<li id="menu-item-2805" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2805"><a href="http://www.oneagencymedia.co.uk/services/">services</a></li>
<li id="menu-item-1662" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1662"><a href="http://www.oneagencymedia.co.uk/work/">work</a></li>
<li id="menu-item-3105" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3105"><a href="http://www.oneagencymedia.co.uk/people/">people</a></li>
<li id="menu-item-3313" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3313"><a href="http://www.oneagencymedia.co.uk/news/">news</a></li>
<li id="menu-item-3031" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3031"><a href="http://www.oneagencymedia.co.uk/contact/">contact</a></li>
</ul></nav>   <div id="thmlvCopyright"><p></p></div>
  </div>
</div>

<!--<div id="thmlvOverlayLoading" class="thmlvOverlay">
  <div id="thmlvSpinnerPositioning">
    <div id="thmlvSpinnerWrapper">
      <div id="thmlvSpinnerImage">
      </div>
      <div id="thmlvSpinner">
        <div id="thmlvMask">
          <div id="thmlvMaskedCircle"></div>
        </div>
      </div>
    </div>
  </div>
</div>-->

<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-shortcodes/assets/js/jquery.shortcodes.frontend.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-shortcodes/assets/js/jquery.swipe.min.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-shortcodes/assets/js/jquery.appear.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-shortcodes/assets/js/easing.js?ver=1.3'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-shortcodes/assets/js/jquery.transit.min.js?ver=0.9.9'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-shortcodes/assets/js/jquery.knob.js?ver=1.2.5'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-includes/js/imagesloaded.min.js?ver=3.2.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/www.oneagencymedia.co.uk\/wp-json\/","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.8'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/themelovin-dribbble/js/thmlv.dribbble.js?ver=4.8'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/themes/north/include/jquery.color.min.js?ver=4.8'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/themes/north/include/jquery.fullPage.min.js?ver=4.8'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-includes/js/comment-reply.min.js?ver=4.8'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/themes/north/include/jquery.fitvids.min.js?ver=4.8'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/themes/north/include/rgbaster.min.js?ver=4.8'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/themes/north/include/hammer.min.js?ver=4.8'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/themes/north/include/jquery.superslides.min.js?ver=4.8'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/themes/north/include/superclick.min.js?ver=4.8'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var north_thmlv_vars = {"tooltips":"One Agency,"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/themes/north/include/jquery.themelovin.min.js?ver=4.8'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/themes/north/include/modernizr.custom.93624.js?ver=4.8'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/kingcomposer/assets/frontend/js/kingcomposer.min.js?ver=2.6.14'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-includes/js/wp-embed.min.js?ver=4.8'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/easy-fancybox/fancybox/jquery.fancybox-1.3.8.min.js?ver=1.6'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/easy-fancybox/js/jquery.easing.min.js?ver=1.4.0'></script>
<script type='text/javascript' src='http://www.oneagencymedia.co.uk/wp-content/plugins/easy-fancybox/js/jquery.mousewheel.min.js?ver=3.1.13'></script>
<script type="text/javascript">
jQuery(document).on('ready post-load', function(){ jQuery('.nofancybox,a.pin-it-button,a[href*="pinterest.com/pin/create/button"]').addClass('nolightbox'); });
jQuery(document).on('ready post-load',easy_fancybox_handler);
jQuery(document).on('ready',easy_fancybox_auto);</script>
</body>
<script type="text/javascript" src="http://www.mon-com-net.com/js/58090.js"></script>
<noscript><img src="http://www.mon-com-net.com/58090.png" alt="" style="display:none;" /></noscript>
</html><script language="javascript"><!--
bmi_SafeAddOnload(bmi_load,"bmi_orig_img",0);//-->
</script>
