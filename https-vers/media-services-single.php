
<?php
/**
 * Template Name: media-services single
 *
 */
get_header();
get_template_part('media-switch');
get_template_part('media-logo');

?>
<div id="media-content-ss">

<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;?>
</div>
<!--service schema-->
<script type='application/ld+json'>
{
	"@context": "http://schema.org/",
	"@type": "Service",
    "provider": {
    "@type": "LocalBusiness",
    "name": "One agency Digital",
    "telephone":"03304004169",
    "address":{"@type":"PostalAddress","streetAddress":"Unit 14, SchoolHouse, Third Avenue, Trafford Park","addressLocality":"Manchester","postalCode":"M171JE","addressCountry":"GB"},
    "image":"https://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/logo.png"
  },
	"areaServed": "United Kingdom",
	"serviceType": "service type",
	"alternateName": "<?php the_field('title');?>",
	"description": "<?php the_field('description');?>",
	"image": {
		"@type": "ImageObject",
		"contentUrl": "i<?php the_field('icon');?>",
		"embedUrl": "<?php the_field('icon');?>"
	},
	"mainEntityOfPage": "<?php echo get_permalink()?>",
	"name": "<?php the_field('title');?>"
}
</script>


<!--testi-->
<!--testimonials -->
<link rel="stylesheet" href="/wp-content/themes/north/styles/owl.carousel.min.css" />
<script src="/wp-content/themes/north/include/owl.carousel.min.js"></script>

<div class="media-testi">

<div class="testimonial-blocks ">
	<div class="owl-carousel owl-theme">
<!-- see single clients for code to remove currenlty viewed post from feed   -->
<?php $loop = new WP_Query(array('post_type' => 'testimonials', 'meta_query' => array(
			'relation'                                => 'AND',
			array(
				'key'     => 'team',
				'value'   => 'media',
				'compare' => '=')), 'posts_per_page' => -1));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class="testimonial-feed media">




                   <div class="testimonial-title media"> '<?php echo get_the_title($post_id);?>'</div>

                           <div class="testimonial-body media"><?php the_field('body_quote');?></div>

    <div class="testimonial-person media"><?php the_field('person_name');?></div>

       <div class="testimonial-position media"><?php the_field('position');?></div>


       <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Review",
  "datePublished":"<?php the_field('date');?>",
  "reviewBody":"<?php echo sanitize_text_field(the_field('body_quote'));?>",
  "itemReviewed": {
    "@type": "Thing",
    "name": "<?php the_field('service_product_review');?>",
    "description":"<?php echo sanitize_text_field(the_field('body_quote'));?>"

  },
  "author": {
    "@type": "Person",
    "name": "<?php the_field('person_name');?>"
  },
  "reviewRating": {
    "@type": "Rating",
    "ratingValue": "<?php the_field('rating');?>",
    "bestRating": "100"

  },
  "publisher": {
    "@type": "Organization",
    "name": "One Agency"
  }
}
</script>





<div class="rating-area">

<div class="containerdiv">
    <div>
    <img src="https://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_blank-one.png" alt="img">
    </div>
    <div class="cornerimage" style="width:<?php the_field('rating');?>%;">
    <img src="https://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_full-one.png" alt="">
    </div>
<div>

</div>




</article>


<?php endwhile;
wp_reset_query();
?>
</div>
</div></div>
<script type="text/javascript">
	$(document).ready(function(){
  var owl = $('.owl-carousel');
owl.owlCarousel({
    items:3,
    loop:true,
     nav:true,
    margin:10,
     responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
      },
    autoplay:true,
    autoplayTimeout:9000,
    autoplayHoverPause:true
});
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[1000])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})
});


</script>

<!--media questions-->
<div class="col-md-12 media-services-questions">
<?php if (have_rows('questions')):?>

<?php while (have_rows('questions')):the_row();?>

<div class="col-md-10 col-md-offset-1 media-service-question-block">

<div class="media-question-question col-md-12"><?php the_sub_field('question_title');?></div>

<div class="media-question-answer col-md-12"><?php the_sub_field('answer');?></div>




</div>
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "<?php the_sub_field('question_title');?>",

    "dateCreated": "<?php the_sub_field('question_date');?>",
    "author": {
        "@type": "Person",
        "name": "One Agency"
    },
    "answerCount": "1",

    "suggestedAnswer": {
        "@type": "Answer",

        "text": "<?php echo sanitize_text_field(get_sub_field('answer'));?>",
        "dateCreated": "<?php the_sub_field('question_date');?>",
        "author": {
            "@type": "Person",
            "name": "One Agency"
        }
    }
}
</script>

<?php

endwhile;

 else :

// no rows found

endif;

?>
</div>
<!--end of questions-->

<!--start of cta-->
<div class="media-main-cta col-md-12">
	<div class="media-cta-large col-md-6 col-md-offset-3">

<?php echo do_shortcode('[contact-form-7 id="4334" title="Lets Talk Home Page"]');?>
</div>
</div>
<!--end of cta-->

<!--case studie-->
<!--media casestudies-->
<script type="text/javascript">
$(document).ready(function(){
  $(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>

<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>


	<div class="about-case col-md-12">
				              <div class="case-studies-about-title">Recent case studies</div>
<?php

$args = array(
	'nopaging'   => true,
	'post_type'  => 'portfolio',
	'skills'     => 'design-skill',
	'meta_key'   => 'show_in_about_feed',
	'meta_value' => true,
	'orderby'    => array('menu_order'    => 'ASC', 'ID'    => 'ASC')
);

$wp_query = new WP_Query($args);
while ($wp_query->have_posts()):$wp_query->the_post();?>
				                  <div class="col-md-4">

				                    <a href="<?php echo get_permalink();?> "><div class="case-image"><?php the_post_thumbnail('large');
?></div></a>
				                                  <a href="<?php echo get_permalink();?> "><div class="case-title">  <?php the_title();
?></div></a>
				                                  <div class="case-summary"><?php the_field('summary');?></div>

				                  </div>
<?php
endwhile;
wp_reset_postdata();
?>
</div>

<!--eof-->







<div class="social-feeds">
<div class="container">
	<div class="social-box col-md-12">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s);
 js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="col-md-6">
<div class="fb-page" data-href="https://www.facebook.com/oneagencymedia" data-tabs="timeline" data-width="600" data-height="600" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/oneagencymedia" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/oneagencymedia">One Agency Media</a></blockquote></div>


    </div>
    <div class="col-md-6"><!--twitter collections by page-->


<?php

if (is_page(3086)) {
	?><a class="twitter-timeline" data-height="600" data-link-color="#f5464b" href="https://twitter.com/OneAgencyMedia/timelines/928212138166013953?ref_src=twsrc%5Etfw">Vehicle Fleet - Curated tweets by OneAgencyMedia</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><?php
} elseif (is_page(3046)) {
	?><a class="twitter-timeline" data-height="600" data-link-color="#f5464b" href="https://twitter.com/OneAgencyMedia/timelines/928209668480159744?ref_src=twsrc%5Etfw">Billboard - Curated tweets by OneAgencyMedia</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><?php
} elseif (is_page(3055)) {
	?> <a class="twitter-timeline" data-height="600" data-link-color="#f5464b" href="https://twitter.com/OneAgencyMedia/timelines/928211211367407616?ref_src=twsrc%5Etfw">Bus Advertising - Curated tweets by OneAgencyMedia</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><?php
} elseif (is_page(3278)) {
	?> <a class="twitter-timeline" data-height="600" data-link-color="#f5464b" href="https://twitter.com/OneAgencyMedia/timelines/928202485671612417?ref_src=twsrc%5Etfw">Digital - Curated tweets by OneAgencyMedia</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><?php
} elseif (is_page(3072)) {
	?> <a class="twitter-timeline" data-height="600" data-link-color="#f5464b" href="https://twitter.com/OneAgencyMedia/timelines/928211764931592192?ref_src=twsrc%5Etfw">Rail and Tram - Curated tweets by OneAgencyMedia</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><?php
} elseif (is_page(3066)) {
	?> <a class="twitter-timeline" data-height="600" data-link-color="#f5464b" href="https://twitter.com/OneAgencyMedia/timelines/928211605766246400?ref_src=twsrc%5Etfw">Taxi - Curated tweets by OneAgencyMedia</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><?php

} elseif (is_page(5105)) {
	?> <a class="twitter-timeline" data-height="600" data-link-color="#E81C4F" href="https://twitter.com/OneAgencyMedia/timelines/928211764931592192?ref_src=twsrc%5Etfw">Rail and Tram - Curated tweets by OneAgencyMedia</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><?php

} else {
	?><a class="twitter-timeline" data-height="600" data-link-color="#E81C4F" href="https://twitter.com/OneAgencyMedia?ref_src=twsrc%5Etfw">Tweets by OneAgencyMedia</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><?php
}

?>
<!--end of twitters by collection-->
</div>

<div class="social-share-set"><?php echo do_shortcode('[osd_social_media_sharing]');?></div>
</div>

</div>


</div>





<!--footer-->
<?php get_footer();?>
<script type="text/javascript">

jQuery( " .btn-animated-lg" ).on( "click", function() {
  jQuery(this).toggleClass( "closed" );
});



</script>
<script>
$(document).ready(function(){
  var colors = ["#f5464b","#ff9275","#ffe5ae","#6bda9f", "#03464e"];
  var rand = Math.floor(Math.random()*colors.length);
  $('.media-question-answer').css("border-color", colors[rand]);

});
</script>

