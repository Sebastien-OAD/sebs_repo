<?php
/**
 * Template Name: digital services
 */
get_header('digital');
get_template_part('digital-switch');
get_template_part('digital-logo');
?>
<!--<h1><?php echo $post->ID?></h1>-->
<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?>
<div class="col-md-12 digital-services-feed">
<?php if (have_rows('repeater')):?>

<?php while (have_rows('repeater')):the_row();

?>
<?php $icon = get_sub_field('icon');?>
<div class="col-md-4 col-sm-6 col-lg-4  digital-service-block">

<div class="services-icon"><a href="<?php the_sub_field('link');?>"><img src="<?php echo $icon['url'];?>" alt="<?php echo $icon['alt'];?>" /></a></div>

<div class="services-title"><a href="<?php the_sub_field('link');?>"><?php the_sub_field('title');
?></a></div>

<div class="services-summary"><?php the_sub_field('summary');
?></div>

<div class="services-link"><a class="services-bitton-link" href="<?php the_sub_field('link');?>">Read More</a></div>

</div>
<script type='application/ld+json'>
{
	"@context": "http://schema.org/",
	"@type": "Service",
    "provider": {
    "@type": "LocalBusiness",
    "name": "One agency Digital",
    "telephone":"03304004169",
    "address":{"@type":"PostalAddress","streetAddress":"Unit 14, SchoolHouse, Third Avenue, Trafford Park","addressLocality":"Manchester","postalCode":"M171JE","addressCountry":"GB"},
    "image":"https://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/logo.png"
  },
	"areaServed": "United Kingdom",
	"serviceType": "service type",
	"alternateName": "<?php the_sub_field('title');?>",
	"description": "<?php the_sub_field('summary');?>",
	"image": {
		"@type": "ImageObject",
		"contentUrl": "<?php echo $icon['url'];?>",
		"embedUrl": "<?php echo $icon['url'];?>"
	},
	"mainEntityOfPage": "<?php the_sub_field('link');?>",
	"name": "<?php the_sub_field('title');?>"
}
</script>

<?php

endwhile;

 else :

// no rows found

endif;

?>
</div>

<div class="digital-main-cta col-md-12">
	<div class="digital-cta-large col-md-6 col-md-offset-3">

<?php echo do_shortcode('[contact-form-7 id="5548" title="Digital - CTA Large"]');?>
</div>
</div>


<!--
<div class="proud-clients">

<div class="clients-blocks ">
	<div class="our-clients-feed-title">we are proud to work with</div>








<?php $loop = new WP_Query(array('post_type' => 'clients', 'posts_per_page' => -1));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class="col-xs-6 col-sm-6 col-md-3 col-lg-2 col-xl-2 client-blocks">
			<div class="client-block">

<?php the_post_thumbnail('featured');?>
			</div>
			<div class="team-details">
				<a href="<?php the_permalink()?>">
					<div class="overlay-cb">
			    <div class="text-bs">
			              <div class="one-agency-member-name"><?php echo get_the_title($ID);?></div>

								<div class="one-agency-member-role">
<?php the_excerpt();?></div>
								</div>
			    </div>
			       </div></a>








</article>


<?php endwhile;
wp_reset_query();
?>
</div>
</div>-->
<!-- carousel-->
<!--<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>-->

<link rel="stylesheet" href="/wp-content/themes/north/styles/owl.carousel.min.css" />
<script src="/wp-content/themes/north/include/owl.carousel.min.js"></script>

<div class="digital-testi">

<div class="testimonial-blocks ">
	<div class="owl-carousel owl-theme">
<!-- see single clients for code to remove currenlty viewed post from feed   -->
<?php $loop = new WP_Query(array('post_type' => 'testimonials', 'meta_query' => array(
			'relation'                                => 'AND',
			array(
				'key'     => 'team',
				'value'   => 'digital',
				'compare' => '=')), 'posts_per_page' => -1));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class=" testimonial-feed">

				<!--<a href="<?php the_permalink()?>"><div class="testimonial-title"><?php echo get_the_title($ID);?></div></a>-->


                   <div class="testimonial-title"> '<?php echo get_the_title($post_id);?>'</div>

                           <div class="testimonial-body"><?php the_field('body_quote');?></div>

    <div class="testimonial-person"><?php the_field('person_name');?></div>

       <div class="testimonial-position"><?php the_field('position');?></div>


       <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Review",
  "datePublished":"<?php the_field('date');?>",
  "reviewBody":"<?php the_field('body_quote');?>",
  "itemReviewed": {
    "@type": "Thing",
    "name": "<?php the_field('service_product_review');?>",
    "description":"<?php the_field('body_quote');?>"

  },
  "author": {
    "@type": "Person",
    "name": "<?php the_field('person_name');?>"
  },
  "reviewRating": {
    "@type": "Rating",
    "ratingValue": "<?php the_field('rating');?>",
    "bestRating": "100"

  },
  "publisher": {
    "@type": "Organization",
    "name": "One Agency"
  }
}
</script>





<div class="rating-area">

<div class="containerdiv">
    <div>
    <img src="https://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_blank-one.png" alt="img">
    </div>
    <div class="cornerimage" style="width:<?php the_field('rating');?>%;">
    <img src="https://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_full-one.png" alt="">
    </div>
<div>

</div>




</article>


<?php endwhile;
wp_reset_query();
?>
</div>
</div></div>
<script type="text/javascript">
	$(document).ready(function(){
  var owl = $('.owl-carousel');
owl.owlCarousel({
    items:3,
    loop:true,
     nav:true,
    margin:10,
     responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
      },
    autoplay:true,
    autoplayTimeout:9000,
    autoplayHoverPause:true
});
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[1000])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})
});


</script>
<!--questions area-->

<div class="col-md-12 digital-services-questions">
<?php if (have_rows('questions')):?>

<?php while (have_rows('questions')):the_row();?>

<div class="col-md-10 col-md-offset-1 digital-service-question-block">

<div class="digital question-question col-md-12"><?php the_sub_field('question_title');?></div>

<div class="digital-question-answer col-md-12"><?php the_sub_field('answer');?></div>

<!--<div class="question-date col-md-12"><?php the_sub_field('question_date');?></div>-->


</div>
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "<?php the_sub_field('question_title');?>",

    "dateCreated": "<?php the_sub_field('question_date');?>",
    "author": {
        "@type": "Person",
        "name": "One Agency"
    },
    "answerCount": "1",

    "suggestedAnswer": {
        "@type": "Answer",

        "text": "<?php echo sanitize_text_field(get_sub_field('answer'));?>",
        "dateCreated": "<?php the_sub_field('question_date');?>",
        "author": {
            "@type": "Person",
            "name": "One Agency"
        }
    }
}
</script>

<?php

endwhile;

 else :

// no rows found

endif;

?>
</div>
<script>
$(document).ready(function(){
  var colors = ["#03cea4","#e40066","#345995","#eac435","#f5464b", "#03464e"];
  var rand = Math.floor(Math.random()*colors.length);
  $('.digital-question-answer').css("border-color", colors[rand]);

});
</script>



<!--tech used area-->
<div class="technology-blocks col-md-12">
<!-- see single clients for code to remove currenlty viewed post from feed   -->  <!-- 'orderby' => 'rand',-->
<?php $loop = new WP_Query(array('post_type' => 'technology', 'posts_per_page' => -1));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class="col-xs-6 col-sm-6 col-md-3 col-lg-2 col-xl-1 technology-feed">

        <!--<a href="<?php the_permalink()?>"><div class="testimonial-title"><?php echo get_the_title($ID);?></div></a>-->




                           <div class="technology-logo"><?php the_post_thumbnail('featured');?></div>









</article>


<?php endwhile;
wp_reset_query();
?>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>

<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>

<?php get_footer('digital');?>



