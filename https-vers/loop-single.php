<?php
/**
* The Template for loop blog.
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
?>
<article id="post-<?php the_ID(); ?>"  <?php post_class('thmlvClassicPost'); ?>>
	<?php if(has_post_thumbnail()) { ?>
	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
		<?php the_post_thumbnail('featured'); ?>
	</a>	
	<?php } ?>
	<div class="container row">
		<?php
		echo north_switch_loop_title($post->ID, 1);
		echo north_entry_meta($post->ID);
		global $more;
		$more = 0;
		the_content();
		?>
	</div>
</article>