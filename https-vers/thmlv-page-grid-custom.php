<?php
/**
* Template Name: Blog Grid Custom
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
get_header();
?>
<div id="thmlvContent">
	<?php
	echo north_switch_header($post->ID);
	$temp = $wp_query; 
	$wp_query = null; 
	$wp_query = new WP_Query(); 
	$num = get_option('posts_per_page');
	$wp_query->query('showposts='.$num.'&paged='.$paged);
	if(have_posts()) {
		while (have_posts()) {
			the_post();
			//get_template_part('loop-grid-single', get_post_format());
			?><article id="post-<?php the_ID(); ?>"  <?php post_class('thmlvGridPost'); ?>>
            	
            	<div class="thmlvGridOverlay">
            		<div class="thmlvGridCaption">
            			<?php
            			//echo north_switch_loop_title($post->ID, 1);
            			//echo north_entry_meta($post->ID);
            			//the_excerpt();
            			?>
            		</div>
            	</div>
            </article><?php }
	}
	wp_reset_postdata();
	north_numeric_posts_nav();
	?>
</div>
<?php get_footer(); ?>