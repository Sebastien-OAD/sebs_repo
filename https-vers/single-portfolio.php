<?php
/**
 * The Template for single portfolio.
 *
 * @package WordPress
 * @subpackage North
 * @since North 1.0
 */
get_header();
get_template_part('media-switch');
get_template_part('media-logo');
?>
<div class="media-container">

<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;
?>

<div class="next-switch">
<div class="people-control">
<div class="people-prev"><?php previous_post_link();
?> |  <?php next_post_link();
?></div>
</div>
</div>
</div>

<!--<div id="thmlvContent">
<?php
echo north_switch_header($post->ID, 'skills');
while (have_posts()) {
	the_post();
	get_template_part('content-portfolio', get_post_format());
}
?>
</div>
</div>-->
<!--testi-->
<!--testimonials -->
<link rel="stylesheet" href="/wp-content/themes/north/styles/owl.carousel.min.css" />
<script src="/wp-content/themes/north/include/owl.carousel.min.js"></script>

<div class="media-testi">

<div class="testimonial-blocks ">
	<div class="owl-carousel owl-theme">
<!-- see single clients for code to remove currenlty viewed post from feed   -->
<?php $loop = new WP_Query(array('post_type' => 'testimonials', 'meta_query' => array(
			'relation'                                => 'AND',
			array(
				'key'     => 'team',
				'value'   => 'media',
				'compare' => '=')), 'posts_per_page' => -1));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class="testimonial-feed media">




                   <div class="testimonial-title media"> '<?php echo get_the_title($post_id);?>'</div>

                           <div class="testimonial-body media"><?php the_field('body_quote');?></div>

    <div class="testimonial-person media"><?php the_field('person_name');?></div>

       <div class="testimonial-position media"><?php the_field('position');?></div>


       <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Review",
  "datePublished":"<?php the_field('date');?>",
  "reviewBody":"<?php echo sanitize_text_field(the_field('body_quote'));?>",
  "itemReviewed": {
    "@type": "Thing",
    "name": "<?php the_field('service_product_review');?>",
    "description":"<?php echo sanitize_text_field(the_field('body_quote'));?>"

  },
  "author": {
    "@type": "Person",
    "name": "<?php the_field('person_name');?>"
  },
  "reviewRating": {
    "@type": "Rating",
    "ratingValue": "<?php the_field('rating');?>",
    "bestRating": "100"

  },
  "publisher": {
    "@type": "Organization",
    "name": "One Agency"
  }
}
</script>





<div class="rating-area">

<div class="containerdiv">
    <div>
    <img src="https://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_blank-one.png" alt="img">
    </div>
    <div class="cornerimage" style="width:<?php the_field('rating');?>%;">
    <img src="https://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_full-one.png" alt="">
    </div>
<div>

</div>




</article>


<?php endwhile;
wp_reset_query();
?>
</div>
</div></div>
<script type="text/javascript">
	$(document).ready(function(){
  var owl = $('.owl-carousel');
owl.owlCarousel({
    items:3,
    loop:true,
     nav:true,
    margin:10,
     responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
      },
    autoplay:true,
    autoplayTimeout:9000,
    autoplayHoverPause:true
});
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[1000])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})
});


</script>
<!--start of cta-->
<div class="media-main-cta col-md-12">
	<div class="media-cta-large col-md-6 col-md-offset-3">

<?php echo do_shortcode('[contact-form-7 id="4334" title="Lets Talk Home Page"]');?>
</div>
</div>
<!--end of cta-->

<div class="social-feeds">
<div class="container">
	<div class="social-box col-md-12">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s);
 js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="col-md-6">
<div class="fb-page" data-href="https://www.facebook.com/oneagencymedia" data-tabs="timeline" data-width="600" data-height="600" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/oneagencymedia" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/oneagencymedia">One Agency Media</a></blockquote></div>


    </div>
    <div class="col-md-6"><!--twitter collections by page-->


<a class="twitter-timeline" data-height="600" data-link-color="#E81C4F" href="https://twitter.com/OneAgencyMedia?ref_src=twsrc%5Etfw">Tweets by OneAgencyMedia</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>



<!--end of twitters by collection-->
</div>

<div class="social-share-set"><?php echo do_shortcode('[osd_social_media_sharing]');?></div>
</div>

</div>


</div>




<script type="text/javascript">
$(document).ready(function(){
  $(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>

<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>


<?php get_footer('');?>