<?php
/**
* The Template for single post.
*
* @package WordPress
* @subpackage North
* @since North 1.0
*/
get_header();
?>
<div id="thmlvContent">
	<?php
	echo north_switch_header($post->ID, 'skills');
	while (have_posts()) {
		the_post();
		get_template_part('content-single', get_post_format());
	}
	if(esc_attr(get_option('north_author_bio') == '1')) {
	?>
		<!-- Author bio -->
		<div id="thmlvAuthorBio">
			<?php echo get_avatar( get_the_author_meta('email'), '60' ); ?>
					<div class="thmlvAuthorInfo">
					<h3 class="thmlvAuthorTitle"><?php _e('Written by', 'themelovin') ?> <?php the_author_link(); ?></h3>
					<p class="thmlvAuthorDescription"><?php the_author_meta('description'); ?></p>
					<ul class="thmlvAuthorLinks">
						<?php 
							$rss_url = get_the_author_meta( 'rss_url' );
							if ( $rss_url && $rss_url != '' ) {
								echo '<li class="rss"><a href="' . esc_url($rss_url) . '">Rss</a></li>';
							}
			
							$google_profile = get_the_author_meta( 'google_profile' );
							if ( $google_profile && $google_profile != '' ) {
								echo '<li class="google"><a href="' . esc_url($google_profile) . '" rel="author">Google+</a></li>';
							}
			
							$twitter_profile = get_the_author_meta( 'twitter_profile' );
							if ( $twitter_profile && $twitter_profile != '' ) {
								echo '<li class="twitter"><a href="' . esc_url($twitter_profile) . '">Twitter</a></li>';
							}
			
							$facebook_profile = get_the_author_meta( 'facebook_profile' );
							if ( $facebook_profile && $facebook_profile != '' ) {
								echo '<li class="facebook"><a href="' . esc_url($facebook_profile) . '">Facebook</a></li>';
							}
				
							$linkedin_profile = get_the_author_meta( 'linkedin_profile' );
							if ( $linkedin_profile && $linkedin_profile != '' ) {
								echo '<li class="linkedin"><a href="' . esc_url($linkedin_profile) . '">Linkedin</a></li>';
							}
						?>
					</ul>
					<div class="thmlvClear"></div>
				</div>
		<!--END .author-bio-->
		</div>
		<?php
			}
			north_inner_nav();
		?>
	</div>
	<?php if(comments_open() || get_comments_number() != 0) { ?>
	<div id="thmlvCommentsWrap">
		<?php comments_template(); ?>
	</div>
	<?php } ?>
</div>
<?php
get_footer();
?>