<?php
/**
 * The Template for index.
 *
 * @package WordPress
 * @subpackage North
 * @since North 1.0
 */
get_header();
?>
<div id="thmlvContent">
<?php
echo north_switch_header($post->ID);
if (have_posts()) {
	while (have_posts()) {
		the_post();
		$format = get_post_format();
		get_template_part('loop-single', get_post_format());
	}
}
north_numeric_posts_nav();
?>
</div>

<?php get_footer();?>