<?php
/**
 * Template Name: media-home
 *

 */
get_header();
get_template_part('media-switch');
get_template_part('media-logo');

?>
<div id="digital-content-ss">

<?php if (have_posts()):while (have_posts()):the_post();?>
<div class="media-body-ss">
<?php the_content();?>
</div>
<?php endwhile;
endif;?>
</div>

<!--media services-->

<div class="col-md-12 media-services-feed">
<?php if (have_rows('services')):?>
<div class="media-fancy-text">What we offer</div>

<?php while (have_rows('services')):the_row();

?>
<?php $icon = get_sub_field('service_icon');?>
<div class="col-md-3 col-sm-4 col-lg-3  media-service-block-2">

<div class="media-services-icon"><a href="<?php the_sub_field('service_page_link');?>"><img src="<?php echo $icon['url'];?>" alt="<?php echo $icon['alt'];?>" /></a></div>

<div class="media-services-title"><a href="<?php the_sub_field('service_page_link');?>"><?php the_sub_field('service_title');
?></a></div>

<!--<div class="services-summary"><?php the_sub_field('service_body');
?></div>-->

<!--<div class="services-link"><a class="services-bitton-link" href="<?php the_sub_field('service_page_link');?>">Read More</a></div>-->

</div>
<script type='application/ld+json'>
{
	"@context": "http://schema.org/",
	"@type": "Service",
    "provider": {
    "@type": "LocalBusiness",
    "name": "One agency Digital",
    "telephone":"03304004169",
    "address":{"@type":"PostalAddress","streetAddress":"Unit 14, SchoolHouse, Third Avenue, Trafford Park","addressLocality":"Manchester","postalCode":"M171JE","addressCountry":"GB"},
    "image":"https://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/logo.png"
  },
	"areaServed": "United Kingdom",
	"serviceType": "service type",
	"alternateName": "<?php the_sub_field('service_title');?>",
	"description": "<?php the_sub_field('service_body');?>",
	"image": {
		"@type": "ImageObject",
		"contentUrl": "<?php echo $icon['url'];?>",
		"embedUrl": "<?php echo $icon['url'];?>"
	},
	"mainEntityOfPage": "<?php the_sub_field('service_page_link');?>",
	"name": "<?php the_sub_field('service_title');?>"
}
</script>

<?php

endwhile;

 else :

// no rows found

endif;

?>
</div>






<!-- end of media services -->
<div class="media-main-cta col-md-12">
	<div class="media-cta-large col-md-6 col-md-offset-3">

<?php echo do_shortcode('[contact-form-7 id="4334" title="Lets Talk Home Page"]');?>
</div>
</div>


<!--parallax block 1-->

<script src="/wp-content/themes/north/include/parallax.min.js"></script>


<?php if (get_field('parallax_background')) {?>
																																																										<div class="holder-2">

																																																										<div class="parallax-window" data-parallax="scroll" data-image-src="<?php the_field('parallax_background');?>"></div>
																																																										</div>

	<?php }?>
<!-- end of parallax block 1-->



<!--flexible content -->
<!--<div class="flexible-content-middle col-md-12">
<?php the_field('flexible_content_middle');?>
</div>-->
<!-- end of flexible content-->

<!--counters-->
<script src="/wp-content/themes/north/include/jquery.countupcircle.min.js"></script>

<div class="counters-area">
<div class="container">
<?php if (have_rows('counter')):?>
	<?php $a = 0;?>

<?php while (have_rows('counter')):the_row();?>

<div class="col-md-3">
	<div class="wrapper">
  <div id="count-box-<?php echo $a?>"><span class="timer"></span></div>
  <script type="text/javascript">
	jQuery(function($) {
		jQuery('#count-box-<?php echo $a?>').countTo({
			from: 0,
			to: <?php the_sub_field('counter_value');?>,
			speed: 10000,
			refreshInterval: 50,
			onComplete: function(value) {
				console.debug(this);
			}
		});
	});
</script>
<?php $a++?>
</div>

<div class="counter-text center">
<?php the_sub_field('counter_title');?></div>
</div>


<?php

endwhile;

 else :

// no rows found

endif;

?>
</div>
</div>

<!--end of counters-->

<div class="clients-blocks-media col-md-12">
	<div class="our-clients-feed-title-2">We are proud to work with</div>


<?php $currentID = get_the_ID();//removes current viewed post from the feed, using post no in var array
$loop            = new WP_Query(array('post_type' => 'media_clients', 'posts_per_page' => -1, 'post__not_in' => array($currentID)));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class="col-xs-12 col-sm-6 col-md-4 col-lg-2 col-xl-2 client-blocks">
			<div class="client-block">

<?php the_post_thumbnail('featured');?>
			</div>
			<div class="team-details">
				<a href="<?php the_permalink()?>">
					<div class="overlay-cb">
			    <div class="text-bs">
			              <div class="one-agency-member-name"><?php echo get_the_title($ID);?></div>

								<div class="one-agency-member-role"> <?php the_excerpt();?></div>
								</div>
			    </div>
			       </a>
			</div>

</article>


<?php endwhile;
wp_reset_query();
?>
</div><!--parallax block 2-->
<div class="empty"></div>

<div class="holder-2">


<?php if (get_field('parallax_background_2')) {?>

																																																								<div class="parallax-window-2" data-parallax="scroll" data-image-src="<?php the_field('parallax_background_2');?>">  <div class="para-text"><?php the_field('parallax_body_2');
	?></div> </div>

	<?php }?>
<!-- end of parallax block 2--></div>


<div class="media-main-cta col-md-12">
	<div class="media-cta-large col-md-6 col-md-offset-3">

<?php echo do_shortcode('[contact-form-7 id="4334" title="Lets Talk Home Page"]');?>
</div>
</div>
<!--<link rel="stylesheet" href="http://www.oneagencymedia.co.uk/wp-content/themes/north/styles/flexslider.css">
<script src="http://www.oneagencymedia.co.uk/wp-content/themes/north/include/jquery.flexslider.js"></script>-->


<?php get_footer('');?>
<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
  jQuery(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>


<script type="text/javascript">
jQuery(document).ready(function(){
jQuery('.parallax-window, .parallax-window-2').parallax({
	naturalWidth: 600,
	naturalHeight: 500
  });


//jQuery('.flexslider').flexslider({
 //   animation: "slide"
 // });


})

</script>
<script type="text/javascript">
(function($) {
	$.fn.countTo = function(options) {
		// merge the default plugin settings with the custom options
		options = $.extend({}, $.fn.countTo.defaults, options || {});

		// how many times to update the value, and how much to increment the value on each update
		var loops = Math.ceil(options.speed / options.refreshInterval),
			increment = (options.to - options.from) / loops;

		return $(this).each(function() {
			var _this = this,
				loopCount = 0,
				value = options.from,
				interval = setInterval(updateTimer, options.refreshInterval);

			function updateTimer() {
				value += increment;
				loopCount++;
				$(_this).html(value.toFixed(options.decimals));

				if (typeof(options.onUpdate) == 'function') {
					options.onUpdate.call(_this, value);
				}

				if (loopCount >= loops) {
					clearInterval(interval);
					value = options.to;

					if (typeof(options.onComplete) == 'function') {
						options.onComplete.call(_this, value);
					}
				}
			}
		});
	};

	$.fn.countTo.defaults = {
		from: 0,  // the number the element should start at
		to: 100,  // the number the element should end at
		speed: 1000,  // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,  // the number of decimal places to show
		onUpdate: null,  // callback method for every time the element is updated,
		onComplete: null,  // callback method for when the element finishes updating
	};
})(jQuery);</script>