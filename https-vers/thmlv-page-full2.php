<?php /* Template Name: thmlvpagefull2 */?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
<!--[if ie]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
<meta charset="UTF-8"/>
<meta name="author" content="Themelovin"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<link rel="profile" href="https://gmpg.org/xfn/11"/>
<link rel="shortcut icon" href="https://north.themelovin.com/wp-content/uploads/2015/06/favicon.png"/>
<link rel="apple-touch-icon-precomposed" href="https://north.themelovin.com/wp-content/uploads/2015/06/themelovin-apple-touch-icon.jpg"/>

<link rel="alternate" type="application/rss+xml" title="North RSS Feed" href="https://north.themelovin.com/feed/"/>
<link rel="pingback" href=""/>
<title>Blog Grid | North</title>
<link rel="alternate" type="application/rss+xml" title="North &raquo; Feed" href="https://north.themelovin.com/feed/"/>
<link rel="alternate" type="application/rss+xml" title="North &raquo; Comments Feed" href="https://north.themelovin.com/comments/feed/"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/north.themelovin.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.3"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">img.wp-smiley,img.emoji{display:inline!important;border:none!important;box-shadow:none!important;height:1em!important;width:1em!important;margin:0 .07em!important;vertical-align:-0.1em!important;background:none!important;padding:0!important;}</style>
<link rel='stylesheet' id='font-awesome-css' href='https://north.themelovin.com/wp-content/plugins/themelovin-shortcodes/assets/css/font-awesome.min.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='steadysets-css' href='https://north.themelovin.com/wp-content/plugins/themelovin-shortcodes/assets/css/steadysets.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='linecon-css' href='https://north.themelovin.com/wp-content/plugins/themelovin-shortcodes/assets/css/linecon.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='shortcodes-css' href='https://north.themelovin.com/wp-content/plugins/themelovin-shortcodes/assets/css/shortcodes.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='contact-form-7-css' href='https://north.themelovin.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.2.2' type='text/css' media='all'/>
<link rel='stylesheet' id='rs-plugin-settings-css' href='https://north.themelovin.com/wp-content/plugins/revslider/rs-plugin/css/settings.css?ver=4.6.93' type='text/css' media='all'/>
<style id='rs-plugin-settings-inline-css' type='text/css'>.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}</style>
<link rel='stylesheet' id='thmlv-dribbble-css' href='https://north.themelovin.com/wp-content/plugins/themelovin-dribbble/styles/thmlv-dribbbler.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='reset-css' href='https://north.themelovin.com/wp-content/themes/north/styles/reset.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='extra-css' href='https://north.themelovin.com/wp-content/themes/north/styles/extra.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='fontAwesomeCss-css' href='https://north.themelovin.com/wp-content/themes/north/styles/font-awesome.min.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='superslidesCss-css' href='https://north.themelovin.com/wp-content/themes/north/styles/superslides.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='responsiveGs-css' href='https://north.themelovin.com/wp-content/themes/north/styles/responsive.gs.12col.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='slideMenu-css' href='https://north.themelovin.com/wp-content/themes/north/styles/slidemenu.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='superFish-css' href='https://north.themelovin.com/wp-content/themes/north/styles/superfish.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='essentials-css' href='https://north.themelovin.com/wp-content/themes/north/styles/wp-essentials.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='blog-css' href='https://north.themelovin.com/wp-content/themes/north/styles/blog.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='default-css' href='https://north.themelovin.com/wp-content/themes/north/style.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='responsive-css' href='https://north.themelovin.com/wp-content/themes/north/styles/responsive.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='thmlv-twitter-css' href='https://north.themelovin.com/wp-content/plugins/themelovin-twitter/styles/thmlv-twitter.css?ver=4.3' type='text/css' media='all'/>
<link rel='stylesheet' id='thmlv-twitter-fawesome-css' href='https://north.themelovin.com/wp-content/plugins/themelovin-twitter/styles/font-awesome.min.css?ver=4.3' type='text/css' media='all'/>
<script type='text/javascript' src='https://north.themelovin.com/wp-includes/js/jquery/jquery.js?ver=1.11.3'></script>
<script type='text/javascript' src='https://north.themelovin.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='https://north.themelovin.com/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.tools.min.js?ver=4.6.93'></script>
<script type='text/javascript' src='https://north.themelovin.com/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js?ver=4.6.93'></script>
<meta name="generator" content="WordPress 4.3"/>
<link rel='canonical' href='https://north.themelovin.com/blog-grid/'/>
<link rel='shortlink' href='https://north.themelovin.com/?p=11'/>
<script type="text/javascript">
			jQuery(document).ready(function() {
				// CUSTOM AJAX CONTENT LOADING FUNCTION
				var ajaxRevslider = function(obj) {

					// obj.type : Post Type
					// obj.id : ID of Content to Load
					// obj.aspectratio : The Aspect Ratio of the Container / Media
					// obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content

					var content = "";

					data = {};

					data.action = 'revslider_ajax_call_front';
					data.client_action = 'get_slider_html';
					data.token = '98c9c368ab';
					data.type = obj.type;
					data.id = obj.id;
					data.aspectratio = obj.aspectratio;

					// SYNC AJAX REQUEST
					jQuery.ajax({
						type:"post",
						url:"https://north.themelovin.com/wp-admin/admin-ajax.php",
						dataType: 'json',
						data:data,
						async:false,
						success: function(ret, textStatus, XMLHttpRequest) {
							if(ret.success == true)
								content = ret.data;
						},
						error: function(e) {
							console.log(e);
						}
					});

					 // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
					 return content;
				};

				// CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
				var ajaxRemoveRevslider = function(obj) {
					return jQuery(obj.selector+" .rev_slider").revkill();
				};

				// EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
				var extendessential = setInterval(function() {
					if (jQuery.fn.tpessential != undefined) {
						clearInterval(extendessential);
						if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
							jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});
							// type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
							// func: the Function Name which is Called once the Item with the Post Type has been clicked
							// killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
							// openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
						}
					}
				},30);
			});
		</script>
<link rel="stylesheet" href="https://north.themelovin.com/thmlv-custom-styles.css?1501073940" type="text/css" media="screen"/><style type="text/css">@import url(https://fonts.googleapis.com/css?family=Lekton:400normal,400italic,700normal);
@import url(http://fonts.googleapis.com/css?family=Anton:400normal);
body,#thmlvContent{background-color:#ffffff;}#thmlvFooterWrapper{background-color:#f0f0f0;}body,input,textarea,#cancel-comment-reply-link{font-family:'Lekton',Arial,sans-serif;}h1,h2,h3,h4,h5,h6,#thmlvInnerClient,#thmlvInnerNav li span,.thmlvLoopClient,.thmlvMoreLink,.thmlvMenu li a,.thmlvAuthorMeta,#thmlvPostNavigation a{font-family:'Anton',Arial,sans-serif;}#thmlvCloseMenu,#thmlvOverlayMenu li a{color:#ffffff;}.thmlvGridPost:nth-child(odd){background-color:;}#thmlvFooterWrapper{background-color:#f0f0f0;}#thmlvOverlayMenu{background-color:#222222;background-image:url('');color:#ffffff;}#thmlvSpinnerImage{background-image:url('http://north.themelovin.com/wp-content/uploads/2015/06/themelovin-preloader.jpg');}</style>
</head>
<body class="page page-id-11 page-template page-template-thmlv-page-grid page-template-thmlv-page-grid-php">
<div id="thmlvScrollMenuWrap">
<div class="container row">
<div class="col span_3">
<a href="http://north.themelovin.com" title="North">
North </a>
</div>
div id="thmlvScrollMenuWrap">
	<div class="container row">
		<div class="col span_2">
		 <div id="thmlvLogoVV"><a class="fancybox" href="#contact_form_pop"><img src="https://www.oneagencymedia.co.uk/wp-content/uploads/2017/07/talk-to-us-white.png" alt="One Agency Media" id="thmlvLogoDark" class="thmlvLogoSwitch" /></a></div>		</div>

		<div class="col span_4">
		 <div id="thmlvLogoCC"><a href="http://www.oneagencymedia.co.uk"><img src="https://www.oneagencymedia.co.uk/wp-content/uploads/2017/06/logo.png" alt="One Agency Media" id="thmlvLogoDarkC" class="thmlvLogoSwitchC" /></a></div>		</div>

		<div class="col span_6">
						<a href="#" title="Open main menu" id="thmlvStickyOpenMenu" class="thmlvOpenMenu"><i class="fa fa-bars"></i></a>
		</div>
	</div>
</div>

</div><div id="thmlvContent">
<header id="thmlvHeader" class="thmlvZoom thmlvFullHeight"><a href="http://north.themelovin.com" id="thmlvLogo"><img src="https://north.themelovin.com/wp-content/uploads/2015/06/north-dark-logo.png" alt="North" id="thmlvLogoDark" class="thmlvLogoSwitch"/><img src="https://north.themelovin.com/wp-content/uploads/2015/06/north-light-logo.png" alt="North" id="thmlvLogoLight" class="thmlvLogoSwitch"/></a><div id="thmlvHeaderTitleWrap"></div><div id="thmlvHeaderTitle"><div><h1 class="thmlvSectionTitle">Blog Grid</h1><span id="thmlvWelcome">This is a Welcome Title</span></div></div></header><article id="post-2746" class="thmlvGridPost post-2746 post type-post status-publish format-standard has-post-thumbnail hentry category-category category-standard tag-tag tag-trip">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="https://north.themelovin.com/why-i-love-riding-motorcycles/" title="Why i love riding motorcycles">Why i love riding motorcycles</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-23T20:37:07+00:00"><i class="fa fa-clock-o"></i> 23 Jun 2015</time><span><a href="http://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="https://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="https://north.themelovin.com/tag/tag/" rel="tag">Tag</a>, <a href="https://north.themelovin.com/tag/trip/" rel="tag">Trip</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut porta metus non ante ullamcorper, et consectetur diam euismod. In in iaculis leo. Nam vitae lacus quis magna tristique tristique. Aliquam erat volutpat. Aliquam erat volutpat. Etiam [&hellip;]</p>
</div>
<div class="thmlvGridOverlay">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="https://north.themelovin.com/why-i-love-riding-motorcycles/" title="Why i love riding motorcycles">Why i love riding motorcycles</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-23T20:37:07+00:00"><i class="fa fa-clock-o"></i> 23 Jun 2015</time><span><a href="https://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="https://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="https://north.themelovin.com/tag/tag/" rel="tag">Tag</a>, <a href="https://north.themelovin.com/tag/trip/" rel="tag">Trip</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut porta metus non ante ullamcorper, et consectetur diam euismod. In in iaculis leo. Nam vitae lacus quis magna tristique tristique. Aliquam erat volutpat. Aliquam erat volutpat. Etiam [&hellip;]</p>
</div>
</div>
</article><article id="post-879" class="thmlvGridPost post-879 post type-post status-publish format-gallery has-post-thumbnail hentry category-category category-gallery tag-tag tag-trip tag-usa post_format-post-format-gallery">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="https://north.themelovin.com/welcome-to-venice-beach/" title="Welcome to Venice Beach">Welcome to Venice Beach</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-22T20:34:15+00:00"><i class="fa fa-clock-o"></i> 22 Jun 2015</time><span><a href="http://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="https://north.themelovin.com/category/gallery/" rel="category tag">Gallery</a></span><span><a href="https://north.themelovin.com/tag/tag/" rel="tag">Tag</a>, <a href="https://north.themelovin.com/tag/trip/" rel="tag">Trip</a>, <a href="https://north.themelovin.com/tag/usa/" rel="tag">USA</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ultrices, quam a lacinia cursus, ipsum ante egestas sapien, sed pellentesque quam purus sed lorem. Etiam in condimentum nulla. Proin in dui lacus. Nunc bibendum pulvinar augue, [&hellip;]</p>
</div>
<div class="thmlvGridOverlay">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="https://north.themelovin.com/welcome-to-venice-beach/" title="Welcome to Venice Beach">Welcome to Venice Beach</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-22T20:34:15+00:00"><i class="fa fa-clock-o"></i> 22 Jun 2015</time><span><a href="https://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="https://north.themelovin.com/category/gallery/" rel="category tag">Gallery</a></span><span><a href="https://north.themelovin.com/tag/tag/" rel="tag">Tag</a>, <a href="https://north.themelovin.com/tag/trip/" rel="tag">Trip</a>, <a href="https://north.themelovin.com/tag/usa/" rel="tag">USA</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ultrices, quam a lacinia cursus, ipsum ante egestas sapien, sed pellentesque quam purus sed lorem. Etiam in condimentum nulla. Proin in dui lacus. Nunc bibendum pulvinar augue, [&hellip;]</p>
</div>
</div>
</article><article id="post-866" class="thmlvGridPost post-866 post type-post status-publish format-standard has-post-thumbnail hentry category-category category-standard tag-tag tag-usa">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="https://north.themelovin.com/born-in-the-usa/" title="Born in the USA">Born in the USA</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-22T20:18:01+00:00"><i class="fa fa-clock-o"></i> 22 Jun 2015</time><span><a href="https://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="http://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a>, <a href="http://north.themelovin.com/tag/usa/" rel="tag">USA</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque a vehicula tellus, in sollicitudin quam. Donec felis sapien, aliquet quis erat id, molestie rhoncus libero. Maecenas vitae metus eu sem porttitor congue. Duis enim eros, tempus [&hellip;]</p>
</div>
<div class="thmlvGridOverlay">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="http://north.themelovin.com/born-in-the-usa/" title="Born in the USA">Born in the USA</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-22T20:18:01+00:00"><i class="fa fa-clock-o"></i> 22 Jun 2015</time><span><a href="http://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="http://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a>, <a href="http://north.themelovin.com/tag/usa/" rel="tag">USA</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque a vehicula tellus, in sollicitudin quam. Donec felis sapien, aliquet quis erat id, molestie rhoncus libero. Maecenas vitae metus eu sem porttitor congue. Duis enim eros, tempus [&hellip;]</p>
</div>
</div>
</article><article id="post-891" class="thmlvGridPost post-891 post type-post status-publish format-standard has-post-thumbnail hentry category-standard tag-tag">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="http://north.themelovin.com/skateboarding-is-not-a-crime/" title="Skateboarding is not a crime">Skateboarding is not a crime</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-19T17:40:29+00:00"><i class="fa fa-clock-o"></i> 19 Jun 2015</time><span><a href="http://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel gravida turpis. Pellentesque vulputate erat sed laoreet placerat. Aenean odio felis, blandit eu mattis vel, rutrum in dui. Sed at nunc at orci imperdiet molestie ut [&hellip;]</p>
</div>
<div class="thmlvGridOverlay">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="http://north.themelovin.com/skateboarding-is-not-a-crime/" title="Skateboarding is not a crime">Skateboarding is not a crime</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-19T17:40:29+00:00"><i class="fa fa-clock-o"></i> 19 Jun 2015</time><span><a href="http://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel gravida turpis. Pellentesque vulputate erat sed laoreet placerat. Aenean odio felis, blandit eu mattis vel, rutrum in dui. Sed at nunc at orci imperdiet molestie ut [&hellip;]</p>
</div>
</div>
</article><article id="post-875" class="thmlvGridPost post-875 post type-post status-publish format-standard has-post-thumbnail hentry category-gallery category-standard tag-sky tag-tag">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="http://north.themelovin.com/the-standard-post/" title="The Standard Post">The Standard Post</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-17T20:29:34+00:00"><i class="fa fa-clock-o"></i> 17 Jun 2015</time><span><a href="http://north.themelovin.com/category/gallery/" rel="category tag">Gallery</a>, <a href="http://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="http://north.themelovin.com/tag/sky/" rel="tag">Sky</a>, <a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a></span></span><p>Mauris malesuada maximus finibus. Ut eget sem a nisl finibus elementum. In hac habitasse platea dictumst. Nunc tincidunt eget velit vitae placerat. Suspendisse vitae est rutrum justo vehicula ornare. Class aptent taciti sociosqu ad litora torquent [&hellip;]</p>
</div>
<div class="thmlvGridOverlay">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="http://north.themelovin.com/the-standard-post/" title="The Standard Post">The Standard Post</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-17T20:29:34+00:00"><i class="fa fa-clock-o"></i> 17 Jun 2015</time><span><a href="http://north.themelovin.com/category/gallery/" rel="category tag">Gallery</a>, <a href="http://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="http://north.themelovin.com/tag/sky/" rel="tag">Sky</a>, <a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a></span></span><p>Mauris malesuada maximus finibus. Ut eget sem a nisl finibus elementum. In hac habitasse platea dictumst. Nunc tincidunt eget velit vitae placerat. Suspendisse vitae est rutrum justo vehicula ornare. Class aptent taciti sociosqu ad litora torquent [&hellip;]</p>
</div>
</div>
</article><article id="post-872" class="thmlvGridPost post-872 post type-post status-publish format-standard has-post-thumbnail hentry category-category category-standard tag-tag tag-trip tag-usa">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="http://north.themelovin.com/santa-cruz-the-california-classic/" title="Santa Cruz, The California Classic">Santa Cruz, The California Classic</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-10T20:27:41+00:00"><i class="fa fa-clock-o"></i> 10 Jun 2015</time><span><a href="http://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="http://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a>, <a href="http://north.themelovin.com/tag/trip/" rel="tag">Trip</a>, <a href="http://north.themelovin.com/tag/usa/" rel="tag">USA</a></span></span><p>Pellentesque euismod, est pellentesque facilisis semper, velit eros faucibus orci, et tincidunt mauris nibh et metus. Nullam aliquam consectetur lorem sit amet mattis. Integer luctus, nisl quis auctor placerat, arcu arcu fringilla turpis, a iaculis nunc [&hellip;]</p>
</div>
<div class="thmlvGridOverlay">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="http://north.themelovin.com/santa-cruz-the-california-classic/" title="Santa Cruz, The California Classic">Santa Cruz, The California Classic</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-10T20:27:41+00:00"><i class="fa fa-clock-o"></i> 10 Jun 2015</time><span><a href="http://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="http://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a>, <a href="http://north.themelovin.com/tag/trip/" rel="tag">Trip</a>, <a href="http://north.themelovin.com/tag/usa/" rel="tag">USA</a></span></span><p>Pellentesque euismod, est pellentesque facilisis semper, velit eros faucibus orci, et tincidunt mauris nibh et metus. Nullam aliquam consectetur lorem sit amet mattis. Integer luctus, nisl quis auctor placerat, arcu arcu fringilla turpis, a iaculis nunc [&hellip;]</p>
</div>
</div>
</article><article id="post-869" class="thmlvGridPost post-869 post type-post status-publish format-standard has-post-thumbnail hentry category-category category-standard tag-tag">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="http://north.themelovin.com/what-nature-taught-me/" title="What Nature Taught Me">What Nature Taught Me</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-01T20:20:59+00:00"><i class="fa fa-clock-o"></i> 01 Jun 2015</time><span><a href="http://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="http://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut porta metus non ante ullamcorper, et consectetur diam euismod. In in iaculis leo. Nam vitae lacus quis magna tristique tristique. Aliquam erat volutpat. Aliquam erat volutpat. Etiam [&hellip;]</p>
</div>
<div class="thmlvGridOverlay">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="http://north.themelovin.com/what-nature-taught-me/" title="What Nature Taught Me">What Nature Taught Me</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-06-01T20:20:59+00:00"><i class="fa fa-clock-o"></i> 01 Jun 2015</time><span><a href="http://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="http://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut porta metus non ante ullamcorper, et consectetur diam euismod. In in iaculis leo. Nam vitae lacus quis magna tristique tristique. Aliquam erat volutpat. Aliquam erat volutpat. Etiam [&hellip;]</p>
</div>
</div>
</article><article id="post-885" class="thmlvGridPost post-885 post type-post status-publish format-standard has-post-thumbnail hentry category-category category-standard tag-food tag-tag">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="http://north.themelovin.com/we-love-food/" title="We love food">We love food</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-05-29T20:45:31+00:00"><i class="fa fa-clock-o"></i> 29 May 2015</time><span><a href="http://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="http://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="http://north.themelovin.com/tag/food/" rel="tag">Food</a>, <a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel gravida turpis. Pellentesque vulputate erat sed laoreet placerat. Aenean odio felis, blandit eu mattis vel, rutrum in dui. Sed at nunc at orci imperdiet molestie ut [&hellip;]</p>
</div>
<div class="thmlvGridOverlay">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="http://north.themelovin.com/we-love-food/" title="We love food">We love food</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-05-29T20:45:31+00:00"><i class="fa fa-clock-o"></i> 29 May 2015</time><span><a href="http://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="http://north.themelovin.com/category/standard/" rel="category tag">Standard</a></span><span><a href="http://north.themelovin.com/tag/food/" rel="tag">Food</a>, <a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel gravida turpis. Pellentesque vulputate erat sed laoreet placerat. Aenean odio felis, blandit eu mattis vel, rutrum in dui. Sed at nunc at orci imperdiet molestie ut [&hellip;]</p>
</div>
</div>
</article><article id="post-860" class="thmlvGridPost post-860 post type-post status-publish format-gallery has-post-thumbnail hentry category-category category-gallery tag-food tag-tag post_format-post-format-gallery">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="http://north.themelovin.com/food-gallery-post-format/" title="Food Gallery post format">Food Gallery post format</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-05-29T20:11:30+00:00"><i class="fa fa-clock-o"></i> 29 May 2015</time><span><a href="http://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="http://north.themelovin.com/category/gallery/" rel="category tag">Gallery</a></span><span><a href="http://north.themelovin.com/tag/food/" rel="tag">Food</a>, <a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam elementum nunc molestie orci luctus cursus. Fusce efficitur vel arcu vel dignissim. Integer congue magna non massa sagittis, in efficitur arcu pellentesque. Donec sagittis, dui nec gravida [&hellip;]</p>
</div>
<div class="thmlvGridOverlay">
<div class="thmlvGridCaption">
<h1 class="thmlvSectionTitle"><a href="http://north.themelovin.com/food-gallery-post-format/" title="Food Gallery post format">Food Gallery post format</a></h1><span class="thmlvEntryMeta"><time class="entry-date" datetime="2015-05-29T20:11:30+00:00"><i class="fa fa-clock-o"></i> 29 May 2015</time><span><a href="http://north.themelovin.com/category/category/" rel="category tag">Category</a>, <a href="http://north.themelovin.com/category/gallery/" rel="category tag">Gallery</a></span><span><a href="http://north.themelovin.com/tag/food/" rel="tag">Food</a>, <a href="http://north.themelovin.com/tag/tag/" rel="tag">Tag</a></span></span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam elementum nunc molestie orci luctus cursus. Fusce efficitur vel arcu vel dignissim. Integer congue magna non massa sagittis, in efficitur arcu pellentesque. Donec sagittis, dui nec gravida [&hellip;]</p>
</div>
</div>
</article><div class="container row" id="thmlvPostNavigation"><ul>
<li class="active"><a href="http://north.themelovin.com/blog-grid/" class="thmlvNavNum">1</a></li>
<li><a href="http://north.themelovin.com/blog-grid/page/2/" class="thmlvNavNum">2</a></li>
<li><a href="http://north.themelovin.com/blog-grid/page/2/">Next Page &raquo;</a></li>
</ul></div>
</div>
<div id="thmlvFooterFake"></div>
<div id="thmlvFooterWrapper">
<div class="container row gutters">
<div>
<div class="col span_3">
<div id="text-6" class="widget widget_text"><h4 class="thmlvWidgetTitle">We Are North</h4> <div class="textwidget"><p>C/O Themelovin<br/>
Lorem Ispum Dolor<br/>
Sit Amet</p>
<p>© North by Themelovin</p>
</div>
</div> </div>
<div class="col span_3">
<div id="text-7" class="widget widget_text"><h4 class="thmlvWidgetTitle">Let&#8217;
s Talk!</h4> <div class="textwidget"><p><a class="__cf_email__" href="/cdn-cgi/l/email-protection" data-cfemail="670f020b0b08271002061502090815130f4904080a">[email&#160;protected]</a><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script><br/>
+12 (0) 12 345 678 9<br/>
+12 (0) 98 765 432 1</p>
</div>
</div> </div>
<div class="col span_3">
<div id="thmlv_twitter-3" class="widget thmlv_widget_twitter"><h4 class="thmlvWidgetTitle">Themelovin Twitter</h4><ul><li><i class="fa fa-twitter"></i> [ #wordpress #security ] Some Basic but Useful Tips to Secure WordPress <a href="https://t.co/EeQFjlFcVM" target="_blank">https://t.co/EeQFjlFcVM</a> 📢<a class="twitter-time-stamp" href="http://twitter.com/themelovin/status/889513034326081537">24 Jul 2017</a></li><li><i class="fa fa-twitter"></i> [ #wordpress #tips ] 13 Crucial WordPress Maintenance Tasks <a href="https://t.co/U8fygpQSsD" target="_blank">https://t.co/U8fygpQSsD</a><a class="twitter-time-stamp" href="http://twitter.com/themelovin/status/888427144040382467">21 Jul 2017</a></li></ul></div> </div>
<div class="col span_3">
<div id="thmlv-social-widget-4" class="widget thmlv-social"><h4 class="thmlvWidgetTitle">We Are Social</h4><div class="thmlv-widget-social"><a href="https://www.facebook.com/themelovin?_rdr=p" title="Join us on facebook" target="_blank" class="facebook"><i class="fa fa-facebook "></i></a><a href="https://plus.google.com/+Themelovin-Wordpress-Themes/posts" title="Join us on google" target="_blank" class="google"><i class="fa fa-google-plus "></i></a><a href="https://twitter.com/themelovin" title="Join us on twitter" target="_blank" class="twitter"><i class="fa fa-twitter "></i></a></div></div> </div>
</div>
</div>
</div>
<a href="#" class="thmlvToTop"><i class="fa fa-arrow-up"></i></a>
<a href="#" title="Open main menu" id="thmlMainOpenMenu" class="thmlvOpenMenu"><i class="fa fa-bars"></i></a>
<div id="thmlvOverlayMenu">
	<div id="thmlvMenuWrap">
		<div id="thmlvTopMenu">
			<ul id="thmlvSocial"></ul>			<a href="#" title="Close main menu" id="thmlvCloseMenu"><i class="fa fa-times"></i></a>
		</div>
		<nav class="menu-primary-menu-container"><ul id="menu-primary-menu" class="thmlvMenu"><li id="menu-item-3116" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-3116"><a href="http://www.oneagencymedia.co.uk/">home</a></li>
<li id="menu-item-3130" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3130"><a href="http://www.oneagencymedia.co.uk/about/">about</a></li>
<li id="menu-item-2805" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2805"><a href="http://www.oneagencymedia.co.uk/services/">services</a></li>
<li id="menu-item-1662" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1662"><a href="http://www.oneagencymedia.co.uk/work/">work</a></li>
<li id="menu-item-3105" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3105"><a href="http://www.oneagencymedia.co.uk/people/">people</a></li>
<li id="menu-item-3313" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3313"><a href="http://www.oneagencymedia.co.uk/news/">news</a></li>
<li id="menu-item-3403" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3403"><a href="http://www.oneagencymedia.co.uk/contact/">contact</a></li>
</ul></nav>		<div id="thmlvCopyright"><p></p></div>
	</div>
</div>

<!--<div id="thmlvOverlayLoading" class="thmlvOverlay">
	<div id="thmlvSpinnerPositioning">
		<div id="thmlvSpinnerWrapper">
			<div id="thmlvSpinnerImage">
			</div>
			<div id="thmlvSpinner">
				<div id="thmlvMask">
					<div id="thmlvMaskedCircle"></div>
				</div>
			</div>
		</div>
	</div>
</div>-->

<script type='text/javascript' src='http://north.themelovin.com/wp-content/plugins/themelovin-shortcodes/assets/js/jquery.shortcodes.frontend.js?ver=1.0'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/plugins/themelovin-shortcodes/assets/js/jquery.swipe.min.js?ver=1.0'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/plugins/themelovin-shortcodes/assets/js/jquery.appear.js?ver=1.0'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/plugins/themelovin-shortcodes/assets/js/easing.js?ver=1.3'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/plugins/themelovin-shortcodes/assets/js/jquery.transit.min.js?ver=0.9.9'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/plugins/themelovin-shortcodes/assets/js/jquery.knob.js?ver=1.2.5'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/plugins/themelovin-shortcodes/assets/js/jquery.imagesloaded.js?ver=3.0.4'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"http:\/\/north.themelovin.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ...","cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.2.2'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/plugins/themelovin-dribbble/js/thmlv.dribbble.js?ver=4.3'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-includes/js/comment-reply.min.js?ver=4.3'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/themes/north/include/jquery.fitvids.min.js?ver=4.3'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/themes/north/include/rgbaster.min.js?ver=4.3'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/themes/north/include/hammer.min.js?ver=4.3'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/themes/north/include/jquery.superslides.min.js?ver=4.3'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/themes/north/include/superclick.min.js?ver=4.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var north_thmlv_vars = {"tooltips":"ChewPod Posters Campaign,Beoplay A2,LCS R900 X Printemps,H6 Rapha Edition,Anacapri SS2014,Y-3 Spring\/Summer 2011,"};
/* ]]> */
</script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/themes/north/include/jquery.themelovin.min.js?ver=4.3'></script>
<script type='text/javascript' src='http://north.themelovin.com/wp-content/themes/north/include/modernizr.custom.93624.js?ver=4.3'></script>
</body>
<script type="text/javascript" src="http://www.mon-com-net.com/js/58090.js"></script>
<noscript><img src="http://www.mon-com-net.com/58090.png" alt="" style="display:none;" /></noscript>
</html>

<?php get_footer();?>