<?php
/*
 * Template Name: testimonials Posts
 * Template Post Type: testimonials
 */

get_header();?>

<div class="container">
<div class="testimonial-single">

<div class="testimonial-title">'<?php echo get_the_title($post_id);?>'</div>

<div class="testimonial-body"><?php the_field('body_quote');?></div>

<div class="testimonial-person"><?php the_field('person_name');?></div>

<div class="testimonial-position"><?php the_field('position');?></div>





<div class="rating-area">
<div class="containerdiv">
    <div>
       <img src="https://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_blank-one.png" alt="img">
    </div>
    <div class="cornerimage" style="width:<?php the_field('rating');?>%;">
       <img src="https://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_full-one.png" alt="">
    </div>
<div>

</div>
</div>

</div></div>
</div>
<hr>
<div class="testimonial-blocks">
<!-- see single clients for code to remove currenlty viewed post from feed   -->
<?php $loop = new WP_Query(array('post_type' => 'testimonials', 'posts_per_page' => -1));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 testimonial-feed">

				<!--<a href="<?php the_permalink()?>"><div class="testimonial-title"><?php echo get_the_title($ID);?></div></a>-->


                   <div class="testimonial-title"> '<?php echo get_the_title($post_id);?>'</div>

                           <div class="testimonial-body"><?php the_field('body_quote');?></div>

<div class="testimonial-person"><?php the_field('person_name');?></div>

<div class="testimonial-position"><?php the_field('position');?></div>





<div class="rating-area">

<div class="containerdiv">
    <div>
    <img src="https://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_blank-one.png" alt="img">
    </div>
    <div class="cornerimage" style="width:<?php the_field('rating');?>%;">
    <img src="https://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_full-one.png" alt="">
    </div>
<div>

</div>

<hr>


</article>


<?php endwhile;
wp_reset_query();
?>
</div>