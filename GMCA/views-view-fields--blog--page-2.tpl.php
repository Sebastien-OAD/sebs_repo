<article class="featured-blog">

<div class="featured-blog-inner col-md-12">
	<div class="featured-blog-body col-md-6">
    	<div class="featured-blog-title"><?php print $fields['title']->content; ?></div>
        <div class="featured-blod-snippet"><?php print $fields['field_feature_story_snippet']->content; ?></div>
        <div class="featured-blog-read-more ">
    	 <?php print $fields['title_1']->content; ?>
    
    </div>
    
    </div>
	<div class="featured-blog-image col-md-6">
    	 <?php print $fields['field_feature_image']->content; ?>
    
    </div>



</div>


</article>