<?php

/**
 * @file
 * Customize confirmation screen after successful submission.
 *
 * This file may be renamed "webform-confirmation-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-confirmation.tpl.php" to affect all webform confirmations on your
 * site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $progressbar: The progress bar 100% filled (if configured). This may not
 *   print out anything if a progress bar is not enabled for this node.
 * - $confirmation_message: The confirmation message input by the webform
 *   author.
 * - $sid: The unique submission ID of this submission.
 * - $url: The URL of the form (or for in-block confirmations, the same page).
 */
 include_once(drupal_get_path('module', 'webform') 
.'/includes/webform.submissions.inc');

 $nid = $_GET['nid'];
$sid = $_GET['sid'];

$submission = webform_get_submission($nid, $sid);

// See potential content
//echo "<pre>";
//print_r($submission);
//echo"</pre>";

$daily_drinks = $submission->data[42][0];
$daily_calories = $submission->data[21][0];
$daily_units = $submission->data[9][0];
$weekly_drinks = $submission->data[80][0];
$days_you_drink = $submission->data[77][0];
$weekly_units = $submission->data[78][0];
$weekly_calories = $submission->data[79][0];

//weekly units message
$units_warning = "";
$wu = $weekly_units;
//echo $wu;
switch($wu){
	case ($wu > 0 && $wu < 10);
	$units_warning = "You are at Low Risk, consuming ". $wu . " units per week is below the reccommended 14 units per week average ";
	break;
	case ($wu >= 10 && $wu <= 20);
	$units_warning = "You are at Moderate Risk, consuming ". $wu . " units per week is within the reccommended 14 units per week average ";
	break;
	case ($wu >= 21 && $wu <= 30);
	$units_warning = "You are at High Risk, consuming ". $wu .  " units per week is above the reccommended 14 units per week average";
	break;
	case ($wu >= 31 && $wu <= 100);
	$units_warning = "You are at a Dangerously High Risk, consuming more than ". $wu . " units per week is way above the reccommended 14 units per week average";
	break;
	}

//weekly calories message
$calories_warning = "";
$dc = $daily_calories;
$burgers = round($daily_calories / 300);

switch($dc){
	case ($dc > 0 && $dc < 1000);
	$calories_warning = "You will need to run for over 1 hour to burn the extra " .$dc ." calories added daily by your drinking, the calories are equilivant to upto " .  $burgers . " Double Cheese Burgers per day";
	break;
	case ($dc >= 1000 && $dc <= 2000);
	$calories_warning = "You will need to run for over 2 hours to burn the extra " .$dc . " calories added daily by your drinking, which is like running a marathon or the calories are equilivant to upto " .  $burgers . " Double Cheese Burgers per day";
	break;
	case ($dc >= 2001 && $dc <= 3000);
	$calories_warning = "You will need to run for over 3 hours to burn the extra " .$dc ." calories added daily by your drinking, which is more than running a marathon, the calories are equilivant to upto " .  $burgers . " Double Cheese Burgers per day";
	break;
	case ($dc >= 3001 && $dc <= 4000);
	$calories_warning = "You will need to run for over 4 hours to burn the extra " .$dc ." calories added daily by your drinking, which is more than running a marathon, the calories are equilivant to upto " .  $burgers . " Double Cheese Burgers per day";
	break;
	case ($dc >= 4001 && $dc > 5000);
	$calories_warning = "You will need to run for over 6 hours to burn the extra " .$dc ." calories added daily by your drinking, which is more than running a marathon and a half , the calories are equilivant to more " .  $burgers . " Double Cheese Burgers per day";
	break;
	
	}	
	
//type of drinker
$dinker_type_warning = "";	
$dyd = $days_you_drink;
$dd = $daily_drinks;

    //drinks at the weekend and more than 10 drinks
    if (($dyd <= 2) && ($dd >= 10)){
	$dinker_type_warning =" Binge drinker - you consuming all your units within " . $dd . " days, this can have a serious effect on your health. ";}
	//drinks more than 2 days and more than 10 drinks a day
	if (($dyd >= 2) && ($dd >= 10)){
	$dinker_type_warning =" High drinker, consuming " . $dd .  " drinks a day, spread over " . $dyd . " days";}
	// drinks more than 2 days but less than 10 drinks a day
	if (($dyd > 2 && $dyd < 7) && ($dd < 10)){
	$dinker_type_warning =" Moderate drinker, consuming " . $dd .  " drinks a day, spread over " . $dyd . " days";}
	// drinks everyday and has more than 1 drink
	if (($dyd == 7) && ($dd > 1)){
	$dinker_type_warning =" Daily drinker, consuming " . $dd . " drinks a day, " . $dyd . " days a week, consuming so much alcohol has a majoer effect on your health. ";}

?>

<div class="hdym-results col-md-12">
<div class="hdym-title">Heres how you measure.</div>

    <div class="hdym-results-inner">
        <div class="hdym-left col-md-6">
             <div class="daily-drinks"><div class="hdym-lable">Daily Drinks </div> <?php print $daily_drinks; ?></div>
             <div class="daily-calories"><div class="hdym-lable">Daily Calories </div><?php print $daily_calories; ?></div>
             <div class="daily-units"><div class="hdym-lable">Daily Units </div><?php print $daily_units; ?></div>
        </div>
        <div class="hdym-right col-md-6">
        	<div class="weekly-drinks"><div class="hdym-lable">Weekly Drinks </div><?php print $weekly_drinks; ?></div>
             <div class="weekly-calories"><div class="hdym-lable">Weekly Calories </div><?php print $weekly_calories; ?></div>
             <div class="weekly-units"><div class="hdym-lable">Weekly Units </div><?php print $weekly_units; ?></div>
        
        </div>
        <div class="days-drinking col-md-12"><div class="hdym-lable">Days Drinking </div><?php print $days_you_drink; ?></div>
        </div>
    
    
    		<div class="advice-area col-md-12 warning-area">
                <div class="calories-warning col-md-6 warning-area"><?php print $calories_warning; ?></div>
                <div class="units-warning col-md-6 warning-area"><?php print $units_warning; ?></div>
                <div class="drinker-type-warning col-md-6 warning-area"><?php print $dinker_type_warning ?></div>
            
            
            </div>
     </div>
     

</div>


<div class="hdym-bottom col-md-12">
<?php print $progressbar; ?>


<div class="webform-confirmation">
  <?php if ($confirmation_message): ?>
    <?php print $confirmation_message ?>
  <?php else: ?>
    <p><?php print t('Thank you, your submission has been received.'); ?></p>
  <?php endif; ?>
</div>

<div class="links">
  <a href="<?php print $url; ?>"><?php print t('Go back to the form') ?></a>
</div>
</div>