// Adds and removes body class depending on screen width.
function screenClass() {
    if($(window).innerWidth() > 960) {
        $('.news-feed').addClass('big-screen').removeClass('small-screen');
        

    } else {
        $('.news-feed').addClass('small-screen').removeClass('big-screen');

    }
}

// Fire.
screenClass();

// And recheck when window gets resized.
$(window).bind('resize',function(){
    screenClass();
});

$( document ).ready(function() {
	screenClass();
    console.log( "ready!" );
});
