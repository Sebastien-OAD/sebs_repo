
<?php
/**
 * Template Name: media-about
 *
 */
get_header('');

?>
<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
<div id="digital-content-ss">

<?php if (have_posts()):while (have_posts()):the_post();?>
<?php the_content();?>
<?php endwhile;
endif;?>
</div>
<!--media casestudies-->


<?php

include_once (ABSPATH.'wp-admin/includes/plugin.php');
if (is_plugin_active('themelovin-portfolio/thmlv-portfolios.php')) {
	?>
	<div class="about-case col-md-12">
				    <div class="case-studies-about-title">Recent case studies</div>
	<?php

	$args = array(
		'nopaging'   => true,
		'post_type'  => 'portfolio',
		'skills'     => 'design-skill',
		'meta_key'   => 'show_in_about_feed',
		'meta_value' => true,
		'orderby'    => array('menu_order'    => 'ASC', 'ID'    => 'ASC')
	);

	$wp_query = new WP_Query($args);
	while ($wp_query->have_posts()):$wp_query->the_post();?>
								<div class="col-md-4">

									<div class="case-image"><?php the_post_thumbnail('large');?></div>
											          <div class="case-title">  <?php the_title();?></div>
											          <div class="case-summary"><?php the_field('summary');?></div>

								</div>
	<?php
	endwhile;
	wp_reset_postdata();
	?>
	</div>
	<?php
}
?>
<!--testi-->
<!--testimonials -->
<link rel="stylesheet" href="/wp-content/themes/north/styles/owl.carousel.min.css" />
<script src="/wp-content/themes/north/include/owl.carousel.min.js"></script>

<div class="media-testi">

<div class="testimonial-blocks ">
	<div class="owl-carousel owl-theme">
<!-- see single clients for code to remove currenlty viewed post from feed   -->
<?php $loop = new WP_Query(array('post_type' => 'testimonials', 'meta_query' => array(
			'relation'                                => 'AND',
			array(
				'key'     => 'team',
				'value'   => 'media',
				'compare' => '=')), 'posts_per_page' => -1));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<article class="testimonial-feed media">




                   <div class="testimonial-title media"> '<?php echo get_the_title($post_id);?>'</div>

                           <div class="testimonial-body media"><?php the_field('body_quote');?></div>

    <div class="testimonial-person media"><?php the_field('person_name');?></div>

       <div class="testimonial-position media"><?php the_field('position');?></div>


       <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Review",
  "datePublished":"<?php the_field('date');?>",
  "reviewBody":"<?php echo sanitize_text_field(the_field('body_quote'));?>",
  "itemReviewed": {
    "@type": "Thing",
    "name": "<?php the_field('service_product_review');?>",
    "description":"<?php echo sanitize_text_field(the_field('body_quote'));?>"

  },
  "author": {
    "@type": "Person",
    "name": "<?php the_field('person_name');?>"
  },
  "reviewRating": {
    "@type": "Rating",
    "ratingValue": "<?php the_field('rating');?>",
    "bestRating": "100"

  },
  "publisher": {
    "@type": "Organization",
    "name": "One Agency"
  }
}
</script>





<div class="rating-area">

<div class="containerdiv">
    <div>
    <img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_blank-one.png" alt="img">
    </div>
    <div class="cornerimage" style="width:<?php the_field('rating');?>%;">
    <img src="http://www.oneagencymedia.co.uk/wp-content/uploads/2017/12/stars_full-one.png" alt="">
    </div>
<div>

</div>




</article>


<?php endwhile;
wp_reset_query();
?>
</div>
</div></div>
<script type="text/javascript">
	$(document).ready(function(){
  var owl = $('.owl-carousel');
owl.owlCarousel({
    items:3,
    loop:true,
     nav:true,
    margin:10,
     responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
      },
    autoplay:true,
    autoplayTimeout:9000,
    autoplayHoverPause:true
});
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[1000])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})
});


</script>






<script type="text/javascript">
$(document).ready(function(){
  $(".breadcrumbs").detach().appendTo('.breadcrumb-holder')
});
</script>

<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
<?php if (function_exists('bcn_display')) {
	bcn_display();
}?>
</div>
<!--start of cta-->
<div class="media-main-cta col-md-12">
	<div class="media-cta-large col-md-6 col-md-offset-3">

<?php echo do_shortcode('[contact-form-7 id="4334" title="Lets Talk Home Page"]');?>
</div>
</div>
<!--end of cta-->

<?php get_footer('');?>