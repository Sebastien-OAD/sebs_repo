<article class="testimonials">




<div class="testimonial-title">"<?php print $fields['title']->content;?>"</div>
<div class="testimonial-body"><?php print $fields['body']->content;?></div>
<div class="testimonial-bottom">

<div class="testimonial-by-who"><?php print $fields['field_persons_name']->content;?></div>
<div class="testimonial-service-reviewed"><?php print $fields['field_service_being_reviewed']->content;?></div>
<div class="testimonial-date"><?php print $fields['field_date_of_review']->content;?></div>
</div>
</article>