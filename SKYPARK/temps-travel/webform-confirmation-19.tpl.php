<?php

/**
 * @file
 * Customize confirmation screen after successful submission.
 *
 * This file may be renamed "webform-confirmation-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-confirmation.tpl.php" to affect all webform confirmations on your
 * site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $progressbar: The progress bar 100% filled (if configured). This may not
 *   print out anything if a progress bar is not enabled for this node.
 * - $confirmation_message: The confirmation message input by the webform
 *   author.
 * - $sid: The unique submission ID of this submission.
 * - $url: The URL of the form (or for in-block confirmations, the same page).
 */
?>

<?php // Print the webform submission to the submitter
  include_once drupal_get_path('module','webform') . '/includes/webform.submissions.inc';
  $submission = webform_get_submission($node->nid, $sid);


  
  print "<pre>";
  print_r($submission);
  print "</pre>";
  
  
  ?>
<div class="confirmation-message">
    <?php print $confirmation_message ?>
  </div>

  <ul>
    <li><a href="<?php print url('node/' . $node->nid . '/submission/' . $sid)?>">View your submission</a></li>
    <li><a href="<?php print url('node/' . $node->nid . '/submission/' . $sid . '/edit')?>">Edit your submission</a></li>
  </ul>

  <?php /* End sample webform confirmation page */ ?>

<?php 




$contactId = $submission->data[1][0];
$drop_off_date = $submission->data[15][0];
$drop_off_time = $submission->data[16][0];
$departure_terminal =$submission->data[17][0];

$pick_up_date = $submission->data[18][0];
$pick_up_time = $submission->data[19][0];
$arrival_terminal = $submission->data[20][0];
$flight_number = $submission->data[21][0];
$airport_code_name = $submission->data[22][0];
 
print_r ($contactId);
print "<br>";
print_r ($drop_off_date);
print "<br>";
print_r ($drop_off_time);
print "<br>";
print_r ($departure_terminal);
print "<br>";
print_r ($pick_up_date);
print "<br>";
print_r ($pick_up_time);
print "<br>";
print_r ($arrival_terminal);
print "<br>";
print_r ($flight_number);
print "<br>";
print_r ($airport_code_name);
print "<br>";
$drop_off_together = ($drop_off_date . ' ' . $drop_off_time);
print_r($drop_off_together);

$pick_up_together = ($pick_up_date . ' ' . $pick_up_time);
print_r($pick_up_together);

// Perform bootstrap
civicrm_initialize();



$result = civicrm_api3('Activity', 'create', array(
  'source_contact_id' => $contactId,
  'activity_date_time' => $drop_off_together,
  'activity_type_id' => "55",
  'subject' => "Drop Off",
  'custom_17' => $departure_terminal,
));

$result2 = civicrm_api3('Activity', 'create', array(
  'source_contact_id' => $contactId,
  'activity_date_time' => $pick_up_together,
  'activity_type_id' => "56",
  'subject' => "Pick Up",
  'custom_18' => $arrival_terminal,
  'custom_13' => $flight_number,
  'custom_14' => $airport_code_name,
));