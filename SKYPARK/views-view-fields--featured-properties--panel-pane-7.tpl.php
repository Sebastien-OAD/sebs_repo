<article class="featured-carparks col-md-4">

<div class="featured-carpark-inner col-md-12">
    <div class="col-md-4">
    		<div class="featured-image-carpark"><?php print $fields['field_sp_featured_image']->content;?></div>
    </div>
    <div class="col-md-8">
         <div class="featured-body col-md-12">
         		<div class="featured-carpark-title"><?php print $fields['name']->content;?></div>
              <div class="featured-body"><?php print $fields['field_sp_short_description_et']->content;?></div>
              <div class="featured-parking-list"><?php print $fields['field_parking_features']->content;?></div>
            	
		 </div>
    </div>
    <div class="feature-bottom col-md-12"><div class="featured-price"><?php print $fields['nothing']->content;?></div></div>

</div>

</article>