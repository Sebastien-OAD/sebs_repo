<article class="parking-feature-blocks">
 <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">

<div class="parking-icon-2">
<?php print $fields['field_icon']->content; ?>
</div>
<div class="feature-body">
<div class="parking-feature-title">
<?php print $fields['title']->content; ?></div>
<?php print $fields['body']->content; ?>
</div>

</div>
</article>