<?php
/*
 * Template Name: Kingpin - Home
 *
 */

get_header();?>
<?php

$autoslide_on_off = marigold_blog_get_theme_mod('autoslide_on_off', 'on');

// query args
$args = array(
	'post_type'  => 'page',
	'meta_query' => array(
		array(
			'key'     => 'show_in_home_slider',
			'compare' => '==',
			'value'   => '1',
		)
	)

);

// query posts
$marigold_blog_query = new WP_Query($args);

if ($marigold_blog_query->have_posts()):

?>

<div id="slider-2" data-autoplay="<?php echo $autoslide_on_off?>">

	<div class="slider-content">

<?php

/* Start the Loop */
while ($marigold_blog_query->have_posts()):$marigold_blog_query->the_post();?>

<article style="background-image: url(<?php the_post_thumbnail_url();?>);" id="post-<?php the_ID();?>" <?php post_class();
?>>

	<!-- Post Main Content -->
	<div class="post-main">

		<!-- Post Category -->
		<span class="entry-category"><?php the_category(' / ');?></span>

		<!-- Post Title -->
		<header class="entry-header">


<div class="entry-title"><a href="<?php the_permalink();?> "><?php the_title()?></a></div>

</header>

		<!-- Post Content -->
		<div class="entry-content">

<?php the_field('excerpt');?>

		</div>

		<div class="continue-reading">

			<a href="<?php the_permalink();?>"><?php echo esc_html__('Read More', 'marigold-blog')?><span class="arrow">&#8594;
</span></a>

		</div>

	</div>

	<div class="slider-post-overlay"></div>

</article><!-- #post-## -->
<?php
endwhile;

?>
</div>

</div>

<?php

endif;

wp_reset_postdata();

?>
<!--end of slider -->
<?php get_template_part('template-parts/promo-boxes');?>
<!--removed from header template -->



<div id="content" class="site-content row">

	<div id="primary" class="content-area small-12 large-9 column">

		<main id="main" class="site-main">



<?php

// query args
$args = array(
	'post_type'      => 'page',
	'posts_per_page' => -1,
	'meta_query'     => array(
		array(
			'key'     => 'show_in_home_page_service_feed',
			'compare' => '==',
			'value'   => '1',
		)
	)

);

// query posts
$the_query = new WP_Query($args);?>
<div id="services-feed" class="content-area small-12 large-12 column">


<?php if ($the_query->have_posts()):?>

<?php while ($the_query->have_posts()):$the_query->the_post();?>
<article class="services small-12 medium-12 columns">
		<div class="services-inner-left small-12 medium-6 columns">
			<div class="services-image">
<?php the_post_thumbnail('full');?>

		</div></div>
		<div class="services-inner-right small-12 medium-6 columns">
			<div class="services-title"><a href="<?php echo get_permalink($post->ID);?>"><?php the_field('service_title');
?></a></div>
			<div class="services-description"><?php the_field('service_description');?></div>
			<div class="services-link"><a href="<?php the_permalink();?>"><span class="arrow">&#8594;
</span></a>
</div>


		</div>

	</article>


<?php
endwhile;?>
</div>
<?php endif;

// reset query
wp_reset_postdata();

?>
</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();?>
