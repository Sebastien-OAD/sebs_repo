<?php
/*
 * Template Name: Kingpin - Default
 *
 */

get_header();?>
<div id="content" class="site-content row">

<?php

if (marigold_blog_get_theme_mod('single_sidebar_on_off', 'on') == 'on'):

if (marigold_blog_get_theme_mod('single_sidebar_position', 'right') == 'left'):

get_sidebar('left');

endif;

?>
<div id="primary" class="content-area small-12 large-9 column">

<?php  else :?>
<div id="primary" class="content-area small-12 large-12 column">

<?php endif;?>
<main id="main" class="site-main">

<?php while (have_posts()):the_post();?>
<article data-aos="fade" data-aos-once="true" data-aos-duration="600" data-aos-easing="ease-out-cubic" data-aos-delay="300" data-aos-anchor-placement="top-bottom" >

	<!-- Post Main Content -->
	<div class="post-main-2">

		<!-- Post Thumbnail -->
<?php if (has_post_thumbnail()):?><div class="post-thumb">

<?php the_post_thumbnail('marigold-blog-single-thumb');?>
</div>

<?php endif;?>
<!-- Post Title -->
		<header class="entry-header">

<?php

the_title('<h1 class="entry-title">', '</h1>');

?>
</header>

		<!-- Post Content -->
		<div class="entry-content">

<?php the_content();
?>
</div>

	</div>

	<footer class="entry-footer">

<?php marigold_blog_entry_footer();?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## --> <?php

endwhile;// End of the loop.
?>
</main><!-- #main -->
	</div><!-- #primary -->

<?php
if (marigold_blog_get_theme_mod('single_sidebar_on_off', 'on') == 'on' && marigold_blog_get_theme_mod('single_sidebar_position', 'right') == 'right'):
get_sidebar();
endif;
get_footer();
