<?php
/**
 * hoverbody shortcode
 *
 * Wrtie [hoverbody-urdu] in your post editor to render this shortcode.
 *
 * @package	 ABS
 * @since    1.0.0
 */

if (!function_exists('aa_hoverbody_urdu_shortcode')) {
	// Add the action.
	add_action('plugins_loaded', function () {
			// Add the shortcode.
			add_shortcode('hoverbody-urdu', 'aa_hoverbody_urdu_shortcode');
		});

	/**
	 * hoverbody shortcode.
	 *
	 * @return string Shortcode output string.
	 * @since  1.0.0
	 */
	function aa_hoverbody_urdu_shortcode() {
		// Just return the code.
		return '
<img src="/wp-content/uploads/2018/03/body-white.png" alt="" usemap="#Map" />
<map name="Map" id="Map">
    <area alt="" title="head" id="head-area" href="#" shape="poly" coords="146,17,139,25,142,35,150,37,161,30,157,19" />
    <area alt="" title="neck" id="neck-area" href="#" shape="poly" coords="142,92,139,99,143,107,148,109,159,107,160,101,156,93,148,92" />
    <area alt="" title="shoulder" id="shoulder-area" href="#" shape="poly" coords="202,105,195,114,194,121,199,127,208,127,216,122,217,114,212,107" />
    <area alt="" title="upper-arm" id="upper-arm-area" href="#" shape="poly" coords="207,150,203,157,203,165,209,168,219,167,224,161,223,152,219,150" />
    <area alt="" title="back" id="back-area" href="#" shape="poly" coords="176,190,172,199,177,207,185,208,193,202,192,192,185,187" />
    <area alt="" title="hips" id="hips-area" href="#" shape="poly" coords="172,235,165,242,166,250,174,256,184,250,184,242,180,234" />
    <area alt="" title="wrist" id="wrist-area" href="#" shape="poly" coords="239,249,236,261,249,264,257,260,256,247,248,244" />
    <area alt="" title="upper-leg" id="upper-leg-area" href="#" shape="poly" coords="167,303,173,310,184,308,189,303,186,296,178,292,169,295" />
    <area alt="" title="ankle" id="ankle-area" href="#" shape="poly" coords="168,476,163,484,168,495,174,496,183,493,185,483,180,477" />
    <area alt="" title="feet" id="feet-area" href="#" shape="poly" coords="118,509,110,518,117,526,124,525,131,517,128,510" />
    <area alt="" title="knee" id="knee-area" href="#" shape="poly" coords="118,363,112,370,113,379,120,381,129,376,129,366" />
    <area alt="" title="hands" id="hands-area" href="#" shape="poly" coords="44,271,35,276,39,289,47,290,58,285,54,273" />
    <area alt="" title="elbow" id="elbow-area" href="#" shape="poly" coords="75,179,69,190,76,198,87,198,91,192,87,181" />

</map>


<div id="head-text" class="hover-text" style="display: none">Head
<ul>
               <li>Slight hearing loss - £4,850 to £8,250</li>
               <li>Mild Tinnitus £8,250 to £9,570</li>
               <li>Partial hearing loss with moderate tinnitus - £9,750 to £19,500</li>
               <li>Partial hearing loss with severe tinnitus - £19,500 to £30,000</li>
               <li>Loss of hearing in one ear - £20,500 to £30,000</li>
               <li>Total deafness - £59,500 to £72,000</li>
               <li>Minor head injuries - £1,450 to £8,400</li>
               <li>Minor brain damage - £10,000 to £150,000</li>
               <li>Epilepsy as a result of injury - £36,000 to £98,500</li>
               <li>Moderate to severe brain damage - £144,500 to £1,500,000</li>
               <li>Very severe brain damage - £185,000 to £3,000,000</li>
            </ul>


</div>
<div id="neck-text" class="hover-text" style="display: none">Neck
<ul>
               <li>Whiplash injuries (taking up to 2 years to heal) - £5,150 to £9,000</li>
               <li>Whiplash injuries (taking less than 2 years to heal) - £1,000 to £6,000</li>
               <li>Severe neck injuries (eg: fractured bones) - £16,400 to £21,600</li>
               <li>Permanent disability from neck injuries - £21,600 to £97,500</li>
            </ul>
</div>
<div id="shoulder-text" class="hover-text" style="display: none">Shoulder
<ul>
               <li>Clavicle bone fractures - £3,400 to £8,000</li>
               <li>Limited movement in shoulder lasting up to 2 years as a result of injury - £5,150 to £8,400</li>
               <li>Disability from severe shoulder injury - £5,150 to £8,400</li>
            </ul>

</div>
<div id="upper-arm-text" class="hover-text" style="display: none">Upper Arm
<ul>
               <li>Minor injury/fracture to arm - £4,350 to £12,600</li>
               <li>Disability as a result of serious arm injury - £25,750 to £86,000</li>
            </ul>

</div>
<div id="back-text" class="hover-text" style="display: none">Back
<ul>
               <li>Back strains &amp; sprains (healing within 2 years) - £1,000 to £5,150</li>
               <li>Back strains &amp;
 sprains (healing between 2 &amp; 5 years) - £5,150 to £8,250</li>
               <li>Permanent disability from serious back in juries - £25,500 to £111,000</li>
            </ul>
</div>
<div id="hips-text"  class="hover-text" style="display: none">Hips
<ul>
               <li>Minor hip injury average payouts - Up to £2,800</li>
               <li>Broken hip with no ongoing symptoms settlements - £2,800 - £9,000</li>
               <li>Injury Requiring hip replacement compensation amounts - £9,000 - £28,000</li>
               <li>Serious hip injury payouts - £28,000 - £94,000</li>
            </ul>

</div>
<div id="wrist-text" class="hover-text" style="display: none">Wrist
<ul>
               <li>Fracture of the wrist - £2,300 to £4,850</li>
               <li>Wrist injuries resulting in pain/stiffness - £8,250 to £16,100</li>
               <li>Disability as a result of severe wrist injuries - £16,100 to £39,000</li>
            </ul>
</div>
<div id="upper-leg-text" class="hover-text" style="display: none">Upper Leg
<ul>
               <li>Fractures to leg bones/minor leg injuries - £1,000 to £6,000</li>
               <li>Leg injuries which do not heal - £11,800 to £18,250</li>
               <li>Fracture to thigh (femur) bone - £6,000 to £9,200</li>
            </ul>

</div>
<div id="ankle-text" class="hover-text" style="display: none">Ankle
<ul>
               <li>Minor ankle injury - up to £3,600</li>
               <li>Damaged Achilles Tendon - £4,700 - £25,000</li>
               <li>Ankle injuries requiring metal pins and plates to be inserted - £3,500 - £32,000</li>
               <li>Serious ankle injuries - up to £45,000</li>
            </ul>

</div>
<div id="feet-text" class="hover-text" style="display: none">Feet
<ul>
               <li>Soft tissue injury, or fracture to a toe - £1,000 to £3,500</li>
               <li>Loss of big toe as a result of injury - £12,600 to £20,500</li>
               <li>Serious foot injuries - £16,400 to £20,500</li>
               <li>Fractures to both heels - £30,000 to £44,200</li>
            </ul>
</div>
<div id="knee-text" class="hover-text" style="display: none">Knee
<ul>
               <li>Twists and painful bruising knee injuries - £1,000 - £3,800</li>
               <li>Twists with ongoing symptoms average payouts - £3,800 - £9,000</li>
               <li>Torn cartilage in the knee compensation calculator - £9,000 - £18,000</li>
               <li>Serious knee injuries compensation amounts - £18,000 - £63,000</li>
            </ul>
</div>
<div id="hands-text" class="hover-text" style="display: none">Hands
<ul>
               <li>Minor injuries to thumb - £1,450 to £2,600</li>
               <li>Fracture to a finger - £2,000 to £3,125</li>
               <li>Severe dislocation of thumb - £2,600 to £4,450</li>
               <li>Serious injury to finger - £9,750 to £10,750</li>
               <li>Severe fracture/Crushing of hand - £9,500 to £19,000</li>
               <li>Loss of thumb in accident or due to amputation - £23,250 to £36,000</li>
            </ul>
</div>
<div id="elbow-text" class="hover-text" style="display: none">Elbow
<ul>
               <li>Minor injury/fracture to elbow - £4,350 to £12,600</li>
               <li>Disability as a result of serious elbow injury - £25,750 to £86,000</li>
            </ul>
</div>



<script type="text/javascript">
jQuery("#head-area").mouseenter(function() {
      jQuery("#head-text").show();
}).mouseleave(function() {
      jQuery("#head-text").hide();
});

jQuery("#neck-area").mouseenter(function() {
      jQuery("#neck-text").show();
}).mouseleave(function() {
      jQuery("#neck-text").hide();
});

jQuery("#shoulder-area").mouseenter(function() {
      jQuery("#shoulder-text").show();
}).mouseleave(function() {
      jQuery("#shoulder-text").hide();
});

jQuery("#upper-arm-area").mouseenter(function() {
      jQuery("#upper-arm-text").show();
}).mouseleave(function() {
      jQuery("#upper-arm-text").hide();
});

jQuery("#back-area").mouseenter(function() {
      jQuery("#back-text").show();
}).mouseleave(function() {
      jQuery("#back-text").hide();
});

jQuery("#hips-area").mouseenter(function() {
      jQuery("#hips-text").show();
}).mouseleave(function() {
      jQuery("#hips-text").hide();
});

jQuery("#wrist-area").mouseenter(function() {
      jQuery("#wrist-text").show();
}).mouseleave(function() {
      jQuery("#wrist-text").hide();
});

jQuery("#upper-leg-area").mouseenter(function() {
      jQuery("#upper-leg-text").show();
}).mouseleave(function() {
      jQuery("#upper-leg-text").hide();
});

jQuery("#ankle-area").mouseenter(function() {
      jQuery("#ankle-text").show();
}).mouseleave(function() {
      jQuery("#ankle-text").hide();
});

jQuery("#feet-area").mouseenter(function() {
      jQuery("#feet-text").show();
}).mouseleave(function() {
      jQuery("#feet-text").hide();
});

jQuery("#knee-area").mouseenter(function() {
      jQuery("#knee-text").show();
}).mouseleave(function() {
      jQuery("#knee-text").hide();
});

jQuery("#hands-area").mouseenter(function() {
      jQuery("#hands-text").show();
}).mouseleave(function() {
      jQuery("#hands-text").hide();
});

jQuery("#elbow-area").mouseenter(function() {
      jQuery("#elbow-text").show();
}).mouseleave(function() {
      jQuery("#elbow-text").hide();
});
</script>';
	}
}
