<?php get_header(); ?>



<style type="text/css">

	

.main-content-section .buttons-wrap .button.solid {

	background-color: #442d34;

	border-color: #442d34;

	color: #ffffff;

}	



.main-content-section .buttons-wrap .button.solid:hover {

	background-color: transparent;

	color: #442d34;

}



.main-content-section .buttons-wrap .button.transparent {

	background-color: transparent;

	border-color: #442d34;

	color: #442d34;

}	



.main-content-section .buttons-wrap .button.transparent:hover {

	background-color: #442d34;

	color: #ffffff;

}

	

</style>



<!--START PROPERTY INFO CONTENT-->





<div class="property-slideshow-wrap" style="margin-top: 0px!important;">

	

	<div class="property-slideshow">



		<?php if(get_field('lettings_property_slideshow')): ?>

			<?php while(has_sub_field('lettings_property_slideshow')): ?>



				<div class="property-slideshow-slide" style="background-image:url('<?php the_sub_field('lettings_property_image'); ?>');">

				</div>



			<?php endwhile; ?>

		<?php endif; ?>	

		

	</div>

	

	<ul id="nav">

		

		<?php if(get_field('lettings_property_slideshow')): $counter = 1; ?>

			<?php while(has_sub_field('lettings_property_slideshow')): ?>

		

				<li><a href="#">0<?php echo $counter; ?></br><?php the_sub_field('lettings_property_image_title'); ?></a></li>

			

			<?php $counter++; ?>

			<?php endwhile; ?>

		<?php endif; ?>	

		

	</ul>

	

	<div id="left" class="slideshow-nav"></div>

	<div id="right" class="slideshow-nav"></div>

	

	<div class="property-logo" style="background-image:url('<?php the_field('property_logo'); ?>');"></div>

	

		

</div>


<!--START ARRANGE VIEWING CONTENT-->	



<div class="main-content-section" id="arrange">

	<div class="inner-content">

		

		<div class="main-content-header">

			

			<div class="main-content-column">

				<h1>Arrange a Viewing</h1>

				<h3>

					<div class="mini-two-col">

						<div class="col">

							<?php the_field('arrange_viewing_left_column'); ?>

						</div>

						<div class="col">

							<?php the_field('arrange_viewing_right_column'); ?>

						</div>

					</div>

				</h3>

			</div>

			

			<div class="main-content-column">

				<?php echo do_shortcode('[contact-form-7 id="31" title="Viewing Form"]'); ?>				

			</div>

			

		</div>

		

	</div>

</div>



<!--END ARRANGE VIEWING CONTENT-->	



<div class="main-content-section">

	

	<div class="inner-content">

		<div class="main-content-header">

			<div class="main-content-column" style="color: #505050;">

				<h1>

				<span style="color: #442d34;">

				<?php the_field('lettings_property_titles'); ?></br></span>

				<?php the_field('lettings_property_subtitles'); ?>

				</h1>

				

				<div class="percentage-occupied-holder">

					<span class="percentage-occupied"><?php the_field('percentage_occupied'); ?>%</span>

					<span class="percentage-occupied-sub">Occupied</span>

				</div>

								

			</div>

			

			<h3 class="main-content-column property-description">

				

				<span class="short">

					<?php the_field('lettings_property_description'); ?>

					

					<div class="features-section">

					

						<?php if(get_field('property_features')): ?>

							<?php while(has_sub_field('property_features')): ?>

									<div class="feature"> <div class="feature-icon" style="background-image:url('<?php the_sub_field('feature_icon'); ?>');"></div> <div class="feature-text"><?php the_sub_field('feature_text'); ?></div> </div>

							<?php endwhile; ?>

						<?php endif; ?>	

					

					</div>

					

					<div class="buttons-wrap">

						<div class="wpcf7-submit scroll-booking">Arrange a Viewing</div>

						<a href="#arrange"><div class="wpcf7-submit">Find Out More</div></a>

					</div>

					

				</span>

				

				<span class="long">

					<?php the_field('lettings_property_description'); ?>

				</span>

				

				<div class="read-less"><span>Read Less</span></div>

				<div class="read-more"><span>Read More</span></div>

				

				<p style="margin-top: 40px; float: left;"><span class="currentViewing">5</span> others are currently viewing this property</p>

				

			</h3>

		</div>

	</div>

	

	

	

</div>



<!--END PROPERTY INFO CONTENT-->	



<!--START MAP CONTENT-->	



<div class="main-content-section">

	<div class="inner-content">

		

		

		<div class="main-content-header">

			<h1 class="main-content-column">Apartment Packages</h1>

			

			<h3 class="main-content-column">

				<?php the_field('lettings_property_packages_text'); ?>

			</h3>

		</div>

		

		<div class="packages-holder">

			

			<div class="left-tab">

				

				<div class="tab-title">Cluster Apartments (3-7 beds)</div>

				

				<div class="tab-content">

					

					<div class="line"></div>

					

					<h2>What's included in your cluster apartment</h2>

					<div class="bullets">

						

						<div class="bullets-col">

							<?php the_field('cluster_included_left'); ?>

						</div>

						

						<div class="bullets-col">

							<?php the_field('cluster_included_right'); ?>

						</div>

						

					</div>

					

					<div class="line"></div>

					

					<h2>What's included in the cluster common room</h2>

					<div class="bullets">

						

						<div class="bullets-col">

							<?php the_field('cluster_cr_included_left'); ?>

						</div>

						

						<div class="bullets-col">

							<?php the_field('cluster_cr_included_right'); ?>

						</div>

						

					</div>

					

					<div class="line"></div>

					

					<h2>Choose one of our three package options below</h2>

					

					<div class="packages-comparison">

						

						<div class="col-gold col">

							

							<div class="title">GOLD</div>

							

							<?php

							if( have_rows('gold_pack') ):

							

							    while ( have_rows('gold_pack') ) : the_row();?>

							        

							        <div class="packages-item <? the_sub_field('closed_by_default');?>">

										<div class="packages-title"><? the_sub_field('title');?><div class="arrow"><img src="/wp-content/themes/golding/images/arrow.svg"/></div></div>

										<div class="packages-description"><? the_sub_field('items');?></div>

									</div>

							

							<? endwhile;

							else :

							endif;

							?>

							

							<div class="packages-footer">

							

								<div class="packages-cost">

									<div class="price"><?php the_field('gold_package_price'); ?></div>

									<div class="description"><?php the_field('gold_package_price_description'); ?></div>

								</div>

								<div class="book-now book-now-gold">Book Now</div>

							

							</div>

							

						</div>

						

						<div class="col-silver col">

							

							<div class="title">SILVER</div>

							

							<?php

							if( have_rows('silver_pack') ):

							

							    while ( have_rows('silver_pack') ) : the_row();?>

							        

							        <div class="packages-item <? the_sub_field('closed_by_default');?>">

										<div class="packages-title"><? the_sub_field('title');?><div class="arrow"><img src="/wp-content/themes/golding/images/arrow.svg"/></div></div>

										<div class="packages-description"><? the_sub_field('items');?></div>

									</div>

							

							<? endwhile;

							else :

							endif;

							?>

							

							<div class="packages-footer">

							

								<div class="packages-cost">

									<div class="price"><?php the_field('silver_package_price'); ?></div>

									<div class="description"><?php the_field('silver_package_price_description'); ?></div>

								</div>

								<div class="book-now book-now-silver">Book Now</div>

							

							</div>

							

						</div>

						

						<div class="col-bronze col">

							

							<div class="title">BRONZE</div>

							

							<?php

							if( have_rows('bronze_pack') ):

							

							    while ( have_rows('bronze_pack') ) : the_row();?>

							        

							        <div class="packages-item <? the_sub_field('closed_by_default');?>">

										<div class="packages-title"><? the_sub_field('title');?><div class="arrow"><img src="/wp-content/themes/golding/images/arrow.svg"/></div></div>

										<div class="packages-description"><? the_sub_field('items');?></div>

									</div>

							

							<? endwhile;

							else :

							endif;

							?>

														

							<div class="packages-footer">

								

								<div class="packages-cost">

									<div class="price"><?php the_field('bronze_package_price'); ?></div>

									<div class="description"><?php the_field('bronze_package_price_description'); ?></div>

								</div>

								<div class="book-now book-now-bronze">Book Now</div>

							

							</div>

							

						</div>

						

					</div>

					

				</div>

				

			</div>

			

			<div class="right-tab not-selected">

				

				<div class="tab-title">Studio Apartments</div>

				

				<div class="tab-content">

					

					<div class="line"></div>

					

					<h2>What's included in your apartment</h2>

					<div class="bullets">

						

						<div class="bullets-col">

							<?php the_field('studio_included_left'); ?>

						</div>

						

						<div class="bullets-col">

							<?php the_field('studio_included_right'); ?>

						</div>

						

					</div>

					

					<div class="line"></div>

					

					<h2>The Studio extras pack includes</h2>

					<div class="bullets">

						

						<div class="bullets-col">

							<?php the_field('studio_extras_left'); ?>

						</div>

						

						<div class="bullets-col">

							<?php the_field('studio_extras_right'); ?>

						</div>

					

					</div>

					

					<div class="studio-footer">

								

							<div class="packages-cost">

								<div class="price"><?php the_field('studio_package_price'); ?></div>

								<div class="description"><?php the_field('studio_package_price_description'); ?></div>

							</div>

							<div class="book-now book-now-studio">Book Now</div>

							

					</div>

				

			</div>

			

		</div>

		

	</div>

</div>



</div>



<?php if (get_field('show_related_developments') == true):?>



<div class="main-content-section" id="related-lettings">

	<div class="inner-content">	



<div class="main-content-header">

	<h1 class="main-content-column"><?php the_field('related_developments_title'); ?></h1>

	

	<h3 class="main-content-column">

		<?php the_field('related_developments_description'); ?>

	</h3>

</div>



<div class="lettings-grid-wrap">

	

	

	<?php 

		

	$posts = get_field('related_developments');

	

	if( $posts ): ?>

	

	    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>

	        <?php setup_postdata($post); ?>

	        <a class="lettings-grid-block animsition-link" href="<?php the_permalink(); ?>" data-animsition-out="fade-out" data-animsition-out-duration="500" style="height: 430px!important;">

				<div class="lettings-image" style="background-image:url('<?php the_field('lettings_thumbnail_image'); ?>');"></div>

				<div class="lettings-text">

					<div class="top">

						<div><?php the_field('lettings_property_titles'); ?></div>

					</div>

					<div class="bottom">

						<div><?php the_field('lettings_property_subtitles'); ?></div>

					</div>

				</div>

				<div class="view-button">View</div>

			</a>

	    <?php endforeach; ?>

	    

	    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

	

	<?php endif; ?>

	

</div>



	</div>

</div>



<?php endif; ?>

		

	

<div class="main-content-section">

	<div class="inner-content">	

		

		<div class="main-content-header">

			<h1 class="main-content-column">Location</h1>

			

			<h3 class="main-content-column">

				<?php the_field('lettings_property_location_text'); ?>

			</h3>

		</div>

		

		<?php $propertylocation = get_field('lettings_property_location_select'); ?>

		

		<div class="map-center-cos" style="position: fixed; top: 0; left: -99999px; opacity: 0;">

			<div class="map-center-lat"><?php echo $propertylocation['lat']; ?></div>

			<div class="map-center-lng"><?php echo $propertylocation['lng']; ?></div>

		</div>

		

		<div class="map-column left acf-map">



			<?php if( have_rows('general_map_locations', 'option') ): ?>

					<?php while ( have_rows('general_map_locations', 'option') ) : the_row(); ?>

			

						<?php $location = get_sub_field('general_map_locations_location'); ?>

						

						<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>" type="<?php the_sub_field('general_map_locations_type'); ?>">

							<p class="address"><?php echo $location['address']; ?></p>

						</div>

						

				<?php endwhile; ?>

			<?php endif; ?>	

			

		</div>

		

		

		

	</div>

</div>



<div class="purple-booking-section">

		

	<div class="inner-content">

			

		<div class="left-column">

			<h1>Your Booking</h1>

		</div>

		

		<div class="right-column">

			<h3>Configure your booking package today and a member of the urbanbubble team will get back to you to obtain some extra details. Please ensure that the number of people, duration, package type and number of students is correct.</h3>				

		</div>

		

		<div class="booking-form qp">

			<?php echo do_shortcode('[contact-form-7 id="396" title="Booking Form"]'); ?>

		</div>		

		

	</div>

				

</div>



<!--END MAP CONTENT-->	








			

<?php get_footer(); ?>