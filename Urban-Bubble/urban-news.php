<?php
/**
 * Template Name: Urban News
 *
 *
 */

get_header();
?>
<div id="Content">
	<div class="content_wrapper clearfix">

		<!-- .sections_group -->
		<div class="sections_group">

			<div class="entry-content" itemprop="mainContentOfPage">

<?php while (have_posts()) {
	the_post();// Post Loop
	mfn_builder_print(get_the_ID());// Content Builder & WordPress Editor Content
}
?>
<!-- start of news stories -->
				<div class="news-stories-feed-page col-md-12">





					<div class="news-stories-feed-title">News</div>
					<div class="ui-group">
    <div class="ui-group__title d-cs-f"><button>Filter News by Category</button></div>


<div id="filterButtons">

    <div class="filter-button-group js-radio-button-group buttons-left">
      <button class="button od-cd-but is-checked" data-filter="*">show all</button>



<div class="filter-title">Category</div>

<?php
$terms = get_terms("urban_news");
$count = count($terms);
if ($count > 0) {
	foreach ($terms as $term) {

		?> <button class="button od-cd-but" data-filter=".<?php echo $term->slug?>">  <?php echo $term->name?> </button><?php
	}
}
?>
<div class="filter-title">Authors</div>
<?php $loop = new WP_Query(array('post_type' => 'news_stories', 'posts_per_page' => -1, 'meta_key' => 'news_date', 'orderby' => 'meta_value', 'order' => 'DESC'));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
        <?php $author = get_field('author');
if (in_array($author, $added)) {
	continue;
}
$added[] = $author?>

      <button class="button od-cd-but" data-filter=".author-<?php echo $author;?>"><?php the_field('author');
?></button>

<?php endwhile;

wp_reset_query();?>
<div class="filter-title">Date Published</div>

<?php $loop = new WP_Query(array('post_type' => 'news_stories', 'posts_per_page' => -1, 'meta_key' => 'news_date', 'orderby' => 'meta_value', 'order' => 'DESC'));?>
<?php while ($loop->have_posts()):$loop->the_post();?>
<?php $date = get_field('news_date');
$date2      = date("M", strtotime($date));

if (in_array($date2, $added)) {
	continue;
}
$added[] = $date2?>

<button class="button od-cd-but" data-filter=".date-<?php echo $date2;?>"><?php echo $date2;
?></button>


      <!--<button class="button od-cd-but" data-filter=".date-Jan">Jan</button>
      <button class="button od-cd-but" data-filter=".date-Feb">Feb</button>
      <button class="button od-cd-but" data-filter=".date-Mar">Mar</button>
      <button class="button od-cd-but" data-filter=".date-Apr">Apr</button>
      <button class="button od-cd-but" data-filter=".date-May">May</button>
      <button class="button od-cd-but" data-filter=".date-Jun">Jun</button>
      <button class="button od-cd-but" data-filter=".date-Jul">Jul</button>
      <button class="button od-cd-but" data-filter=".date-Aug">Aug</button>
      <button class="button od-cd-but" data-filter=".date-Sep">Sep</button>
      <button class="button od-cd-but" data-filter=".date-Oct">Oct</button>
      <button class="button od-cd-but" data-filter=".date-Nov">Nov</button>
      <button class="button od-cd-but" data-filter=".date-Dec">Dec</button>-->

<?php endwhile;

wp_reset_query();?>
</div>
  </div></div>

<?php $loop = new WP_Query(array('post_type' => 'news_stories', 'posts_per_page' => -1, 'meta_key' => 'news_date', 'orderby' => 'meta_value', 'order' => 'DESC'));?>
<div class="grid">
  <div class="grid-sizer"></div>
<?php while ($loop->have_posts()):$loop->the_post();?>
	<?php
$date  = get_field('news_date');
$date2 = date("M", strtotime($date));?>


<?php $termsArray = get_the_terms($post->ID, "urban_news");
$termsString      = "";//initialize the string that will contain the terms
foreach ($termsArray as $term) {// for each term
	$termsString .= $term->slug.' ';//create a string that has all the slugs
};?>

						<article class="news-story <?php echo $termsString;?>grid-item  author-<?php the_field('author');?> date-<?php echo $date2;?>">


<?php if (is_page(198)) {
	?>
																																																		<div class="news-stroies-image"><?php $news_image = get_field('news_image');?>
																																										<?php if ($news_image) {?>
																																																																																					<a href="<?php the_permalink();?>"><img src="<?php echo $news_image['url'];?>" alt="<?php echo $news_image['alt'];?>" /></a>
		<?php }?></div>

	<?php
}?>
							<div class="news-inner">

							<div class="news-title"><a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>"><?php the_title();
?></a></div>

							<div class="news-summary"><?php the_field('summary');?></div>

							<div class="news-read-more"><a href="<?php the_permalink();?>">Read more</a></div>
							</div>









						</article>
<?php endwhile;
wp_reset_query();?>
</div></div>






				<!--end of news stories-->









				<div class="section section-page-footer">
					<div class="section_wrapper clearfix">

						<div class="column one page-pager">
<?php
// List of pages
wp_link_pages(array(
		'before'         => '<div class="pager-single">',
		'after'          => '</div>',
		'link_before'    => '<span>',
		'link_after'     => '</span>',
		'next_or_number' => 'number',
	));
?>
</div>

					</div>
				</div>

			</div>

<?php if (mfn_opts_get('page-comments')):?>
<div class="section section-page-comments">
					<div class="section_wrapper clearfix">

						<div class="column one comments">
<?php comments_template('', true);?>
</div>

					</div>
				</div>
<?php endif;?>
</div>

		<!-- .four-columns - sidebar -->
<?php get_sidebar();?>
</div>
</div>
<script src="/wp-content/themes/betheme-child/js/isotope.pkgd.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
$('.grid').isotope({
  // set itemSelector so .grid-sizer is not used in layout
  itemSelector: '.grid-item',
  percentPosition: true,
  layoutMode: 'masonry',
  masonry: {
    // use element for option
    columnWidth: '.grid-sizer',
    gutter: 10,

  }
})
// init Isotope
var $grid = $('.grid').isotope({
  // options
});
// filter items on button click
$('.filter-button-group').on( 'click', 'button', function() {
  var filterValue = $(this).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});

});
</script>
<script type="text/javascript">
	$("button").click(function() {
    $("#filterButtons").toggle();
});

</script>

<?php get_footer();

// Omit Closing PHP Tags