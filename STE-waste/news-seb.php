<?php

if (isset($_meta) && array_key_exists('_fragment_01', $_meta) && trim($_meta['_fragment_01']) != '') {
	$pcnt_fragment_01 = $_meta['_fragment_01'];
} else {
	$pcnt_fragment_01 = '';
}

$newsdate = $query_1;
$article  = $query_2;
$page     = $query_3;

if (trim($newsdate) === "") {/* If the user comes in straight from the menu link */

	$newsdate = date("Y-m");/* Add the date first */

	$rset = conn_get("SELECT COUNT(ID) FROM tbl_alpha_news WHERE userdate >= '".$newsdate."-01' AND userdate <= '".$newsdate."-31' AND status = '1'");/* Check to see if dates for this month exist */

	if ($rset[0] > 0) {/* Result of above check */

		/* End of problem */

	} else {

		$rset = conn_get("SELECT ID, userdate FROM tbl_alpha_news WHERE userdate <= '".$newsdate."-01' AND status = '1' ORDER BY userdate DESC LIMIT 0, 1");/* FInd a month with articles that is before the current date */

		if ($rset[0] > 0) {/* Check articles for criteria exist otherwise just use todays date as set above */
			$newsdate = substr($rset[1], 0, 7);
		}

	}

}

$rset = conn_get("SELECT meta_dateformat, meta_dateformat, meta_ordertype FROM tbl_adm_settings WHERE alias = 'alpha_news'");

$endlimit = $rset[0];### Items per page ###
if ($endlimit == 0) {
	$endlimit = "100000";
}
$dataformat = $rset[1];
$ordertype  = $rset[2];

if ($ordertype == "od" || $ordertype == "oa") {
	$userdateorderby = "orderby";
} elseif ($ordertype == "dd" || $ordertype == "da") {
	$userdateorderby = "userdate";
} else {
	$userdateorderby = "alias";
}

if ($ordertype == "dd" || $ordertype == "od") {
	$ascdesc = "DESC";
} else {
	$ascdesc = "ASC";
}

$arcyear__arr  = array();
$arcmonth__arr = array();

$resultset = conn_gets("SELECT ID, userdate FROM tbl_alpha_news WHERE status = '1' ORDER BY userdate DESC");

while ($rset = mysql_fetch_row($resultset)) {

	$arch_ID            = $rset[0];
	$arch_year          = dateFormat("Y", $rset[1], 0);
	$arch_userdate      = dateFormat("Y-m", $rset[1], 0);
	$arch_userdatemonth = dateFormat("F", $rset[1], 0);

	$arcyear__arr[$arch_year]      = $arch_year;
	$arcmonth__arr[$arch_userdate] = $arch_userdatemonth;

}

unset($resultset, $rset, $arch_last_year, $arch_ID, $arch_year, $arch_userdate, $arch_userdatemonth);

$isActive = '';
if ($newsdate == 'all') {
	$isActive = ' class="active"';
}
$compile_archive = "\n	   <li${isActive}><a href=\"/${permalink}/all/\">All</a></li>";

unset($isActive);

$month_end = false;

foreach ($arcyear__arr as $key => $value) {

	if (strlen($key) === 4) {

		$class_append = "archive-year";
		if ($key == substr($newsdate, 0, 4)) {
			$class_append .= " active";
		}

		if (substr($key, 0, 4) == substr($newsdate, 0, 4)) {

			$class_append .= " has-months";
			$month_end = true;

			$compile_archive .= "\n	   <li class=\"".$class_append."\"><a href=\"/$permalink/$key/\">$value</a>";

			foreach ($arcmonth__arr as $key => $value) {
				if (substr($key, 0, 4) == substr($newsdate, 0, 4)) {
					$class_append = "archive-month";
					if ($key === $newsdate) {
						$class_append .= " active";
					}
					$compile_archive .= "\n	     <li class=\"".$class_append."\"><a href=\"/$permalink/$key/\">&ndash; $value</a>";
				}
			}

		} else {

			if ($month_end) {
				$class_append .= " month-end";
				$month_end = false;
			}

			$compile_archive .= "\n	   <li class=\"".$class_append."\"><a href=\"/$permalink/$key/\">$value</a>";

		}

		$compile_archive .= "</li>";

	}

}

$render_archive = '';

if (trim($compile_archive) != '') {

	$render_archive = <<<EOF

         <div class="sidebar">
          <nav class="sidebar-list-to-select">
           <ul class="sidebar-nav">${compile_archive}</ul>
          </nav>
         </div>
EOF

;

}

unset($arcyear__arr, $arcmonth__arr, $key, $value, $init_news_date, $month_end, $newsdate_year, $type, $datetext, $isActive, $isActive_sub);

if ($article > 0) {

	$rset = conn_get("SELECT ID, parentid, toplevelid, levelid, alias, permalink, _image, _fragment_01, author, contentopen, fragparts, keygrip, aux1, aux2, aux3, lockeduser, lockeddate, created, updated, userdate, orderby, status

			FROM tbl_alpha_news WHERE ID = '${article}' AND status = '1'");

$news_ID          = $rset[0];
$news_parentid    = $rset[1];
$news_toplevelid  = $rset[2];
$news_levelid     = $rset[3];
$news_alias       = $rset[4];
$news_permalink   = $rset[5];
$news_image       = $rset[6];
$news_fragment_01 = $rset[7];
$news_author      = $rset[8];
$news_contentopen = $rset[9];
$news_fragparts   = $rset[10];
$news_keygrip     = $rset[11];
$news_aux1        = $rset[12];
$news_aux2        = $rset[13];
$news_aux3        = $rset[14];
$news_lockeduser  = $rset[15];
$news_lockeddate  = $rset[16];
$news_created     = $rset[17];
$news_updated     = $rset[18];
$news_userdate    = $rset[19];
$news_orderby     = $rset[20];
$news_status      = $rset[21];

$pcnt_title_or_alias_tag = '<h2 class="page-title">'.$news_alias.'</h2>';

$_newa_meta = array();

$resultset = conn_gets("SELECT meta_key, meta_value FROM tbl_alpha_news_meta WHERE post_ID = '${news_ID}' AND post_status = 'published'");

$nmet_count = 0;

while ($rset = mysql_fetch_row($resultset)) {

	$nmet_count++;

	$_newa_meta[$rset[0]] = $rset[1];

}

if (isset($_newa_meta)) {

	if (array_key_exists('_fragment_01', $_newa_meta) && trim($_newa_meta['_fragment_01']) != '') {
		$_fragment_01 = $_newa_meta['_fragment_01'];
	} else {
		$_fragment_01 = '';
	}

	if (array_key_exists('_metatitle', $_newa_meta) && trim($_newa_meta['_metatitle']) != '') {
		$meta_title = $_newa_meta['_metatitle'];
	}

	if (array_key_exists('_metadescription', $_newa_meta) && trim($_newa_meta['_metadescription']) != '') {
		$meta_description = useSubMeta($_newa_meta['_metadescription'], $meta_metadescription);
	}

	if (array_key_exists('_metakeywords', $_newa_meta) && trim($_newa_meta['_metakeywords']) != '') {
		$meta_keywords = useSubMeta($_newa_meta['_metakeywords'], $meta_metakeywords);
	}

}

$link_canonical = $public_website_url."/$pcnt_permalink/all/" .$news_ID."/1/".preg_replace("/[^-a-zA-Z0-9s\[\]_]/", "", str_replace(" ", "-", trim(substr(strtolower($news_alias), 0, 255))))."/";

$news_userdate_formatted = dateFormat("D d M Y", $news_userdate, 0);

$compile_images = '';

$media_location = 'images/media/_img/';

$rset = conn_get("SELECT ID, alias, filetype, newfilename FROM tbl_media_library WHERE ID = '${news_image}' AND status = '1'");

$media_ID          = $rset[0];
$media_alias       = $rset[1];
$media_filetype    = $rset[2];
$media_newfilename = $rset[3];

$media_file_location = $media_location.$media_newfilename;

if ($media_filetype == '_image' && trim($media_newfilename) != '' && is_file($media_file_location)) {

	$image_exists = 1;

	$compile_images = "<a href=\"#\" class=\"timg-bg\" style=\"background-image: url( '/${media_file_location}' );
 \"><img src=\"/images/thumbnail-ratio.png\" class=\"img-responsive\" alt=\"${media_alias}\"></a>";
}

unset($media_ID, $media_alias, $media_filetype, $media_newfilename, $media_file_location);

if ($compile_images != "") {

	$eightortwelve = "col-xs-12 col-sm-7";

	$image_holder = "<div class=\"col-xs-12 col-sm-5\">${compile_images}</div>";

} else {

	$eightortwelve = "col-xs-12";

}

$backpath_is_referer = 0;
if ($backpath_is_referer == 1) {

	$ref__ar  = parse_url($_SERVER["HTTP_REFERER"]);
	$backPath = $ref__ar[path];

	$back_to_message = "BACK";

} else {

	$backPath = "/$permalink/";
	if (trim($query_1) != "all") {
		$backPath .= $query_1."/0/$page/";
	} else {
		$backPath .= "all/0/$page/";
	}

	$back_to_message = "BACK";

}

$render_news = <<<EOF
<div class="news-article">   <!--individual news articles-->
         <div class="row">
${image_holder}
          <div class="${eightortwelve} mb-xs-20">

           <p class="article-date">${news_userdate_formatted}</p>

${_fragment_01}

           <div class="sharrre-widget" data-title="${news_alias}" data-url="${PHP_CURL}"></div>

          </div>
         </div>

         <div class="button">
          <a href="${backPath}"><span>${back_to_message}</span></a>
         </div>

        </div>

EOF

;

} else {

	if (trim($pcnt_title) != '') {

		$pcnt_title_or_alias = str_replace("\n", '</span><span>', $pcnt_title);

	} else {

		$pcnt_title_or_alias = $pcnt_alias;

	}

	$pcnt_title_or_alias_tag = '<p class="h1 page-title" ><span>'.$pcnt_title_or_alias.'</span></p>';
	if ($pcnt_title_seo == 1) {
		$pcnt_title_or_alias_tag = '<h1 class="page-title" ><span>'.$pcnt_title_or_alias.'</span></h1>';
	}

	unset($pcnt_title_or_alias);

	if ($page == 0) {
		$page = 1;
	}

	$link_canonical = $public_website_url."/$permalink/$newsdate/0/$page/";

	$sql_where = '';

	if (trim($newsdate) != "" && trim($newsdate) != "all") {

		if (strlen($key) === 4) {
			$newsdate_first = $newsdate."-01-01";
			$newsdate_last  = $newsdate."-12-31";
		} else {
			$newsdate_first = $newsdate."-01";
			$newsdate_last  = $newsdate."-31";
		}

		$sql_where = "userdate >= '$newsdate_first' AND userdate <= '$newsdate_last' AND";

	}

	### PAGES NEXT & PREV : START

	$endlimit = 20;### Items Per Page ###

	$rset       = conn_get("SELECT COUNT(ID) FROM tbl_alpha_news WHERE ${sql_where} status = '1'");
$recordcount = $rset[0];
$pagescount  = numberFormat(ceil($recordcount/$endlimit), 0);

if ($page > $pagescount) {
	$page = numberMin($pagescount, 1);
}

$compile_pages = "";

if ($pagescount > 1) {

	for ($ipage = 1; $ipage <= $pagescount; $ipage++) {

		if ($ipage < 10) {
			$ipage_zeros = "0".$ipage;
		} else {
			$ipage_zeros = $ipage;
		}

		if ($ipage == $page) {
			$compile_pages .= "<li><span>$ipage_zeros</span></li>";
		} else {
			$compile_pages .= "<li><a href=\"/$permalink/$newsdate/0/$ipage/\">$ipage_zeros</a></il>";
		}

	}

	$nxtpage = ($page+1);
	$prepage = ($page-1);

}

$startlimit = numberFormat((($page-1)*$endlimit), 0);
if ($startlimit < 0) {
	$startlimit = 0;
}

$previous = ($page-1);
if ($previous < 1) {
	$previous = $pagescount;
}

$next = ($page+1);
if ($next > $pagescount) {
	$next = 1;
}

$prev_pages_next = "";
if ($recordcount > $endlimit) {

	$prev_pages_next = "<nav><div class=\"pagination\"><ul><li><a href=\"/$permalink/$newsdate/0/$previous/\">&lt; Prev</a></li><li><ul>" .$compile_pages."</ul></li><li><a href=\"/$permalink/$newsdate/0/$next/\">Next &gt;</a></li></ul></div></nav>";

}

### PAGES NEXT & PREV : END

$resultset = conn_gets("SELECT ID, parentid, toplevelid, levelid, alias, permalink, _image, _fragment_01, author, contentopen, fragparts, keygrip, aux1, aux2, aux3, lockeduser, lockeddate, created, updated, userdate, orderby, status

                        FROM tbl_alpha_news

                        WHERE ${sql_where} status = '1' ORDER BY ${userdateorderby} ${ascdesc}, ID LIMIT ${startlimit}, ${endlimit}");

$news_numrows = mysql_num_rows($resultset);

$GF__ = array();# Get a list of the news ID for this news list
$GI__ = array();# Get a list of the image ID for this news list
while ($rset = mysql_fetch_row($resultset)) {
	$GF__[$rset[0]] = $rset[0];
	$GI__[$rset[6]] = $rset[6];
}
unset($rset);

$ML_          = array();# image array
$resultset_ML = conn_gets("SELECT ID, alias, filetype, newfilename FROM tbl_media_library WHERE ID IN(".implode(',', $GI__).") AND status = '1'");
while ($rset_ML = mysql_fetch_row($resultset_ML)) {# Store an array of images that are required for this gallery only
	$ML_[$rset_ML[0]] = array($rset_ML[0], $rset_ML[1], $rset_ML[2], $rset_ML[3]);
}
mysql_free_result($resultset_ML);

unset($GI__, $rset_ML);

#####################

$NM__ = array();

$resultset_MT = conn_gets("SELECT post_ID, meta_value FROM tbl_alpha_news_meta WHERE post_ID IN(".implode(',', $GF__).") AND meta_key = '_fragment_01' AND post_status = 'published'");

$nmet_count = 0;

while ($rset_MT = mysql_fetch_row($resultset_MT)) {

	$nmet_count++;

	$compile_snippet = '';

	if (trim($rset_MT[1]) != '') {

		$news_fragment_01_snippet = substr(strip_tags($rset_MT[1]), 0, 600);
		$news_fragment_01_snippet = explode(" ", $news_fragment_01_snippet);

		$stripstrlen = 0;
		foreach ($news_fragment_01_snippet as $key => $value) {

			$stripstrlen = numberFormat(($stripstrlen+strlen($value)), 0);

			if ($stripstrlen < 190) {
				$compile_snippet .= " ".$value;
			}
		}

	}
	unset($news_fragment_01_snippet, $stripstrlen, $key, $value);

	$NM__[$rset_MT[0]] = $compile_snippet;
	unset($compile_snippet);

}
mysql_free_result($resultset_MT);
unset($rset_MT, $nmet_count, $_newa_meta);

mysql_data_seek($resultset, 0);# Rewind resultset

$news_count           = 0;
$compile_articles     = '';
$compile_articles_top = '';

while ($rset = mysql_fetch_row($resultset)) {

	$news_count++;
	$news_ID          = $rset[0];
	$news_parentid    = $rset[1];
	$news_toplevelid  = $rset[2];
	$news_levelid     = $rset[3];
	$news_alias       = $rset[4];
	$news_permalink   = $rset[5];
	$news_image       = $rset[6];
	$news_fragment_01 = $rset[7];
	$news_author      = $rset[8];
	$news_contentopen = $rset[9];
	$news_fragparts   = $rset[10];
	$news_keygrip     = $rset[11];
	$news_aux1        = $rset[12];
	$news_aux2        = $rset[13];
	$news_aux3        = $rset[14];
	$news_lockeduser  = $rset[15];
	$news_lockeddate  = $rset[16];
	$news_created     = $rset[17];
	$news_updated     = $rset[18];
	$news_userdate    = $rset[19];
	$news_orderby     = $rset[20];
	$news_status      = $rset[21];

	$news_userdate_url       = dateFormat("Y-m", $news_userdate, 0);
	$news_userdate_formatted = dateFormat("D<b>d</b>M", $news_userdate, 0);

	$compile_article_link = "/${permalink}/";
if (trim($query_1) != "") {
	$compile_article_link .= "${query_1}/";
} else {
	$compile_article_link .= "all/";
}
$compile_article_link .= "${news_ID}/${page}/" .preg_replace("/[^-a-zA-Z0-9s\[\]_]/", "", str_replace(" ", "-", trim(substr(strtolower($news_alias), 0, 255))))."/";

if ($news_count > 1) {
	$hasHr = "<hr>";
} else {
	$hasHr = "";
}

$compile_snippet = $NM__[$news_ID];

$this_content = <<<EOF

		 <h2>${news_alias}</h2>
		 <p>${compile_snippet} &hellip;</p>
                 <p><a href="${compile_article_link}">Read More</a></p>



EOF

;

unset($news_alias, $compile_snippet);

$media_location = 'images/media/_img/';
$image_holder   = '';
$image_exists   = 0;

$media_ID          = $ML_[$news_image][0];
$media_alias       = $ML_[$news_image][1];
$media_filetype    = $ML_[$news_image][2];
$media_newfilename = $ML_[$news_image][3];

$media_file_location = $media_location.$media_newfilename;

if ($media_filetype == '_image' && trim($media_newfilename) != '' && is_file($media_file_location)) {

	$image_exists = 1;

	$image_holder = "<a href=\"${compile_article_link}\" class=\"timg-bg\" style=\"background-image: url( '/${media_file_location}' );
 \"><img src=\"/images/thumbnail-ratio.png\" class=\"img-responsive\" alt=\"${media_alias}\"></a>";
}

unset($media_ID, $media_alias, $media_filetype, $media_newfilename, $media_file_location);

if ($image_exists > 0) {

	$compile_articles .= <<<EOF

              <li>

                <ul>
                 <li class="article-date"><span>${news_userdate_formatted}</span></li>
                 <li class="article-image">
${image_holder}</li>
                 <li class="article-text">
${this_content}</li>
                </ul>


              </li>

EOF

;

} else {

	$compile_articles .= <<<EOF

              <li>

               <ul>
                <li class="article-date"><span>${news_userdate_formatted}</span></li>
                <li class="article-text">
${this_content}</li>
               </ul>

              </li>

EOF

;

}

unset($image_holder, $news_userdate_formatted);

}

if ($compile_articles != '') {

	$render_news = <<<EOF
<div class="news-list"><!--this is all news list-->
${render_archive}<div class="news-content">
          <ul>
${compile_articles}</ul>
         </div>
        </div>

EOF

;

}

}

unset($ML_, $NM__);

$include_content .= <<<EOF
<div class="page-news">
<!--this is all news-->

     <main>

      <div class="main">
       <div class="container">

${pcnt_title_or_alias_tag}


${render_news}</div>
      </div>

     </main>

    </div>


EOF

;

unset($render_archive, $render_news);

?>
